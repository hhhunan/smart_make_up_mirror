<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('index');
});
Route::get('/dashboard', function () {
    return view('index');
});
Route::get('/login', function () {
    return view('index');
});
Route::get('/signup', function () {
    return view('index');
});
Route::get('/dashboard/{any?}', function ($any) {
    return view('index');
});
Route::get('/components/{any?}', function ($any) {
    return view('index');
});
Route::get('/not-found', function () {
    return view('index');
});
Route::get('/components/products/edit/{any?}', function ($any) {
    return view('index');
});
Route::get('/components/product/add', function () {
    return view('index');
});
Route::get('/components/products/{any?}', function ($any) {
    return view('index');
});
Route::get('/components/styleShow/{any?}', function ($any) {
    return view('index');
});
Route::get('/components/style/edit/{any?}', function ($any) {
    return view('index');
});
Route::get('/components/sessions/{any?}', function ($any) {
    return view('index');
});