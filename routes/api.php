<?php

/*
|--------------------------------------------------------------------------
|      //   / /
|     //___   ___      _   __     / __      ___
|    / ___  //   ) ) // ) )  ) ) //   ) ) //   ) ) \\ / /
|   //     //   / / // / /  / / //   / / //   / /   \/ /
|  //     ((___( ( // / /  / / ((___/ / ((___/ /    / /\
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/*
 * Auth
 * */

Route::post('register', 'Api\Auth\RegisterController@register');
Route::post('refresh', 'Api\Auth\LoginController@refresh');
Route::post('social_auth', 'Api\Auth\SocialAuthController@socialAuth');

// Web login
Route::post('login', 'Api\Auth\LoginController@login');

// Device login
Route::post('device_login', 'Api\Auth\LoginController@device_login');

Route::middleware('DeviceAuth')->group(function () {
    Route::post('device/update/data_version', 'DeviceUpdateData@data_version');
    Route::post('device/update/change_status', 'DeviceUpdateData@change_status');
});

/*
 * other routes
 * must by authorized
 * must by insert header Access token before send http query
 * */
Route::group(['middleware' => ['auth:api','RoleChecker']], function() {
    Route::get('GetDeviceUpdate', 'DeviceController@getDeviceUpdate');
    Route::get('GetDeviceUpdate/{id}', 'DeviceUpdateData@getDeviceUpdate_show');

    Route::put('DeviceUpdate', 'DeviceController@editDeviceUpdate');
    Route::delete('DeviceUpdate', 'DeviceController@destroyDeviceUpdate');

    Route::get('devicesUpdates/getDeviceUpdates', 'DeviceUpdateData@getDevicesAndUpdates');
    Route::get('deviceUpdate/searchByImeiNumberByHash', 'DeviceController@searchByImaiNumberByHash');
    Route::get('deviceUpdate/searchDate', 'DeviceUpdateData@getUpdateDate');
    Route::get('deviceUpdate/searchStatus', 'DeviceUpdateData@getUpdateStatus');

    Route::delete('device/{id}/{pid?}', 'DeviceController@destroy')->where([
        'id' => '[0-9]+', // id - device id
        'pid' => '[0-9]+' // pid - product id
    ]);
    Route::delete('device_product_destroy/{id}/{pid?}', 'DeviceController@destroy_device_product')->where([
        'id' => '[0-9]+', // id - device id
        'pid' => '[0-9]+' // pid - product id
    ]);

    Route::delete('product_device/{id}/{pid?}', 'ProductController@destroy_device')->where([
        'id' => '[0-9]+', // id - device id
        'pid' => '[0-9]+' // pid - product id
    ]);
    Route::delete('style_device/{id}/{pid?}', 'DeviceController@destroy_device_style')->where([
        'id' => '[0-9]+', // id - device id
        'pid' => '[0-9]+' // pid - product id
    ]);
    Route::delete('product_brand/{id}/{pid?}', 'ProductController@destroy_brand')->where([
        'id' => '[0-9]+', // id - brand id
        'pid' => '[0-9]+' // pid - product id
    ]);
    Route::delete('product_session/{id}/{pid?}', 'ProductController@destroy_session')->where([
        'id' => '[0-9]+', // id - session id
        'pid' => '[0-9]+' // pid - product id
    ]);
    Route::delete('product_category/{id}/{pid?}', 'ProductController@destroy_category')->where([
        'id' => '[0-9]+', // id - category id
        'pid' => '[0-9]+' // pid - product id
    ]);

    Route::get('myProfile', 'UserController@get_user');
    Route::get('GetDevicetList', 'DeviceController@getDevice');
    Route::get('GetProductList', 'DeviceController@getProduct');
    Route::get('GetStyleList', 'DeviceController@getStyle');
    Route::get('GetBrandList', 'BrandController@getBrand');
    Route::get('GetCategoryList', 'CategoryController@getCategory');
    Route::get('GetSessionList', 'SessionController@getSession');
    Route::get('GetGroup', 'DeviceConfigController@getGroup');
    Route::get('GetType', 'DeviceConfigController@getType');
    Route::get('photos/get_all_by_type', 'PhotoController@getAllImagesByType');
    Route::get('photos/get_image_types', 'PhotoController@getImageTypes');

    Route::get('GetPlayList', 'PlayListController@getPlayList');
    Route::get('GetMovieList', 'MovieController@getMovieList');
    Route::get('product/analytics', 'ProductController@analytic');
    Route::get('styles/analytics', 'MakeUpStyleController@analytic');
    Route::get('device/analytics', 'DeviceController@analytic');

    Route::post('UserPersonalData/{user}', 'UserController@UserPersonalData')->where([
        'id' => '[0-9]+' // id - user id
    ]);
    Route::post('UserRoleSystem/{user}', 'UserController@UserRoleSystem')->where([
        'id' => '[0-9]+' // id - user id
    ]);
    Route::post('UserPassword/{user}', 'UserController@UserPassword')->where([
        'id' => '[0-9]+' // id - user id
    ]);

    Route::post('logout', 'Api\Auth\LoginController@logout');
    Route::post('editMyProfile', 'UserController@editMyProfile');
    Route::post('device_product/{id}', 'DeviceController@addProducts')->where([
        'id' => '[0-9]+' // id - device id
    ]);
    Route::post('device_style/{id}', 'DeviceController@addStyle')->where([
        'id' => '[0-9]+' // id - device id
    ]);
    Route::post('product_brand/{id}', 'ProductController@addBrands')->where([
        'id' => '[0-9]+', // id - brand id
    ]);
    Route::post('product_category/{id}', 'ProductController@addCategory')->where([
        'id' => '[0-9]+', // id - category id
    ]);
    Route::post('product_device/{id}', 'ProductController@addDevices')->where([
        'id' => '[0-9]+' // id - product id
    ]);
    Route::post('product_session/{id}', 'ProductController@addSession')->where([
        'id' => '[0-9]+' // id - product id
    ]);
    Route::post('device_status/{id}', 'DeviceController@setStatus')->where([
        'id' => '[0-9]+' // id - device id
    ]);
    Route::get('device_status/{id}', 'DeviceController@getStatus')->where([
        'id' => '[0-9]+' // id - device id
    ]);
    Route::put('generateSecretKey/{client_id}', 'DeviceController@GenerateSecretKey')->where([
        'id' => '[0-9]+' // id - device id
    ]);

//    Route::post('device_style/{id}', 'DeviceController@addStyle')->where([
//        'id' => '[0-9]+' // id - device id
//    ]);

    Route::resource('device_config', 'DeviceConfigController');
    Route::resource('brand', 'BrandController');
    Route::resource('category', 'CategoryController');
    Route::resource('product', 'ProductController');
    Route::resource('make_up_by_styles', 'MakeUpStyleController');
    Route::resource('make_up_by_methods', 'MakeUpMethodController');
    Route::resource('session', 'SessionController');
    Route::resource('playlist', 'PlayListController');
    Route::resource('user', 'UserController');
    Route::resource('language', 'LanguageController');
    Route::resource('role', 'RoleController');
    Route::resource('photo', 'PhotoController');
    Route::resource('movie', 'MovieController');
    Route::resource('device', 'DeviceController');
    Route::resource('labels', 'DeviceLabelController');

});



