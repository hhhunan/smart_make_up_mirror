<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Admin Mirror">
        <title>Admin Mirror</title>
        <base href="/">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- <link rel="icon" type="image/x-icon" href="favicon.ico"> -->
        <link rel="stylesheet" type="text/css" href="css/jquery-jesse.css" />
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <app-root></app-root>
    <!--Required JavaScript Libraries-->

    <!-- Section start -->
    <script type="text/javascript" src="project/inline.bundle.js"></script>
    <script type="text/javascript" src="project/polyfills.bundle.js"></script>
    <script type="text/javascript" src="project/scripts.bundle.js"></script>
    <script type="text/javascript" src="project/styles.bundle.js"></script>
    <script type="text/javascript" src="project/vendor.bundle.js"></script>
    <script type="text/javascript" src="project/main.bundle.js"></script>
    <script type="text/javascript" src="js/jquery-jesse.js"></script>
    </body>
</html>
