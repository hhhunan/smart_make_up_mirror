
<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >
      <div class="form-group">
        <label for="catagry_name">Name</label>
         <input type="hidden" name="_token" value="{{ csrf_token()}}">
        <input type="text" class="form-control" id="catagry_name" placeholder="Name">
        <p class="invalid">Enter Catagory Name.</p>
      </div>
      <div class="form-group">
        <label for="catagry_name">Logo</label>
        <input type="file" class="form-control" id="catagry_logo">
        <p class="invalid">Enter Catagory Logo.</p>
    </div>

    </form>
    </div>
    <div class="modelFootr">
      <button type="button" class="addbtn">Add</button>
      <button type="button" class="cnclbtn">Reset</button>
    </div>
  </div>
<script>
    $(".addbtn").click(function(){
        $.ajax({
            url:'/photo',
            data:{
                logo:new FormData($("#upload_form")[0]),
            },
            dataType:'json',
            async:false,
            type:'post',
            processData: false,
            contentType: false,
            success:function(response){
                console.log(response);
            },
        });
    });
</script>