
<?php

return [
    'access' => 'You do not have access.',
    'success_creat' => 'successfully created',
    'success_updated' => 'successfully updated',
    'success_change_status' => 'successfully changed status',
    'error_save' => 'Failed is not save.',
    'lang'=>'The language',
    '_lang'=>'is not found.',
    'destroy' =>'successfully deleted',
    'error_destroy' => 'Failed is not delete',
    'error_delete' => 'Failed id doesn`t exists.',
    'error_file' => 'file error',
    'file_exists' => 'The file exists...',
    'undefined' =>'undefined',
    'no_item'=> 'No record to see! Please create',
    'latest_updates'=> 'You have the latest updates...',
    'error_default'=> 'this is default language...',

    "error_created" => "Failed to created.",
    "error_field" => "Undefined field.",
    "record_not_found" => "Not found item id ':id' in ':model'.",
    "page_not_found" => "This page not found.",
    "language_not_default" => "The last language should not be inactive.",

    "not_translation" => "There is no translation from this language(:language) in ':model'.",
];
