
<?php

return [
    'access' => 'У вас нет доступа.',
    'success_creat' => 'успешно создано',
    'success_updated' => 'успешно обновлено',
    'error_save' => 'Не удалось сохранить',
    'lang'=>'Язык ',
    '_lang'=>'не найден',
    'destroy' =>'успешно удален',
    'error_destroy' => 'Не удалось удалить',
    'error_delete' => 'Идентификатор не существует',
    'error_file' => 'ошибка файла',
    'file_exists' => 'Файл существует...',
    'undefined' =>'неопределенный',
    'no_item'=> 'Ничего не найдено!',
    "language_not_default" => "Последний язык не должен быть неактивным.",

    "not_translation" => "В ':model' отсутствует перевод с данного языка(:language).",
];
