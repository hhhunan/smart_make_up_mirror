import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ProductComponent} from './product.component';
import {BrandComponent} from './brand.component';
import {ImageComponent} from './image.component';
import {DeviceComponent} from './device.component';
import {VideoComponent} from './video.component';
import {SessionComponent} from './session.component';
import {PlayListComponent} from './playList.component';
import {CategoryComponent} from './category.component';
import {UserComponent} from './user.component';
import {ProductEditComponent} from './product-edit.component';
import {ProductDetailComponent} from './product-detail.component';
import {ProductAddComponent} from './product-add.component';
import {DeviceconfigComponent} from './deviceconfig.component';
import {ViewsessionComponent} from './viewsession.component';
import {LanguageComponent} from './language.components';
import {LabelsComponent} from './labels.component';
import {DeviceupdateComponent} from './deviceupdate.component';
import {MakeupMethodsComponent} from './makeupmethods.component';
import {StyleComponent} from './style.component';
import {StyleSessionsComponent} from './style-sessions.component';
import {StylePlaylistComponent} from './style-playlist.component';
import {StyleVideoComponent} from './style-video.component';
import {StyleAddComponent} from './style-add.component';
import {StyleshowComponent} from './styleshow.component';
import {StyleEditComponent} from './style-edit.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Components'
        },
        children: [
            {
                path: 'makeup',
                component: MakeupMethodsComponent,
                data: {
                    title: 'Makeup'
                }
            },
            {
                path: 'products',
                component: ProductComponent,
                data: {
                    title: 'Products'
                }
            },
            {
                path: 'products/:id',
                component: ProductDetailComponent,
                data: {
                    title: 'Products Detail'
                }
            },
            {
                path: 'products/edit/:id',
                component: ProductEditComponent,
                data: {
                    title: 'Products Edit'
                }
            },
            {
                path: 'product/add',
                component: ProductAddComponent,
                data: {
                    title: 'Products Add'
                }
            },
            {
                path: 'categories',
                component: CategoryComponent,
                data: {
                    title: 'Category'
                }
            },
            {
                path: 'brands',
                component: BrandComponent,
                data: {
                    title: 'Brands'
                }
            },
            {
                path: 'images',
                component: ImageComponent,
                data: {
                    title: 'Image'
                }
            },
            {
                path: 'devices',
                component: DeviceComponent,
                data: {
                    title: 'Devices'
                }
            },
            {
                path: 'deviceconfig',
                component: DeviceconfigComponent,
                data: {
                    title: 'Device Config'
                }
            },
            {
                path: 'playLists',
                component: PlayListComponent,
                data: {
                    title: 'PlayLists'
                }
            },
            {
                path: 'sessions',
                component: SessionComponent,
                data: {
                    title: 'Sessions'
                }
            },
            {
                path: 'sessions/:id',
                component: ViewsessionComponent,
                data: {
                    title: 'View sessions'
                }
            },
            {
                path: 'videos',
                component: VideoComponent,
                data: {
                    title: 'Videos'
                }
            }, {
                path: 'users',
                component: UserComponent,
                data: {
                    title: 'Users'
                }

            },
            {
                path: 'language',
                component: LanguageComponent,
                data: {
                    title: 'Language'
                }

            },
            {
                path: 'labels',
                component: LabelsComponent,
                data: {
                    title: 'Labels'
                }
            },
            {
                path: 'deviceupdatedataversion',
                component: DeviceupdateComponent,
                data: {
                    title: 'Devices Updates'
                }
            },
            {
                path: 'style',
                component: StyleComponent,
                data: {
                    title: 'Style'
                }
            },
            {
                path: 'style-create',
                component: StyleAddComponent,
                data: {
                    title: 'Style Create'
                }
            },
            {
                path: 'styleShow/:id',
                component: StyleshowComponent,
                data: {
                    title: 'Style show'
                }
            },
            {
                path: 'style-sessions',
                component: StyleSessionsComponent,
                data: {
                    title: 'Style sessions'
                }
            },
            {
                path: 'style-playlist',
                component: StylePlaylistComponent,
                data: {
                    title: 'Style playlist'
                }
            },
            {
                path: 'style-video',
                component: StyleVideoComponent,
                data: {
                    title: 'Style video'
                }
            },
            {
                path: 'style/edit/:id',
                component: StyleEditComponent,
                data: {
                    title: 'Style Edit'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ComponentsRoutingModule {
}