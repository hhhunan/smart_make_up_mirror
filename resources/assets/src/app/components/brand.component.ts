import {Component, OnInit, ElementRef} from '@angular/core';
import {Brand} from '../models/brand';
import {RestDataService} from '../shared/service/rest-data.service';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';

declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'brand.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('300ms', [
                            animate('600ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})

export class BrandComponent implements OnInit {

    private brands: Brand[];
    private viewData: string;
    private messageError: string;
    private messageError_modal: string;
    private message: string;
    private loading: boolean;
    private editBrands: any;
    public showBrand: any;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private lang: string;
    private CreateEditObj: CreateEditObj;
    private paginDiss = 1;
    private user_type: string;
    private loader: string;
    private fullPagination: any;
    public four: number;
    private language_: any;
    private brand_image: any;
    private image_id: number;
    private name: object;
    private sortType: string;
    private messageSearch: string;
    private no_result: boolean;
    private no_result_brand: boolean;
    private no_result_mejjige: string;
    private button_disable: boolean;
    private search_params: string;
    private sort_params: string;
    private status: number;
    private objstatus: object;
    private disablestatus: number;
    private imageBase: any;
    private fileEditImage: boolean;
    private fileSize: boolean;
    private recuared_image: number;
    private degriss: string = 'deg0';
    private tabs: any = 'tabs1';
    private img_path: any = '';
    private img_size: number;

    constructor(private dataService: RestDataService,
                private elem: ElementRef) {
    }

    ngOnInit() {
        this.paginDiss = 1;
        this.img_size = this.dataService.img_size_in_byte
        this.recuared_image = -1;
        this.search_params = '';
        this.sort_params = '';
        this.button_disable = false;
        this.pagination = [];
        this.fullPagination = [];
        this.four = 4;
        this.fileEditImage = true;
        this.fileSize = true;
        this.no_result = false;
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('brand', this.lang).subscribe((items) => {
            this.brands = items['data'];
            if (items.message) {
                this.brands = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            this.sortType = items['sort_type'];
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }

            this.tooltipImage();
            $(document).ready(function () {
                $('.search-open').click(function () {
                    $('#search-toggle').toggle(500);
                });
            });
        }, (err) => {
            this.brands = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // tooltype for images

    tooltipImage() {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip({
                animated: 'fade',
                placement: 'bottom',
                html: true
            });
        });
    }

    // Message hide

    timOut() {
        setTimeout(function () {
            // this.message = '';
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    // Refresh Component

    refresh() {
        this.messageSearch = '';
        this.brands = null;
        this.ngOnInit();
    }

    // Update status from table

    changeStatus(status, id) {
        if (status === true) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.objstatus = {
            'status': this.status
        };
        this.disablestatus = id;
        this.dataService.editData('brand', this.objstatus, id).subscribe(res => {
            this.message = 'Updated';
            this.degris();
            this.disablestatus = -1;
            this.timOut();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    // Change language

    change_language(lang) {
        if (lang !== localStorage.getItem('activeLanguage')) {
            this.loader = '';
            this.lang = lang;
            localStorage.removeItem('activeLanguage');
            localStorage.setItem('activeLanguage', lang);
            this.dataService.getData('brand?page=' + this.paginDiss, lang).subscribe(
                (items) => {
                    this.brands = items['data'];
                    this.loader = 'true';
                    if (items.message) {
                        this.brands = [];
                        this.no_result = true;
                        this.no_result_mejjige = items.message;
                    }
                    this.tooltipImage();
                }, (err) => {
                    this.brands = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                });
        }
    }

    // Searching

    search(form) {
        this.search_params = '';
        for (const ress in form) {
            if (form[ress] !== '' && form[ress] !== null) {
                this.search_params += '&' + ress + '=' + form[ress];
            }
        }
        this.button_disable = true;
        this.loader = '';
        this.dataService.getData('brand?' + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.fullPagination = [];
                this.pagination = [];
                if (typeof items['message'] === 'undefined') {
                    this.brands = items['data'];
                } else {
                    this.brands = [];
                }
                this.loader = 'true';
                this.tooltipImage();
                this.paginDiss = 1;
                for (let i = 1; i <= items['last_page']; i++) {
                    this.fullPagination.push(i);
                    this.recentPage = items['last_page'];
                }

                if (this.fullPagination.length > 5) {
                    this.pagination = this.fullPagination.slice(0, 5);
                } else {
                    this.pagination = this.fullPagination;
                }
                if (this.brands[0] == null) {
                    this.messageSearch = 'No result';
                } else {
                    this.messageSearch = '';
                }
            }, (err) => {
                this.brands = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
    }

    // Sorting table

    filter(sort_name, sort) {
        this.sort_params = '';
        this.sort_params += '&' + sort_name + '=' + sort;
        this.loader = '';
        this.dataService.getData('brand?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.brands = items['data'];
                this.sortType = items['sort_type'];
                this.loader = 'true';
                this.tooltipImage();
            }, (err) => {
                this.brands = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
    }

    // View this brand

    openModal(id) {
        this.showBrand = null;
        this.messageError = '';
        this.messageError_modal = '';
        this.viewData = 'view';
        this.dataService.getData('brand/' + id, '').subscribe((items) => {
            this.showBrand = items;
        }, (err) => {
            this.brands = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // open Edit modal this brand

    openEditModal(id) {
        this.img_path = '';
        this.recuared_image = -1;
        this.fileEditImage = true;
        this.fileSize = true;
        this.image_id = -1;
        this.messageError = '';
        this.messageError_modal = '';
        this.viewData = 'edit';
        this.no_result_brand = false;
        this.brand_image = null;
        this.editBrands = null;
        this.dataService.getData('brand/' + id, '').subscribe((items) => {
            this.editBrands = items;
            this.image_id = this.editBrands.image_id;
        }, (err) => {
            this.editBrands = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // open add Modal

    openAddModal() {
        this.img_path = '';
        this.recuared_image = -1;
        $('#formadd').trigger('reset');
        this.fileEditImage = true;
        this.fileSize = true;
        this.image_id = -1;
        this.messageError = '';
        this.messageError_modal = '';
        this.viewData = 'add';
        this.no_result_brand = false;
    }

    // Update this brand

    onSumbit(form, id) {
        this.loader = '';
        this.loading = true;
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['name_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang;
        }
        if (form.editstatus == 1) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        const files = this.elem.nativeElement.querySelector('#selectFile').files;
        const file = files[0];
        if (file !== undefined) {
            this.CreateEditObj = {
                'name': this.name,
                'status': this.status,
                'image': this.imageBase,
            };
        } else {
            this.CreateEditObj = {
                'name': this.name,
                'status': this.status,
            };
        }

        this.dataService.editData('brand', this.CreateEditObj, id).subscribe((res) => {
            this.dataService.getData('brand?page=' + this.paginDiss, this.lang).subscribe((items) => {
                this.brands = items['data'];
                this.message = 'Updated';
                this.degris();
                this.timOut();
                this.loading = false;
                this.loader = 'true';
                $('#myModal .close').click();
                this.tooltipImage();
            }, (err) => {
                this.brands = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    // Delete this brand

    deleteId(id) {
        this.loader = '';
        this.lastPage = '';
        this.dataService.getDeleteId(id, 'brand').subscribe((items) => (
            this.dataService.getData('brand?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
                .subscribe((item) => {
                    this.brands = item['data'];
                    this.loader = 'true';
                    this.message = 'Removed';
                    this.degris();
                    this.tooltipImage();
                    this.timOut();
                    if (item['message']) {
                        this.brands = [];
                        this.no_result = true;
                        this.no_result_mejjige = item['message'];
                    }
                    if (this.fullPagination.length !== item['last_page']) {
                        this.pagination = [];
                        this.fullPagination = [];
                        for (let i = 1; i <= item['last_page']; i++) {
                            this.fullPagination.push(i);
                            this.recentPage = item['last_page'];
                        }
                        if (this.fullPagination.length > 5) {
                            this.pagination = this.fullPagination.slice(0, 5);
                        } else {
                            this.pagination = this.fullPagination;
                        }
                        this.paginationData(1);
                    }
                }, (err) => {
                    this.brands = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        ), error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    // Create brand

    add(form) {
        this.name = {};
        this.loader = '';
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['nameadd_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang;
        }

        if (form.addstatus == null || form.addstatus == false) {
            this.status = 0;
        } else {
            this.status = 1;
        }
        this.CreateEditObj = {
            'name': this.name,
            'status': this.status,
            'image': this.imageBase
        };
        this.dataService.createData('brand', this.CreateEditObj).subscribe(
            (item) => (this.dataService.getData('brand?page=' + this.paginDiss, this.lang)
                .subscribe((items) => {
                    this.no_result = false;
                    this.brands = items['data'];
                    this.loader = 'true';
                    $('#myModal .close').click();
                    $('#formadd').trigger('reset');
                    this.tooltipImage();
                    this.message = 'Created';
                    this.degris();
                    this.timOut();
                    if (this.fullPagination.length !== items['last_page']) {
                        this.pagination = [];
                        this.fullPagination = [];
                        for (let i = 1; i <= items['last_page']; i++) {
                            this.fullPagination.push(i);
                            this.recentPage = items['last_page'];
                        }
                        if (this.fullPagination.length > 5) {
                            this.pagination = this.fullPagination.slice(0, 5);
                        } else {
                            this.pagination = this.fullPagination;
                        }
                    }
                    this.loader = 'true';
                }, (err) => {
                    this.brands = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })), error => {
                this.messageError_modal = this.dataService.error;
                this.loading = false;
                this.loader = 'true';
            }
        );
    }

    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationget(page);
                } else if (this.fullPagination[page] !== undefined && this.fullPagination[page + 1] === undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationget(page);
                } else if (page === this.recentPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationget(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationget(page);
            }
        }
    }

    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss !== 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss !== this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationget(page) {
        this.paginDiss = page;
        this.loader = '';
        this.lastPage = this.recentPage;
        this.dataService.getData('brand?page=' + page + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.brands = items['data'];
                this.loader = 'true';
                this.tooltipImage();
            }, (err) => {
                this.brands = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss !== 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('brand?page=' + 1 + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.brands = items['data'];
                    this.loader = 'true';
                    this.tooltipImage();
                }, (err) => {
                    this.brands = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        } else if (last_first === 'last' && this.paginDiss !== this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('brand?page=' + this.lastPage + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.brands = items['data'];
                    this.loader = 'true';
                    this.tooltipImage();
                }, (err) => {
                    this.brands = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        }
    }

    selected(id) {
        this.image_id = id;
    }


    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                } else {
                    this.recuared_image = 1;
                    this.fileSize = true;
                    let _this = this;
                    _this.readUrl(event);
                    let input = event.target;
                    let reader = new FileReader();
                    reader.onload = function () {
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.recuared_image = -1;
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.recuared_image = -1;
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path = '';
        }
    }

    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }

    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }

    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();

            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }

            reader.readAsDataURL(event.target.files[0]);
        }
    }
}

interface CreateEditObj {
    'name': any;
    'status': number,
    'image'?: number
}
