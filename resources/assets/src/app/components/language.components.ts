import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {Router} from '@angular/router';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'language.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('600ms', [
                            animate('800ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})
export class LanguageComponent implements OnInit {

    private language: any;
    public images: any;
    private lang: string;
    private loader: string;
    private viewData: string;
    private showLanguage: any;
    private showEdit: any;
    private user_type: string;
    private message: string;
    private messageError: string;
    private messageError_modal: string;
    public imgActive: number;
    private CreateEditObj: CreateEditObj;
    private status: number;
    private default: number;
    private objstatus: object;
    private disablestatus: number;
    private imageBase: any;
    private fileEditImage: boolean;
    private fileSize: boolean;
    private recuared_image; number;
    private degriss: string = 'deg0';
    private img_path: any = '';
    private img_size: number;
    constructor(private dataService: RestDataService,
                private router: Router) {
    }

    ngOnInit() {
        this.img_size = this.dataService.img_size_in_byte;
        this.recuared_image = -1;
        this.fileEditImage = true;
        this.fileSize = true;
        if (localStorage.getItem('user_type') === 'administrator') {
            this.lang = localStorage.getItem('activeLanguage');
            this.language = JSON.parse(localStorage.getItem('languages'));
            this.user_type = localStorage.getItem('user_type');
        }else {
            this.router.navigateByUrl('/dashboard');
        }
    }

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }
    timOut_() {
        setTimeout(function () {
            this.messageError = ''
        }.bind(this), 4000);
    }
    openModal(lang) {

        this.showLanguage = lang;
        this.viewData = 'view';
    }


    openDeleteModal(lang) {

        this.showLanguage = lang;
        this.viewData = 'delete';
    }


    openEditModal(id) {
        this.img_path = '';
        this.imageBase = '';
        this.recuared_image = -1;
        this.messageError = '';
        this.messageError_modal = '';
        this.viewData = 'edit';
        this.dataService.getData('language/' + id, this.lang).subscribe((items) => {
            this.showEdit = items;
            this.imgActive = this.showEdit.image_id;
        });
    }

    onSubmit(id, form, img) {
        this.loader = '';

        if (form.editstatus == 1) {
            this.status = 1;
        } else {
            this.status = 0;
        }

        if ( this.imageBase == ''){
            this.CreateEditObj = {
                'name': form.name,
                'code': form.code,
                'status': this.status,
            };
        }else {
            this.CreateEditObj = {
                'name': form.name,
                'code': form.code,
                'status': this.status,
                'image': this.imageBase,
            };
        }
        this.dataService.editData('language', this.CreateEditObj, id).subscribe((res) => {
            this.dataService.getData('language', this.lang).subscribe((items) => {
                this.language = items;
                localStorage.removeItem('languages');
                localStorage.setItem('languages', JSON.stringify(this.language));
                this.message = 'Updated';
                this.degris();
                this.timOut();
            });
            this.loader = 'true';
            $('#myModal .close').click();
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
            this.timOut();
        });
    }

    changeStatus(status, id) {
        if (status === true) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.objstatus = {
            'status': this.status
        };
        this.disablestatus = id;
        this.dataService.editData('language', this.objstatus, id).subscribe(res => {
            this.dataService.getData('language', this.lang).subscribe((items) => {
                this.language = items;
                localStorage.removeItem('languages');
                localStorage.setItem('languages', JSON.stringify(this.language));
                this.message = 'Updated';
                this.degris();
                this.disablestatus = -1;
                this.timOut();
            });
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
            this.disablestatus = -1;
            this.timOut_();
            this.dataService.getData('language', this.lang).subscribe((items) => {
                this.language = items;
                this.timOut_();
            });
        });
    }


    changeDefault(defaultlang, id) {
        this.loader = '';
        if (defaultlang === true) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.objstatus = {
            'default': this.status
        };
        this.disablestatus = id;
        this.dataService.editData('language', this.objstatus, id).subscribe(res => {
            this.dataService.getData('language', this.lang).subscribe((items) => {
                this.language = items;
                for (let i = 0; i < items.length; i++) {
                    if (items[i].default === 1) {
                        localStorage.setItem('activeLanguage', items[i].code);
                    }
                }
                localStorage.removeItem('languages');
                localStorage.setItem('languages', JSON.stringify(this.language));
                this.message = 'Updated';
                this.degris();
                this.disablestatus = -1;
                this.timOut();
            });
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
            this.timOut_();
            this.disablestatus = -1;
            this.dataService.getData('language', this.lang).subscribe((items) => {
                this.language = items;
                this.timOut_();
            });
        });
    }

    openAddModal() {
        this.img_path = '';
        this.imageBase = '';
        $('#formadd').trigger('reset');
        this.messageError_modal = ''
        this.imgActive = -1;
        this.viewData = 'add';
        this.recuared_image = -1;
        this.fileEditImage = true;
        this.fileSize = true;
    }

    addLanguage(form) {
        this.loader = '';

        if (form.addstatus == null || form.addstatus == false) {
            this.status = 0;
        } else {
            this.status = 1;
        }

        this.CreateEditObj = {
            'name': form.addname,
            'code': form.addcode,
            'image': this.imageBase,
        };
        this.dataService.createData('language', this.CreateEditObj).subscribe((res) => {
            this.dataService.getData('language', this.lang).subscribe((items) => {
                this.language = items;
                localStorage.removeItem('languages');
                localStorage.setItem('languages', JSON.stringify(this.language));
                this.loader = 'true';
                this.message = 'Created';
                this.degris();
                $('#myModal .close').click();
                $('#formadd')[0].reset();
                this.timOut();
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    imageId(img) {
        this.imgActive = img;
    }

    deleteId(id) {
        this.loader = '';
        this.dataService.getDeleteId(id, 'language').subscribe((items) => (
            this.dataService.getData('language', this.lang).subscribe((item) => {
                this.language = item;
                localStorage.removeItem('languages');
                localStorage.setItem('languages', JSON.stringify(this.language));
                this.message = 'Removed';
                this.degris();
                this.loader = 'true';
                $('#myModal .close').click();
                this.timOut();
            })
        ), error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
            $('#myModal .close').click();
            this.timOut();
        });
    }

    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                }else {
                    this.recuared_image = 1;
                    this.fileSize = true;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl(event);
                    let reader = new FileReader();
                    reader.onload = function(){
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.recuared_image = -1;
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.recuared_image = -1;
            this.fileSize = true;
            this.fileEditImage = true;
            this.imageBase = '';
            this.img_path = '';
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }
}

interface CreateEditObj {
    'name': string,
    'code': string,
    'status'?: number,
    'image'?: number,
    'default'?: number,
}
