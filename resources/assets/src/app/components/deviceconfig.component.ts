import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: './deviceconfig.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ])
    ]
})
export class DeviceconfigComponent implements OnInit {
    private devicearray: any;
    private devicename: object[];
    private onedevice: any;
    private loader: string;
    private user_type: string;
    private viewData: string;
    private message: string;
    private messageError: string;
    private messageError_modal: string;
    private oneconfig: any;
    private CreateEditObj: CreateEditObj;
    private groupname: Array<object>;
    private typename: Array<object>;
    private creategroup: string;
    private createtype: string;
    private selectconfig: number;
    private oneconfigedit: any;
    private dev_id: any;
    private lang: string;
    private device: any;
    private no_result: boolean;
    private no_result_mejjige: string;
    private status: number;
    private degriss: string = 'deg0';

    constructor(private dataService: RestDataService) {
    }

    ngOnInit() {
        this.message = '';
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.onedevice = [];
        this.dataService.getData('device', 'en').subscribe((items) => {
            this.devicearray = items['data'];
            if (items.message) {
                this.devicearray = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            this.devicename = [];
            for (let i = 0; i < this.devicearray.length; i++) {
                this.devicename.push({'id': this.devicearray[i].id, 'name': this.devicearray[i].translation[0].name});
            }
        }, (err) => {
            this.devicearray = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        this.GetGroupGetType();
    }

    GetGroupGetType() {
        this.dataService.getData('GetGroup', 'en').subscribe((group) => {
            this.groupname = group;
        }, (err) => {
            this.groupname = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        this.dataService.getData('GetType', 'en').subscribe((type) => {
            this.typename = type;
        }, (err) => {
            this.typename = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // tooltype for table

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    // select device

    select(id) {
        this.dev_id = id;
        this.loader = '';
        this.selectconfig = id;
        this.dataService.getData('device/' + this.dev_id, this.lang).subscribe(res => {
            this.device = res;
            this.onedevice = res.device_config;
            this.loader = 'true';
        }, (err) => {
            this.devicearray = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        $('.create').css('display', 'block');
        $('.configalert').css('display', 'block');
    }

    // Open create modal
    createModal() {
        $('#formadd').trigger('reset');
        this.viewData = 'add';
     this.messageError_modal = '';
    }

    // Create device_config for this device
    add(form) {
        this.loader = '';
        if (form.addgroup == 'Select Group') {
            this.creategroup = form.group;
        } else {
            this.creategroup = form.addgroup;
        }

        if (form.addtype == 'Select Type') {
            this.createtype = form.typeinput;
        } else {
            this.createtype = form.addtype;
        }
        if (form.addstatus == null || form.addstatus == false) {
            this.status = 0;
        } else {
            this.status = 1;
        }
        this.CreateEditObj = {
            'group': this.creategroup,
            'status': this.status,
            'type': this.createtype,
            'value': form.configvalue,
            'device_id': this.selectconfig
        };

        this.dataService.createData('device_config', this.CreateEditObj).subscribe((items) => {
            this.dataService.getData('device/' + this.dev_id, 'en').subscribe((item) => {
                this.onedevice = item.device_config;
                this.message = 'Created';
                this.degris();
                this.loader = 'true';
                $('#myModal .close').click();
                this.timOut();
            }, (err) => {
                this.devicearray = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    // open view Modal
    view(config) {
        this.viewData = 'view';
        this.oneconfig = [config];
    }

    // open edit modal
    openEditModal(id) {
        this.messageError = '';
        this.messageError_modal = '';
        this.viewData = 'edit';
        this.GetGroupGetType();
        this.oneconfigedit = null;
        this.dataService.getData('device_config/' + id, this.lang).subscribe(items => {
            this.oneconfigedit = items;
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        })
    }

    // Update this device_config
    update(form, id) {
        this.loader = '';
        if (form.editstatus == 1) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.CreateEditObj = {
            'group': form.editgroup,
            'status': this.status,
            'type': form.edittype,
            'value': form.configvalueedit,
            'device_id': this.selectconfig
        };
        this.dataService.editData('device_config', this.CreateEditObj, id).subscribe(items => {
            this.dataService.getData('device/' + this.dev_id, this.lang).subscribe(res => {
                this.device = res;
                this.onedevice = res.device_config;
                this.loader = 'true';
                this.message = 'Updated';
                this.degris();
                $('#myModal .close').click();
                this.timOut();
            }, (err) => {
                this.devicearray = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        })
    }

    // deleted this device_config
    deleteID(id) {
        this.loader = '';
        this.messageError = '';
        this.dataService.getDeleteId(id, 'device_config').subscribe((items) => {
            this.dataService.getData('device/' + this.dev_id, 'en').subscribe((item) => {
                this.onedevice = item.device_config;
                this.loader = 'true';
                this.message = 'Removed';
                this.degris();
                this.timOut();
            }, (err) => {
                this.devicearray = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
}

interface CreateEditObj {
    'group': string,
    'status': any,
    'type': string,
    'value': string,
    'device_id': any
}
