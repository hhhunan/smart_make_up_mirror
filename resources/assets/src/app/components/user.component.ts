import {Component, OnInit, ElementRef} from '@angular/core';
import {Users} from '../models/users';
import {RestDataService} from '../shared/service/rest-data.service';
import {Router} from '@angular/router';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';

declare var jquery: any;
declare var $: any;

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('300ms', [
                            animate('600ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})
export class UserComponent implements OnInit {
    private users: Users[];
    private message: string;
    private viewData: string;
    private messageError: string;
    private messageError_modal: string;
    private editcategories: any;
    private loader: string;
    private showUser: any;
    private CreateEditObj: CreateEditObj;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private paginDiss = 1;
    private fullPagination: any;
    public four: number;
    private imageBase: any;
    private fileEditImage: boolean;
    private fileSize: boolean;
    private fileImage: boolean;
    private user_id: any;
    private no_result: boolean;
    private no_result_mejjige: string;
    private SendData: object;
    private recuared_image; number;
    private degriss: string = 'deg0';
    private img_path: any = '';
    private img_size: number;
    constructor(private dataService: RestDataService,
                private elem: ElementRef,
                private router: Router) {
    }

    ngOnInit() {
        if (localStorage.getItem('user_type') === 'administrator') {
            this.img_size = this.dataService.img_size_in_byte;
            this.recuared_image = -1;
            this.user_id = localStorage.getItem('user_id');
            this.fileEditImage = true;
            this.fileSize = true;
            this.fileImage = false;
            this.pagination = [];
            this.fullPagination = [];
            this.four = 4;
            this.dataService.getData('user', 'en').subscribe((items) => {
                this.users = items['data'];
                for (let i = 1; i <= items['last_page']; i++) {
                    this.fullPagination.push(i);
                    this.recentPage = items['last_page'];
                }
                if (this.fullPagination.length > 5) {
                    this.pagination = this.fullPagination.slice(0, 5);
                } else {
                    this.pagination = this.fullPagination;
                }
            }, (err) => {
                this.users = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }else {
            this.router.navigateByUrl('/dashboard');
        }
    }


    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    deleteId(id) {
        this.loader = '';
        this.dataService.getDeleteId(id, 'user').subscribe((item) => {
            this.dataService.getData('user', 'en').subscribe((items) => {
                this.users = items['data'];
                this.loader = 'true';
                this.message = 'Removed';
                this.degris();
                this.timOut();
                if (this.fullPagination.length !== items['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= items['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = items['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                    this.paginationData(1);
                }
            })
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    openEditModal(id) {
        this.img_path = '';
        this.messageError_modal = '';
        $('#passForm').trigger('reset');
        this.messageError = '';
        this.viewData = 'edit';
        this.editcategories = [];
        this.dataService.getData('user/' + id, '').subscribe((items) => {
            this.editcategories = [items];
        });
    }

    onSubmit(form, id) {
        this.loader = '';
        const files = this.elem.nativeElement.querySelector('#selectFile').files;
        const file = files[0];
        if (file !== undefined) {
            this.SendData = {'name': form.name, 'image': this.imageBase };
        }else {
            this.SendData = {'name': form.name };
        }
        this.dataService.createData('UserPersonalData/' + id, this.SendData).subscribe((res) => {
            this.dataService.getData('user?page=' + this.paginDiss, 'en').subscribe((items) => {
                this.users = items['data'];
                for (let i = 0; i < this.users.length; i++) {
                    if (this.users[i].id == this.user_id) {
                        localStorage.removeItem('image');
                        localStorage.removeItem('name');
                        localStorage.setItem('image', this.users[i].image);
                        localStorage.setItem('name', this.users[i].name);
                    }
                }
                this.loader = 'true';
                $('#myModal .close').click();
                this.message = 'Updated';
                this.degris();
                this.timOut();
            });

        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    openModal(id) {
        this.viewData = 'view';
        this.showUser = [];
        this.dataService.getData('user/' + id, '').subscribe((items) => {
            this.showUser = [items];
        });
    }

    openAddModal() {
        this.img_path = '';
        this.recuared_image = -1;
        this.fileEditImage = true;
        this.fileSize = true;
        $('#formadd').trigger('reset');
        this.messageError = '';
        this.messageError_modal = '';
        this.viewData = 'add';
    }

    onCreateSumbit(form) {
        this.loader = '';
        let files = this.elem.nativeElement.querySelector('#selectFile2').files;
        let formData = new FormData();
        let file = files[0];
        if (file && this.fileImage === false) {
            formData.append('image', this.imageBase);
        }
        formData.append('name', form.user_name);
        formData.append('email', form.email);
        formData.append('password', form.user_password);
        formData.append('naconfirm_passwordme', form.user_confirm_password);
        formData.append('role_id', form.role_id);
        this.dataService.register(formData).subscribe((data) => {
            this.dataService.getData('user?page=' + this.paginDiss, 'en').subscribe((items) => {
                this.users = items['data'];
                this.message = 'Created';
                this.degris();
                this.loader = 'true';
                $('#myModal .close').click();
                this.timOut();
                if (this.fullPagination.length !== items['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= items['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = items['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                }
            });
        }, (error) => {
            this.loader = 'true';
            this.messageError_modal = this.dataService.error;
        })
    }

    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                }else {
                    this.recuared_image = 1;
                    this.fileSize = true;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl(event);
                    let reader = new FileReader();
                    reader.onload = function(){
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.recuared_image = -1;
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.recuared_image = -1;
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path = '';
        }
    }

    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationget(page);
                } else if (this.fullPagination[page] != undefined && this.fullPagination[page + 1] == undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationget(page);
                } else if (page == this.lastPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationget(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationget(page);
            }
        }
    }

    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss != 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss != this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationget(page) {
        this.paginDiss = page;
        this.lastPage = this.recentPage;
        this.dataService.getData('user?page=' + page, 'en').subscribe(
            (items) => {
                this.users = items['data'];
                this.loader = 'true';
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss != 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('user?page=' + 1, 'en').subscribe(
                (items) => {
                    this.users = items['data'];
                    this.loader = 'true';
                })
        } else if (last_first === 'last' && this.paginDiss != this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('user?page=' + this.lastPage, 'en').subscribe(
                (items) => {
                    this.users = items['data'];
                    this.loader = 'true';
                })
        }
    }

    onRole(form, id) {
        this.loader = '';
        this.CreateEditObj = {
            'role_id': form.role_id_edit
        }
        this.dataService.createData('UserRoleSystem/' + id, this.CreateEditObj).subscribe((res) => {
            this.dataService.getData('user?page=' + this.paginDiss, 'en').subscribe((items) => {
                this.users = items['data'];
                this.loader = 'true';
                this.message = 'Updated';
                this.degris();
                $('#myModal .close').click();
                this.timOut();
            });

        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    onPassword(form, id) {
        this.loader = '';
        this.CreateEditObj = {
            'password': form.password,
            'confirm_password': form.confirm_password,
        }
        this.dataService.createData('UserPassword/' + id, this.CreateEditObj).subscribe((res) => {
            this.dataService.getData('user?page=' + this.paginDiss, 'en').subscribe((items) => {
                this.users = items['data'];
                this.loader = 'true';
                this.message = 'Updated';
                this.degris();
                $('#myModal .close').click();
                this.timOut();
            });

        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }
}

interface CreateEditObj {
    'name'?: string,
    'password'?: string,
    'confirm_password'?: string,
    'role_id'?: any;
}

