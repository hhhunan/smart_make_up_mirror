import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {RestDataService} from '../shared/service/rest-data.service';
import {Image} from '../models/image';
import {Video} from '../models/video';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'product-add.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ]),
        trigger('tabsdescription', [
            transition('tabsdesc1 => tabsdesc2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabsdesc2 => tabsdesc1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ])
    ]
})
export class ProductAddComponent implements OnInit {

    public images: Image[];
    public movie: Video;
    private lang: string;
    private language_: any;
    private message: string;
    private messageError: string;
    private loader: string;
    private user_type: string;
    public value: string[];
    public id: any;

    public addSessionsId: any;
    public sessions: any;
    public sessionsName: any;
    public sessionsData: Array<object> = this.sessions;

    public addBrandId: any;
    public brands: any;
    public brandsName: any;
    public brandsData: Array<object> = this.brands;

    public addCategoryId: any;
    public category: any;
    public categoryName: any;
    public categoryData: Array<object> = this.category;

    private CreateEditObj: CreateEditObj;

    private name: object;
    private description: object;
    private no_result_mejjige: string;
    private no_result: boolean;
    private status: number;

    private imageBase: any;
    private fileEditImage: boolean;
    private fileSize: boolean;
    private recuared_image; number;
    private myStatus: boolean;
    private degriss: string = 'deg0';
    private tabs: any = 'tabs1';
    private tabs_description: any = 'tabsdesc1';
    private img_path: any = '';
    public img_size: number;
    constructor(private router: Router,
                private dataService: RestDataService) {
    }

    ngOnInit() {
        if (localStorage.getItem('user_type') === 'administrator') {
            this.img_size = this.dataService.img_size_in_byte;
            this.addBrandId = [];
            this.addCategoryId = [];
            this.addSessionsId = [];
            this.recuared_image = -1;
            this.addSessionsId = [];
            this.lang = localStorage.getItem('activeLanguage');
            this.language_ = JSON.parse(localStorage.getItem('languages'));
            this.user_type = localStorage.getItem('user_type');
            this.dataService.getData('GetSessionList', this.lang).subscribe((items) => {
                this.sessions = items;
                this.sessionsName = [];
                this.addSessionsId = [];
                this.sessionsData = [];
                for (let i = 0; i < this.sessions.length; i++) {
                    this.sessionsName.push(this.sessions[i].translation[0].name);
                    this.sessionsData.push({'id': this.sessions[i].id.toString(), 'text': this.sessions[i].translation[0].name});
                }
                this.statusValidate();
            }, (err) => {
                this.sessions = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });

            this.dataService.getData('GetBrandList', this.lang).subscribe((items) => {
                this.brands = items;
                this.brandsName = [];
                this.addBrandId = [];
                this.brandsData = [];
                for (let i = 0; i < items.length; i++) {
                    this.brandsName.push(items[i].translation[0].name);
                    this.brandsData.push({'id': items[i].id.toString(), 'text': items[i].translation[0].name});
                }
                this.statusValidate();
            }, (err) => {
                this.brands = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });

            this.dataService.getData('GetCategoryList', this.lang).subscribe((items) => {
                this.category = items;
                this.categoryName = [];
                this.addCategoryId = [];
                this.categoryData = [];
                for (let i = 0; i < items.length; i++) {
                    this.categoryName.push(items[i].translation[0].name);
                    this.categoryData.push({'id': items[i].id.toString(), 'text': items[i].translation[0].name});
                }
                this.statusValidate();
            }, (err) => {
                this.category = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });

        } else {
            this.router.navigateByUrl('/components/products');
        }
    }

    statusValidate() {
        if (this.addSessionsId.length > 0 && this.addBrandId.length > 0 && this.addCategoryId.length > 0) {
            this.myStatus = true;
        } else {
            this.myStatus = false;
        }
    }

    public selectedSessions(value: any): void {
        this.addSessionsId.push(value.id);
        this.statusValidate();
    }

    public removedSessions(value: any): void {
        this.addSessionsId.splice(this.addSessionsId.indexOf(value.id), 1);
        this.statusValidate();
    }

    public selectedBrands(value: any): void {
        this.addBrandId.push(value.id);
        this.statusValidate();
    }

    public removedBrands(value: any): void {
        this.addBrandId.splice(this.addBrandId.indexOf(value.id), 1);
        this.statusValidate();
    }

    public selectedCategory(value: any): void {
        this.addCategoryId.push(value.id);
        this.statusValidate();
    }

    public removedCategory(value: any): void {
        this.addCategoryId.splice(this.addCategoryId.indexOf(value.id), 1);
        this.statusValidate();
    }

    public refreshValue(value: any): void {
        this.value = value;
    }

    onSubmit(form, status) {
        this.loader = '';
        this.name = {};
        this.description = {};
        for (let i = 0; i < this.language_.length; i++) {
            const value_lang = form['description_' + this.language_[i].code];
            const name_lang = form['add_name_' + this.language_[i].code];
            this.description[this.language_[i].code] = value_lang;
            this.name[this.language_[i].code] = name_lang;
        }
        if (form.addstatus == null && status.valid === true) {
            this.status = 1;
        }else if (form.addstatus === false) {
            this.status = 0;
        }else {
            this.status = 1;
        }
        this.CreateEditObj = {
            'name': this.name,
            'description': this.description,
            'status': this.status,
            'session_ids': this.addSessionsId,
            'brand_ids': this.addBrandId,
            'image': this.imageBase,
            'category_ids': this.addCategoryId,
        };
        this.dataService.createData('product', this.CreateEditObj)
            .subscribe(() => {
                this.loader = 'true';
                this.message = 'Created';
                this.degris();
                this.router.navigateByUrl('/components/products');
            }, error => {
                this.messageError = this.dataService.error;
                this.loader = 'true';
            });
    }

    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            this.img_path = event.srcElement.value
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                }else {
                    this.fileSize = true;
                    this.recuared_image = 1;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl(event);
                    let reader = new FileReader();
                    reader.onload = function(){
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.recuared_image = -1;
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.recuared_image = -1;
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path = '';
        }
    }
    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();

            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }

            reader.readAsDataURL(event.target.files[0]);
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
    tab_description() {
        this.tabs_description = (this.tabs_description === 'tabsdesc1' ? 'tabsdesc2' : 'tabsdesc1');
    }
}

interface CreateEditObj {
    'name': any,
    'description': any,
    'status': number
    'session_ids': [number],
    'brand_ids': [number],
    'image': [string],
    'category_ids': [number]
}
