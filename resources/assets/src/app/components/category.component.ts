import {Component, OnInit} from '@angular/core';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';
import {Category} from '../models/category';
import {RestDataService} from '../shared/service/rest-data.service';

declare var jquery: any;
declare var $: any;


@Component({
    templateUrl: 'category.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('600ms', [
                            animate('800ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})
export class CategoryComponent implements OnInit {


    private categories: Category[];
    private sortType: string;
    private messageSearch: string;
    private viewData: any;
    private message: string;
    private messageError: string;
    private messageError_modal: string;
    private button_disable: boolean;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private lang: string;
    private loader: string;
    public paginDiss = 1;
    private fullPagination: any;
    public four: number;
    private editcategories: any;
    public showCategory: any;
    private user_type: string;
    private CreateEditObj: CreateEditObj;
    private language_: any;
    private name: object;
    private no_result: boolean;
    private no_result_mejjige: string;
    private search_params: string;
    private sort_params: string;
    private status: number;
    private objstatus: object;
    private disablestatus: number;
    private degriss: string = 'deg0';
    private tabs: any = 'tabs1';

    constructor(private dataService: RestDataService) {
    }

    ngOnInit() {
        this.paginDiss = 1;
        this.search_params = '';
        this.sort_params = '';
        this.button_disable = false;
        this.pagination = [];
        this.fullPagination = [];
        this.four = 4;
        this.no_result = false;
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('category', this.lang).subscribe((items) => {
            this.categories = items['data'];
            this.sortType = items['sort_type'];
            if (items.message) {
                this.categories = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }

            this.tooltipData();
            $(document).ready(function () {
                $('.search-open').click(function () {
                    $('#search-toggle').toggle(500);
                });
            });
        }, (err) => {
            this.categories = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // tooltype for table

    tooltipData() {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    }


    // Refresh Component

    refresh() {
        this.messageSearch = '';
        this.categories = null;
        this.ngOnInit();
    }

    // Message hide

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    // Update status from table

    changeStatus(status, id) {
        this.disablestatus = id;
        if (status === true) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.objstatus = {
            'status': this.status
        };
        this.dataService.editData('category', this.objstatus, id).subscribe(res => {
            this.message = 'Updated';
            this.degris();
            this.disablestatus = -1;
            this.timOut();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    // View this category

    openModal(id) {
        this.showCategory = null;
        this.messageError = '';
        this.viewData = 'view';
        this.dataService.getData('category/' + id, '').subscribe((items) => {
            this.showCategory = items;
        }, (err) => {
            this.categories = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // Change language

    change_language(lang) {
        if (lang !== localStorage.getItem('activeLanguage')) {
            this.loader = '';
            this.lang = lang;
            localStorage.removeItem('activeLanguage');
            localStorage.setItem('activeLanguage', lang);
            this.dataService.getData('category?page=' + this.paginDiss, lang).subscribe(
                (items) => {
                    this.categories = items['data'];
                    this.loader = 'true';
                    this.tooltipData();
                    if (items.message) {
                        this.categories = [];
                        this.no_result = true;
                        this.no_result_mejjige = items.message
                    }
                }, (err) => {
                    this.categories = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                });
        }
    }


    // Update this category
    onSubmit(form, id) {
        this.loader = '';
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['name_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang
        }
        if (form.editstatus == 1) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.CreateEditObj = {
            'name': this.name,
            'status': this.status
        };
        this.dataService.editData('category', this.CreateEditObj, id).subscribe((res) => {
            this.dataService.getData('category?page=' + this.paginDiss, this.lang).subscribe((items) => {
                this.categories = items['data'];
                this.message = 'Updated';
                this.degris();
                this.loader = 'true';
                $('#myModal .close').click();
                this.tooltipData();
                this.timOut();
            }, (err) => {
                this.categories = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    // open Edit modal this category

    openEditModal(id) {
        this.editcategories = null;
        this.messageError = '';
        this.messageError_modal = '';
        this.viewData = 'edit';
        this.dataService.getData('category/' + id, '').subscribe((items) => {
            this.editcategories = items;
        }, (err) => {
            this.categories = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // open add Modal

    openAddModal(category) {
        $('#formadd').trigger('reset');
        this.showCategory = category;
        this.messageError_modal = '';
        this.viewData = 'add';
    }

    // Create category

    add(form) {
        this.loader = '';
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['nameadd_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang
        }
        if (form.addstatus == null || form.addstatus == false) {
            this.status = 0;
        } else {
            this.status = 1;
        }
        this.CreateEditObj = {
            'name': this.name,
            'status': this.status
        };
        this.dataService.createData('category', this.CreateEditObj).subscribe(
            (item) => (this.dataService.getData('category?page=' + this.paginDiss, this.lang)
                .subscribe((items) => {
                this.categories = items['data'];
                this.no_result = false;
                this.loader = 'true';
                $('#myModal .close').click();
                $('#formadd').trigger('reset');
                this.tooltipData();
                this.message = 'Created';
                this.degris();
                this.timOut();
                if (this.fullPagination.length !== items['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= items['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = items['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                }
                this.loader = 'true';
            }, (err) => {
                this.categories = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })), error => {
                this.messageError_modal = this.dataService.error;
                this.loader = 'true';
            }
        );
    }

    // Delete this category

    deleteId(id) {
        this.loader = '';
        this.lastPage = '';
        this.dataService.getDeleteId(id, 'category').subscribe((items) => (
            this.dataService.getData('category?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
                .subscribe((item) => {
                this.categories = item['data'];
                this.loader = 'true';
                this.message = 'Removed';
                this.degris();
                this.tooltipData();
                this.timOut();
                if (item['message']) {
                    this.categories = [];
                    this.no_result = true;
                    this.no_result_mejjige = item['message'];
                }
                if (this.fullPagination.length !== item['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= item['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = item['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                    this.paginationData(1);
                }
            }, (err) => {
                this.categories = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
        ), error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationget(page);
                } else if (this.fullPagination[page] !== undefined && this.fullPagination[page + 1] === undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationget(page);
                } else if (page === this.recentPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationget(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationget(page);
            }
        }
    }

    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss !== 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss !== this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationget(page) {
        this.loader = '';
        this.paginDiss = page;
        this.lastPage = this.recentPage;
        this.dataService.getData('category?page=' + page + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.categories = items['data'];
                this.loader = 'true';
                this.tooltipData();
            }, (err) => {
                this.categories = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss !== 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('category?page=' + 1 + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.categories = items['data'];
                    this.loader = 'true';
                    this.tooltipData();
                }, (err) => {
                    this.categories = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        } else if (last_first === 'last' && this.paginDiss !== this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('category?page=' + this.lastPage + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.categories = items['data'];
                    this.loader = 'true';
                    this.tooltipData();
                }, (err) => {
                    this.categories = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        }
    }

    // Searching

    search(form) {
        this.search_params = '';
        for (const ress in form) {
            if (form[ress] !== '' && form[ress] !== null) {
                this.search_params += '&' + ress + '=' + form[ress];
            }
        }
        this.button_disable = true;
        this.loader = '';
        this.dataService.getData('category?' + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
            this.fullPagination = [];
            this.pagination = [];
            if (typeof items['message'] === 'undefined') {
                this.categories = items['data'];
            } else {
                this.categories = [];
            }
            this.loader = 'true';
            this.paginDiss = 1;
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }

            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
            if (this.categories[0] == null) {
                this.messageSearch = 'No result';
            } else {
                this.messageSearch = '';
            }

            this.tooltipData();
        }, (err) => {
            this.categories = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // Sorting table

    filter(sort_name, sort) {
        this.sort_params = '';
        this.sort_params += '&' + sort_name + '=' + sort;
        this.loader = '';
        this.dataService.getData('category?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
            this.categories = items['data'];
            this.sortType = items['sort_type'];
            this.loader = 'true';
            this.tooltipData();
        }, (err) => {
            this.categories = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
}

interface CreateEditObj {
    'name': any,
    'status': number
}
