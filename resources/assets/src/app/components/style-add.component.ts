import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {Router} from '@angular/router';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
@Component({
    templateUrl: './style-add.component.html',
    providers: [RestDataService],
    animations: [
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ])
    ]
})
export class StyleAddComponent implements OnInit {
    private lang: string;
    private user_type: string;
    private language_: any;
    private imgActive: string[];
    private sessions: object[];
    public sessionsData: Array<object> = this.sessions;
    private addSessionID: any[];
    private sessionsName: any;
    public value: string[];
    private addobject: object;
    private name: any;
    private message: string;
    private loader: string;
    private imageBase: any;
    private fileEditImage: boolean;
    private fileSize: boolean;
    private myStatus: boolean;
    private recuared_image: number;
    private status: number;
    private tabs: any = 'tabs1';
    private img_path: any = '';
    public img_size: number;
    constructor(private dataService: RestDataService, private router: Router) {
    }

    ngOnInit() {
        this.img_size = this.dataService.img_size_in_byte;
        this.recuared_image = -1;
        this.fileEditImage = true;
        this.imgActive = [];
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('session?type=make_up_by_style', this.lang).subscribe(res => {
            this.sessions = res['data'];
            this.sessionsName = [];
            this.sessions = [];
            if (res['data'] !== undefined) {
                for (let i = 0; i < res['data'].length; i++) {
                    this.sessionsName.push(res['data'][i].translation[0].name);
                    this.sessions.push({'id': res['data'][i].id, 'name': res['data'][i].translation[0].name});
                }
            }
            this.sessionsData = [];
            this.addSessionID = [];
            if (this.sessions) {
                for (let i = 0; i < this.sessions.length; i++) {
                    this.sessionsData.push({'id': this.sessions[i]['id'].toString(), 'text': this.sessions[i]['name']});
                }
            }
            this.statusValidate();
        });
    }

    statusValidate() {
        if (this.addSessionID.length > 0) {
            this.myStatus = true;
        } else {
            this.myStatus = false;
        }
    }

    // pushed product id array
    public addselected(value: any): void {
        this.addSessionID.push(value.id);
        this.statusValidate();
    }

    // removed products id array
    public addremoved(value: any): void {
        this.addSessionID.splice(this.addSessionID.indexOf(value.id), 1);
        this.statusValidate();
    }

    public refreshValue(value: any): void {
        this.value = value;
    }

    public itemsToString(value: Array<any> = []): string {
        return value
            .map((item: any) => {
                return item.text;
            }).join(',');
    }

    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                } else {
                    this.recuared_image = 1;
                    this.fileSize = true;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl(event);
                    let reader = new FileReader();
                    reader.onload = function () {
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.recuared_image = -1;
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.recuared_image = -1;
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path = '';
        }
    }

    onSubmit(form, sessionsid, status) {
        this.loader = '';
        if (form.addstatus == null && status.valid === true) {
            this.status = 1;
        }else if (form.addstatus === false) {
            this.status = 0;
        }else {
            this.status = 1;
        }
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['add_name_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang;
        }
        this.addobject = {
            'name': this.name,
            'image': this.imageBase,
            'session_ids': sessionsid,
            'status': this.status,
            'type': 'make_up_by_style'
        };
        this.dataService.createData('make_up_by_styles', this.addobject).subscribe(items => {
            this.loader = 'true';
            this.router.navigateByUrl('/components/style');
        }, error => {
            this.message = this.dataService.error;
            this.loader = 'true';
        });
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }
}
