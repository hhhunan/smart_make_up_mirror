import {Component, OnInit} from '@angular/core';
import {PlayList} from '../models/playList';
import {RestDataService} from '../shared/service/rest-data.service';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'playList.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('200ms', [
                            animate('500ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})
export class PlayListComponent implements OnInit {

    private playLists: PlayList[];
    private dragIndex: boolean;
    private lang: string;
    private loader: string;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private paginDiss = 1;
    private fullPagination: any;
    public four: number;
    private edit_lpaylist: any;
    private user_type: string;
    private view: boolean;
    private viewData: any;
    private message: string;
    private messageError: string;
    private messageError_modal: string;
    private showCategory: any;
    public stop: any;
    private video_ids: any;
    private videos: any;
    private playList_id: number;
    private send_video_ids: any;
    private CreateEditObj: CreateEditObj;
    private no_result: boolean;
    private no_result_mejjige: string;
    private no_video: boolean = false;
    private no_video_edit: boolean = false;
    private degriss: string = 'deg0';

    constructor(private dataService: RestDataService) {
    }

    ngOnInit() {
        this.dragIndex = true;
        this.view = true;
        this.pagination = [];
        this.fullPagination = [];
        this.four = 4;
        this.no_result = false;
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.dataService.getData('GetPlayList', this.lang).subscribe((items) => {
            this.playLists = items['data'];
            if (items.message) {
                this.playLists = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
        }, (err) => {
            this.playLists = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    go_playlist() {
        this.messageError = '';
        this.message = '';
        this.loader = '';
        this.paginationget(this.paginDiss);
        this.view = true;
    }

    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationget(page);
                } else if (this.fullPagination[page] != undefined && this.fullPagination[page + 1] == undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationget(page);
                } else if (page == this.lastPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationget(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationget(page);
            }
        }
    }

    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss != 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss != this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationget(page) {
        this.loader = '';
        this.paginDiss = page;
        this.lastPage = this.recentPage;
        this.dataService.getData('GetPlayList?page=' + page, this.lang).subscribe(
            (items) => {
                this.playLists = items['data'];
                this.loader = 'true';
                if (items['message']) {
                    this.playLists = [];
                    this.no_result = true;
                    this.no_result_mejjige = 'No items'
                }
            }, (err) => {
                this.playLists = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss != 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('GetPlayList?page=' + 1, this.lang).subscribe(
                (items) => {
                    this.playLists = items['data'];
                    this.loader = 'true';
                },(err) => {
                    this.playLists = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        } else if (last_first === 'last' && this.paginDiss != this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('GetPlayList?page=' + this.lastPage, this.lang).subscribe(
                (items) => {
                    this.playLists = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.playLists = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        }
    }

    edit(id) {
        this.playList_id = id;
        this.message = '';
        this.edit_lpaylist = [];
        this.video_ids = [];
        this.loader = '';
        this.dataService.getData('playlist/' + id, '').subscribe((items) => {
            this.edit_lpaylist = items;
            for (let i = 0; i < items.length; i++) {
                this.video_ids.push(items[i].movie_id);
            }
            this.view = false;
            this.loader = 'true';
        }, (err) => {
            this.playLists = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    open_for_new_video() {
        this.send_video_ids = [];
        this.viewData = 'edit';
        this.messageError_modal = '';
        this.videos = [];
        this.dataService.getData('GetMovieList', '').subscribe((items) => {
            for (let i = 0; i < items.length; i++) {
                if (this.video_ids.indexOf(items[i].id) === -1) {
                    this.videos.push(items[i]);
                }
            }
            if (this.videos.length === 0) {
                this.no_video_edit = true;
            }
        }, (err) => {
            this.playLists = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        })
    }

    edit_video() {
        this.loader = '';
        this.CreateEditObj = {
            'id': this.playList_id,
            'movie_ids': this.send_video_ids,
        }
        this.dataService.createData('playlist', this.CreateEditObj).subscribe((items) => {
            this.dataService.getData('playlist/' + this.playList_id, '').subscribe((item) => {
                this.edit_lpaylist = item;
                this.video_ids = [];
                for (let i = 0; i < item.length; i++) {
                    this.video_ids.push(item[i].movie_id);
                }
                this.loader = 'true';
                this.message = 'Updated';
                this.degris();
                $('#myModal .close').click();
                this.timOut();
            }, (err) => {
                this.playLists = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        })
    }

    new_playlist() {
        this.loader = '';
        this.CreateEditObj = {
            'movie_ids': this.send_video_ids,
        }
        this.dataService.createData('playlist', this.CreateEditObj).subscribe((items) => {
            this.dataService.getData('GetPlayList', this.lang).subscribe((item) => {
                this.playLists = item['data'];
                this.no_result = false;
                if (this.fullPagination.length !== item['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= item['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = item['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                }
                this.loader = 'true';
                this.message = 'Created';
                this.degris();
                $('#myModal .close').click();
                this.timOut();
            }, (err) => {
                this.playLists = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        })
    }

    selected(id) {
        if (this.send_video_ids.indexOf(id) === -1) {
            this.send_video_ids.push(id);
        } else {
            this.send_video_ids.splice(this.send_video_ids.indexOf(id), 1);
        }
    }

    openAddModal() {
        this.send_video_ids = [];
        this.videos = undefined;
        this.viewData = 'add';
        this.messageError_modal = '';
        this.dataService.getData('GetMovieList', '').subscribe((items) => {
            this.videos = items;
            if (this.videos.length === 0) {
                this.no_video = true;
            }
        })
    }

    delete(id) {
        this.loader = '';
        this.dataService.getDeleteId(id, 'playlist').subscribe(
            (items) => {this.dataService.getData('GetPlayList?page=' + this.paginDiss, 'en')
                .subscribe((item) => {
                this.playLists = item['data'];
                if (item['message']) {
                    this.playLists = [];
                    this.no_result = true;
                    this.no_result_mejjige = item['message'];
                }
                if (this.fullPagination.length !== item['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= item['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = item['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                    this.paginationData(1);
                }
                this.loader = 'true';
                this.message = 'Removed';
                this.degris();
                this.timOut();
            }, (err) => {
                this.playLists = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
            }, error => {
                this.messageError = this.dataService.error;
                this.loader = 'true';
            });

    }

    view_video(id) {
        this.viewData = 'view';
        this.showCategory = [];
        this.dataService.getData('movie' + '/' + id, 'en').subscribe((items) => {
            this.showCategory = [items];
        }, (err) => {
            this.playLists = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    delete_video(id, movie_id) {
        this.loader = '';
        this.CreateEditObj = {
            'movie_id': movie_id,
            '_method': 'DELETE'
        }
        this.dataService.createData('playlist/' + id, this.CreateEditObj).subscribe((items) => {
            this.dataService.getData('playlist/' + id, '').subscribe((item) => {
                this.edit_lpaylist = item;
                this.no_video_edit = false;
                this.video_ids = [];
                for (let i = 0; i < item.length; i++) {
                    this.video_ids.push(item[i].movie_id);
                }
                this.loader = 'true';
                this.message = 'Removed';
                this.degris();
                this.timOut();
            }, (err) => {
                this.playLists = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.loader = 'true';
        })
    }

    stop_button() {
        if (this.viewData === 'view') {
            this.stop = document.getElementById('stop');
                this.stop.pause();
        }
    }

    reloadDragaDropEvents() {
        if (this.dragIndex) {
            this.dragIndex = false;
            const parentThis = this;
            const url = '/api/playlist/' + this.playList_id;
            $('#list').jesse({
                onStop: function (new_pos, old_pos, item) {
                    if (new_pos !== old_pos) {
                        parentThis.loader = '';
                        const body = {'movie_id': item[0].id, 'ordering': old_pos, 'new_ordering': new_pos};
                        parentThis.dataService.restAPI(url, body, 'en').subscribe((items) => {
                        });
                        parentThis.edit(parentThis.playList_id);
                    }
                },
                dragBox: 'video_size'
            });
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
}

interface CreateEditObj {
    'movie_id'?: number;
    '_method'?: string,
    'id'?: number,
    'movie_ids'?: any,
}
