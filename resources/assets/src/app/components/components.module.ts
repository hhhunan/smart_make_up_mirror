import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {ProductComponent} from './product.component';
import {BrandComponent} from './brand.component';
import {ImageComponent} from './image.component';
import {DeviceComponent} from './device.component';
import {VideoComponent} from './video.component';
import {SessionComponent} from './session.component';
import {PlayListComponent} from './playList.component';
import {CategoryComponent} from './category.component';
// Components Routing
import {ComponentsRoutingModule} from './components-routing.module';
import {FormsModule} from '@angular/forms';
import {SelectModule} from 'ng2-select';
import {UserComponent} from './user.component';
import {ProductDetailComponent} from './product-detail.component';
import {ProductEditComponent} from './product-edit.component';
import {ProductAddComponent} from './product-add.component';
import {DeviceconfigComponent} from './deviceconfig.component';
import {ViewsessionComponent} from './viewsession.component';
import {LabelsComponent} from './labels.component';
import {LanguageComponent} from './language.components';
import {DeviceupdateComponent} from './deviceupdate.component';
import {MakeupMethodsComponent} from './makeupmethods.component';
import {StyleComponent} from './style.component';
import {StyleSessionsComponent} from './style-sessions.component';
import {StylePlaylistComponent} from './style-playlist.component';
import {StyleVideoComponent} from './style-video.component';
import { StyleAddComponent } from './style-add.component';
import { StyleshowComponent } from './styleshow.component';
import { StyleEditComponent } from './style-edit.component';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        ComponentsRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        SelectModule,
    ],
    declarations: [
        ProductComponent,
        BrandComponent,
        ImageComponent,
        DeviceComponent,
        VideoComponent,
        SessionComponent,
        PlayListComponent,
        CategoryComponent,
        UserComponent,
        ProductDetailComponent,
        ProductEditComponent,
        ProductAddComponent,
        DeviceconfigComponent,
        ViewsessionComponent,
        LabelsComponent,
        LanguageComponent,
        DeviceupdateComponent,
        MakeupMethodsComponent,
        StyleComponent,
        StyleSessionsComponent,
        StylePlaylistComponent,
        StyleVideoComponent,
        StyleAddComponent,
        StyleshowComponent,
        StyleEditComponent,
    ]
})
export class ComponentsModule {
}
