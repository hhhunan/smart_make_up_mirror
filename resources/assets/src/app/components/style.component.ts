import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';

declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'style.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ])
    ]
})
export class StyleComponent implements OnInit {
    private lang: string;
    private user_type: string;
    private language_: any;
    private styles: object[];
    private disablestatus: any;
    private status: number;
    private objstatus: object;
    private message: string;
    private messageError: string;
    private no_result: boolean;
    private no_result_mejjige: string;
    private loader: string;
    paginDiss = 1;
    private fullPagination: any;
    public four: number;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private messageSearch: string;
    private button_disable: boolean;
    private search_params: string;
    private sort_params: string;
    private sort_type_style: string;
    private sort_type_device: string;
    private sort_type_sessions: string;
    private degriss: string = 'deg0';

    constructor(private dataService: RestDataService) {
    }

    ngOnInit() {
        this.pagination = [];
        this.fullPagination = [];
        this.search_params = '';
        this.sort_params = '';
        this.four = 4;
        this.no_result = false;
        this.paginDiss = 1;
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('make_up_by_styles', this.lang).subscribe(items => {
            this.styles = items['data'];
            if (items.message) {
                this.styles = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            this.sort_type_style = items['sort_type_style'];
            this.sort_type_device = items['sort_type_device'];
            this.sort_type_sessions = items['sort_type_session'];
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
            this.tooltipImage();
            $(document).ready(function () {
                $('.search-open').click(function () {
                    $('#search-toggle').toggle(500);
                });
            });
        }, error => {
            this.styles = [];
            this.messageError = this.dataService.error;
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    tooltipImage() {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip({
                animated: 'fade',
                placement: 'bottom',
                html: true
            });
        });
    }

    search(form) {
        this.search_params = '';
        this.button_disable = true;
        this.loader = '';
        this.paginDiss = 1;
        for (const ress in form) {
            if (form[ress] !== '' && form[ress] !== null) {
                this.search_params += '&' + ress + '=' + form[ress];
            }
        }
        this.dataService.getData('make_up_by_styles?' +
            this.search_params +
            this.sort_params, this.lang).subscribe((items) => {
            this.fullPagination = [];
            this.pagination = [];
            this.styles = items['data'];
            this.tooltipImage();
            if (typeof items['message'] === 'undefined') {
                this.styles = items['data'];
            } else {
                this.styles = [];
            }

            this.loader = 'true';
            this.paginDiss = 1;
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }

            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
            if (this.styles[0] == null) {
                this.messageSearch = 'No result';
            } else {
                this.messageSearch = '';
            }
        }, (err) => {
            this.styles = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    filter(sort_name, sort) {
        this.sort_params = '';
        this.sort_params += '&' + sort_name + '=' + sort;
        this.loader = '';
        this.dataService.getData('make_up_by_styles?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.styles = items['data'];
                this.sort_type_style = items['sort_type_style'];
                this.sort_type_device = items['sort_type_device'];
                this.sort_type_sessions = items['sort_type_session'];
                this.loader = 'true';
                this.tooltipImage();
            }, (err) => {
                this.styles = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
    }

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    refresh() {
        this.messageSearch = '';
        this.styles = null;
        this.ngOnInit();
    }

    changeStatus(status, id) {
        this.disablestatus = id;
        if (status === true) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.objstatus = {
            'status': this.status
        };
        this.dataService.editData('make_up_by_styles', this.objstatus, id).subscribe(res => {
            this.message = 'Updated';
            this.degris();
            this.disablestatus = -1;
            this.timOut();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    deleteId(id) {
        this.loader = '';
        this.dataService.getDeleteId(id, 'make_up_by_styles').subscribe(res => {
            this.dataService.getData('make_up_by_styles', this.lang).subscribe(items => {
                this.styles = items['data'];
                this.loader = 'true';
                this.message = 'Removed';
                this.degris();
                this.timOut();
                if (items['message']) {
                    this.styles = [];
                    this.no_result = true;
                    this.no_result_mejjige = items['message'];
                }
                if (this.fullPagination.length !== items['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= items['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = items['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                    this.paginationData(1);
                }
            }, error => {
                this.messageError = this.dataService.error;
                this.loader = 'true';
            });
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    change_language(lang) {
        if (lang !== localStorage.getItem('activeLanguage')) {
            this.loader = '';
            this.lang = lang;
            localStorage.removeItem('activeLanguage');
            localStorage.setItem('activeLanguage', lang);
            this.dataService.getData('make_up_by_styles?page=' + this.paginDiss, lang).subscribe(
                (items) => {
                    this.styles = items['data'];
                    this.loader = 'true';
                    if (items.message) {
                        this.styles = [];
                        this.no_result = true;
                        this.no_result_mejjige = 'No items'
                    }
                    this.tooltipImage();
                }, (err) => {
                    this.styles = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                });
        }
    }

    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationGet(page);
                } else if (this.fullPagination[page] !== undefined && this.fullPagination[page + 1] === undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationGet(page);
                } else if (page === this.recentPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationGet(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationGet(page);
            }
        }
    }

    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss !== 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss !== this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationGet(page) {
        this.loader = '';
        this.paginDiss = page;
        this.lastPage = this.recentPage;
        this.dataService.getData('make_up_by_styles?page=' + page + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.styles = items['data'];
                this.loader = 'true';
            }, (err) => {
                this.styles = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss != 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('make_up_by_styles?page=' + 1 + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.styles = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.styles = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                });
        } else if (last_first === 'last' && this.paginDiss !== this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('make_up_by_styles?page=' + this.lastPage + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.styles = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.styles = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                });
        }
    }

    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
}