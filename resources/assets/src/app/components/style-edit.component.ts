import {Component, OnInit, ElementRef} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
@Component({
    templateUrl: './style-edit.component.html',
    providers: [RestDataService],
    animations: [
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ])
    ]
})
export class StyleEditComponent implements OnInit {
    private lang: string;
    private language_: any;
    private style: any;
    private loader: string;
    private editobject: Editobject;
    private value: any;
    private sessionsData: any[];
    private SessionID: any[] = [];
    private activeSessions: any[] = [];
    private imageBase: any;
    private fileEditImage: boolean;
    private fileSize: boolean;
    private message: string;
    private name: any;
    private selectedLng: number;
    private select: boolean = false;
    private myStatus: boolean;
    private tabs: any = 'tabs1';
    private img_path: any = '';
    public img_size: number;
    constructor(private dataService: RestDataService,
                private route: ActivatedRoute,
                private router: Router,
                private elem: ElementRef) {
    }

    ngOnInit() {
        this.img_size = this.dataService.img_size_in_byte;
        this.lang = localStorage.getItem('activeLanguage');
        this.selectedLng = this.dataService.selectedLanguage;
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('make_up_by_styles/' + this.route.snapshot.params['id'], '').subscribe((items) => {
            this.style = items;
            for (let i = 0; i < this.style.sessions.length; i++) {
                this.activeSessions.push({
                    'id': this.style.sessions[i].id.toString(),
                    'text': this.style.sessions[i].translation[this.selectedLng].name
                });
            }
            for (let i = 0; i < this.activeSessions.length; i++) {
                this.SessionID.push(this.activeSessions[i].id.toString());
            }
            this.statusValidate();
        });
        this.dataService.getData('session?type=make_up_by_style', this.lang).subscribe(res => {
            this.sessionsData = [];
            for (let j = 0; j < res['data'].length; j++) {
                this.sessionsData.push({'id': res['data'][j].id.toString(), 'text': res['data'][j].translation[0].name});
            }
        });
    }

    statusValidate() {
        if (this.SessionID.length > 0) {
            this.myStatus = true;
        } else {
            this.myStatus = false;
        }
    }


    public selected(value: any): void {
        this.SessionID.push(value.id);
        this.select = true;
        this.statusValidate();
    }

    public removed(value: any): void {
        this.SessionID.splice(this.SessionID.indexOf(value.id), 1);
        this.select = true;
        this.statusValidate();
    }

    public refreshValue(value: any): void {
        this.value = value;
    }

    public itemsToString(value: Array<any> = []): string {
        return value
            .map((item: any) => {
                return item.text;
            }).join(',');
    }

    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                } else {
                    this.fileSize = true;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl(event);
                    let reader = new FileReader();
                    reader.onload = function () {
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path = '';
        }
    }

    onSubmit(form, sessionsid) {
        this.loader = '';
        if (form.editstatus == true) {
            form.editstatus = 1;
        } else {
            form.editstatus = 0;
        }
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['add_name_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang;
        }
        let sessions: any;
        if (this.select === true) {
            sessions = sessionsid;
        } else {
            sessions = undefined;
        }
        const files = this.elem.nativeElement.querySelector('#selectFile').files;
        const file = files[0];
        if (file !== undefined) {
            this.editobject = {
                'name': this.name,
                'image': this.imageBase,
                'session_ids': sessions,
                'status': form.editstatus,
                'type': 'make_up_by_style'
            };
        }else {
            this.editobject = {
                'name': this.name,
                'session_ids': sessions,
                'status': form.editstatus,
                'type': 'make_up_by_style'
            };
        }
        this.dataService.editData('make_up_by_styles', this.editobject, this.route.snapshot.params['id']).subscribe(items => {
            this.loader = 'true';
            this.router.navigateByUrl('/components/style');
        }, error => {
            this.message = this.dataService.error;
            this.loader = 'true';
        });
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }
}

interface Editobject {
    'name'?: object,
    'image'?: string,
    'session_ids'?: string[],
    'status'?: number,
    'type': string
}