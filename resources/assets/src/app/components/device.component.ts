import {Component, OnInit} from '@angular/core';
import {Device} from '../models/device';

import {RestDataService} from '../shared/service/rest-data.service';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'device.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'})
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('200ms', [
                            animate('500ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})

export class DeviceComponent implements OnInit {

    private devices: Device[];
    device: any[];
    private viewData: any;
    private message: string;
    private messageError: string;
    private messageError_modal: string;
    private showEdit: boolean;


    public value: string[];
    public current: string;
    public productsName: any;
    public products: any;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    public showDevices: any;
    public showDevicesEdit: any;
    private lang: string;
    paginDiss = 1;
    private fullPagination: any;
    public four: number;
    private user_type: string;
    private messageSearch: string;
    private sort_type_device: string;
    private sort_type_product: string;
    private sort_type_style: string;

    public productID: any[];
    public styleID: any[];
    private selectedLng: number;
    private EditProduct: EditProduct;
    private CreateEditObj: CreateEditObj;
    public exampleData: Array<object> = this.products;
    private productActiveName: Array<object>;
    private addProductID: any[];

    public disabled: boolean = false;
    private loader: string;
    private language_: any;
    private name: object;
    private button_disable: boolean;
    private no_result: boolean;
    private no_result_mejjige: string;
    private search_params: string;
    private sort_params: string;
    private status: number;
    private objstatus: object;
    private disablestatus: number;
    private style: any;
    private styleData: Array<object> = this.style;
    private styleName: any;
    private styleActiveName: any[];
    private editStyle: object;
    private addStylesID: any[];
    private myStatus: any;
    private getStatus: any;
    private fileEditImage: boolean;
    private fileEditImage2: boolean;
    private fileSize: boolean;
    private fileSize2: boolean;
    private imageBase: any;
    private imageBase1: any;
    private recuared_image: number;
    private recuared_image2: number;
    private degriss: string = 'deg0';
    private tabs: any = 'tabs1';
    private img_path_by_live: any = '';
    private img_path_by_session: any = '';
    public img_size: number;
    constructor(private dataService: RestDataService) {
    }

    ngOnInit() {
        this.paginDiss = 1;
        this.img_size = this.dataService.img_size_in_byte;
        this.search_params = '';
        this.sort_params = '';
        this.recuared_image = -1;
        this.recuared_image2 = -1;
        this.button_disable = false;
        this.pagination = [];
        this.fullPagination = [];
        this.four = 4;
        this.no_result = false;
        this.selectedLng = this.dataService.selectedLanguage;
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('device', this.lang).subscribe((items) => {
            this.devices = items['data'];
            this.sort_type_device = items['sort_type_device'];
            this.sort_type_product = items['sort_type_product'];
            this.sort_type_style = items['sort_type_style'];
            if (items.message) {
                this.devices = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
            this.tooltipData();
            $(document).ready(function () {
                $('.search-open').click(function () {
                    $('#search-toggle').toggle(500);
                });
            });
        }, (err) => {
            this.devices = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        this.dataService.getData('GetProductList', this.lang).subscribe((items) => {
            this.products = items;
            this.productsName = [];
            this.products = [];
            for (let i = 0; i < items.length; i++) {
                this.productsName.push(items[i].translation[this.selectedLng].name);
                this.products.push({'id': items[i].id, 'name': items[i].translation[this.selectedLng].name});
            }
        }, (err) => {
            this.devices = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        this.dataService.getData('GetStyleList', this.lang).subscribe((items) => {
            this.style = items;
            this.styleName = [];
            this.style = [];
            for (let i = 0; i < items.length; i++) {
                this.styleName.push(items[i].translation[this.selectedLng].name);
                this.style.push({'id': items[i].id, 'name': items[i].translation[this.selectedLng].name});
            }
        });
    }

    // Tooltype for table
    tooltipData() {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    }

    // message hide
    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    // update product for this device
    public selected(value: any): void {
        this.productID.splice(this.productID.indexOf(value.id), 1);
        this.productID.push(value.id);
        this.loader = '';
        this.EditProduct = {
            'product_ids': this.productID
        };
        this.dataService.createData('device_product/' + this.showDevicesEdit.id, this.EditProduct)
            .subscribe((items) => {
                this.myStatus = items.active;
                this.getStatus = true;
                this.loader = 'true';
            }, error => {
                this.messageError_modal = this.dataService.error;
                this.loader = 'true';
            })
    }

    // update style for this device
    public styleSelected(value: any): void {
        this.styleID.splice(this.styleID.indexOf(value.id), 1);
        this.styleID.push(value.id);
        this.loader = '';
        this.editStyle = {
            'style_ids': this.styleID
        };
        this.dataService.createData('device_style/' + this.showDevicesEdit.id, this.editStyle)
            .subscribe((items) => {
                this.myStatus = items.active;
                this.loader = 'true';
                this.myStatus = items.active;
                this.getStatus = true;
            }, error => {
                this.messageError_modal = this.dataService.error;
                this.loader = 'true';
            })
    }

    // remove product for this device update
    public removed(value: any): void {
        this.loader = '';
        this.productID.splice(this.productID.indexOf(value.id), 1);
        this.dataService.getDeleteId(this.showDevicesEdit.id + '/' + value.id, 'device_product_destroy').subscribe((items) => {
            this.loader = 'true';
            this.myStatus = items.active;
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    // remove style for this device update
    public styleRemoved(value: any): void {
        this.loader = '';
        this.styleID.splice(this.styleID.indexOf(value.id), 1);
        this.dataService.getDeleteId(this.showDevicesEdit.id + '/' + value.id, 'style_device').subscribe((items) => {
            this.loader = 'true';
            this.myStatus = items.active;
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    // pushed product id array
    public addselected(value: any): void {
        this.addProductID.push(value.id);
    }

    // removed products id array
    public addremoved(value: any): void {
        this.addProductID.splice(this.addProductID.indexOf(value.id), 1);
    }

    // pushed style id array
    public addStyleselected(value: any): void {
        this.addStylesID.push(value.id);
    }

    // removed style id array
    public addStyleremoved(value: any): void {
        this.addStylesID.splice(this.addStylesID.indexOf(value.id), 1);
    }

    public refreshValue(value: any): void {
        this.value = value;
    }

    public itemsToString(value: Array<any> = []): string {
        return value
            .map((item: any) => {
                return item.text;
            }).join(',');
    }

    // View this device
    openModal(id) {
        this.showDevices = null;
        this.viewData = 'view';
        this.dataService.getData('device/' + id, '').subscribe((items) => {
            this.showDevices = items;
        }, (err) => {
            this.devices = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    // Update status from table
    changeStatus(status, id) {
        if (status === true) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.objstatus = {
            'status': this.status
        };
        this.disablestatus = id;
        this.dataService.editData('device', this.objstatus, id).subscribe(res => {
            this.message = 'Updated';
            this.degris();
            this.disablestatus = -1;
            this.timOut();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    // Refresh Component
    refresh() {
        this.messageSearch = '';
        this.devices = null;
        this.ngOnInit();
    }

    // Change language
    change_language(lang) {
        if (lang !== localStorage.getItem('activeLanguage')) {
            this.loader = '';
            this.lang = lang;
            localStorage.removeItem('activeLanguage');
            localStorage.setItem('activeLanguage', lang);
            this.dataService.getData('device?page=' + this.paginDiss, lang).subscribe(
                (items) => {
                    this.devices = items['data'];
                    this.loader = 'true';
                    if (items.message) {
                        this.devices = [];
                        this.no_result = true;
                        this.no_result_mejjige = items.message;
                    }
                    this.tooltipData();
                }, (err) => {
                    this.devices = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                });
        }
    }

    // Update this device
    onSubmit(id, form) {
        this.loader = '';
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['name_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang;
        }
        if (form.editstatus == 1) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.CreateEditObj = {
            'name': this.name,
            'status': this.status,
            'type': 1,
            'by_live_image': this.imageBase,
            'by_session_image': this.imageBase1
        };
        this.dataService.editData('device', this.CreateEditObj, id).subscribe((res) => {
            this.dataService.getData('device?page=' + this.paginDiss, this.lang).subscribe((items) => {
                this.devices = items['data'];
                this.message = 'Updated';
                this.degris();
                this.loader = 'true';
                $('#myModal .close').click();
                this.tooltipData();
                this.timOut()
            }, (err) => {
                this.devices = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    // open Edit modal this device
    openEditModal(id) {
        this.img_path_by_session = '';
        this.img_path_by_live = '';
        this.fileSize2 = true;
        this.fileSize = true;
        this.fileEditImage2 = true;
        this.fileEditImage = true;
        this.recuared_image = -1;
        this.recuared_image2 = -1;
        this.messageError = '';
        this.messageError_modal = '';
        this.exampleData = [];
        this.viewData = 'edit';
        this.productID = [];
        this.styleID = [];
        if (this.products) {
            for (let i = 0; i < this.products.length; i++) {
                this.exampleData.push({'id': this.products[i].id.toString(), 'text': this.products[i].name})
            }
        }
        this.styleData = [];
        if (this.style) {
            for (let i = 0; i < this.style.length; i++) {
                this.styleData.push({'id': this.style[i].id.toString(), 'text': this.style[i].name});
            }
        }
        this.styleActiveName = [];
        this.productActiveName = [];
        this.showDevicesEdit = null;
        this.dataService.getData('device/' + id, '').subscribe((items) => {
            this.showDevicesEdit = items;
            for (let k = 0; k < this.showDevicesEdit.products.length; k++) {
                this.productActiveName.push({
                    'id': this.showDevicesEdit.products[k].id,
                    'text': this.showDevicesEdit.products[k].translation[0].name
                });
            }
            for (let k = 0; k < this.showDevicesEdit.styles.length; k++) {
                this.styleActiveName.push({
                    'id': this.showDevicesEdit.styles[k].id,
                    'text': this.showDevicesEdit.styles[k].translation[0].name
                });
                this.styleID.push(this.showDevicesEdit.styles[k].id);
            }
            if (this.styleActiveName.length != 0 && this.productActiveName.length != 0) {
                this.getStatus = true;
            } else {
                this.getStatus = false;
            }
        }, (err) => {
            this.devices = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        this.showEdit = false;
    }

    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path_by_live = '';
                }else {
                    this.recuared_image = 1;
                    this.fileSize = true;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl_live(event);
                    let reader = new FileReader();
                    reader.onload = function(){
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.recuared_image = -1;
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path_by_live = '';
            }
        } else {
            this.recuared_image = -1;
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path_by_live = '';
        }
    }

    onImageChangeFromFileLive(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize2 = false;
                    this.img_path_by_session = '';
                }else {
                    this.recuared_image2 = 1;
                    this.fileSize2 = true;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl_session(event);
                    let reader = new FileReader();
                    reader.onload = function(){
                        _this.imageBase1 = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage2 = true;
            } else {
                this.recuared_image2 = -1;
                this.fileSize2 = true;
                this.fileEditImage2 = false;
                this.img_path_by_session = '';
            }
        } else {
            this.recuared_image2 = -1;
            this.fileSize2 = true;
            this.fileEditImage2 = true;
            this.img_path_by_session = '';
        }
    }

    // open add Modal
    openAddModal() {
        this.img_path_by_session = '';
        this.img_path_by_live = '';
        this.recuared_image = -1;
        this.recuared_image2 = -1;
        this.fileSize2 = true;
        this.fileSize = true;
        this.fileEditImage2 = true;
        this.fileEditImage = true;
        $('#formadd').trigger('reset');
        this.exampleData = [];
        this.styleData = [];
        this.messageError_modal = '';
        this.addProductID = [];
        this.addStylesID = [];
        this.viewData = 'add';
        if (this.products) {
            for (let i = 0; i < this.products.length; i++) {
                this.exampleData.push({'id': this.products[i].id.toString(), 'text': this.products[i].name});
            }
        }
        if (this.style) {
            for (let i = 0; i < this.style.length; i++) {
                this.styleData.push({'id': this.style[i].id.toString(), 'text': this.style[i].name});
            }
        }
    }

    // Create device
    add(form, productid, styleid) {
        this.loader = '';
        this.messageError = '';
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['nameadd_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang
        }

        if (form.addstatus == null || form.addstatus == false) {
            this.status = 0;
        } else {
            this.status = 1;
        }
        this.CreateEditObj = {
            'name': this.name,
            'imei_number': form.imeinumber,
            'status': this.status,
            'type': 1,
            'product_ids': productid,
            'style_ids': styleid,
            'by_live_image': this.imageBase,
            'by_session_image': this.imageBase1
        };
        this.dataService.createData('device', this.CreateEditObj).subscribe((res) => {
            this.dataService.getData('device?page=' + this.paginDiss, this.lang).subscribe((items) => {
                this.devices = items['data'];
                this.no_result = false;
                if (this.fullPagination.length !== items['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= items['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = items['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                }
                this.loader = 'true';
                this.message = 'Created';
                this.degris();
                $('#myModal .close').click();
                $('#formadd').trigger('reset');
                this.tooltipData();
                this.timOut();
            }, (err) => {
                this.devices = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    // Delete this device
    deleteId(id) {
        this.loader = '';
        this.lastPage = '';
        this.dataService.getDeleteId(id, 'device').subscribe((items) => (
            this.dataService.getData('device?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
                .subscribe((item) => {
                    this.devices = item['data'];
                    this.loader = 'true';
                    this.message = 'Removed';
                    this.degris();
                    this.timOut();
                    this.tooltipData();
                    if (item['message']) {
                        this.devices = [];
                        this.no_result = true;
                        this.no_result_mejjige = item['message'];
                    }
                    if (this.fullPagination.length !== item['last_page']) {
                        this.pagination = [];
                        this.fullPagination = [];
                        for (let i = 1; i <= item['last_page']; i++) {
                            this.fullPagination.push(i);
                            this.recentPage = item['last_page'];
                        }
                        if (this.fullPagination.length > 5) {
                            this.pagination = this.fullPagination.slice(0, 5);
                        } else {
                            this.pagination = this.fullPagination;
                        }
                        this.paginationData(1);
                    }
                }, (err) => {
                    this.devices = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        ), error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    // Searching

    search(form) {
        this.search_params = '';
        for (const ress in form) {
            if (form[ress] !== '' && form[ress] !== null) {
                this.search_params += '&' + ress + '=' + form[ress];
            }
        }
        this.button_disable = true;
        this.loader = '';
        this.paginDiss = 1;
        this.dataService.getData('device?' + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.fullPagination = [];
                this.pagination = [];
                this.devices = items['data'];
                this.tooltipData();
                if (typeof items['message'] === 'undefined') {
                    this.devices = items['data'];
                } else {
                    this.devices = [];
                }

                this.loader = 'true';
                this.paginDiss = 1;
                for (let i = 1; i <= items['last_page']; i++) {
                    this.fullPagination.push(i);
                    this.recentPage = items['last_page'];
                }

                if (this.fullPagination.length > 5) {
                    this.pagination = this.fullPagination.slice(0, 5);
                } else {
                    this.pagination = this.fullPagination;
                }
                if (this.devices[0] == null) {
                    this.messageSearch = 'No result';
                } else {
                    this.messageSearch = '';
                }
            });
    }

    // Sorting table

    filter(sort_name, sort) {
        this.sort_params = '';
        this.sort_params += '&' + sort_name + '=' + sort;
        this.loader = '';
        this.dataService.getData('device?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.devices = items['data'];
                this.tooltipData();
                this.sort_type_device = items['sort_type_device'];
                this.sort_type_product = items['sort_type_product'];
                this.sort_type_style = items['sort_type_style'];
                this.loader = 'true';
            }, (err) => {
                this.devices = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
    }


    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationGet(page);
                } else if (this.fullPagination[page] !== undefined && this.fullPagination[page + 1] === undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationGet(page);
                } else if (page === this.recentPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationGet(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationGet(page);
            }
        }
    }

    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss !== 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss !== this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationGet(page) {
        this.loader = '';
        this.paginDiss = page;
        this.lastPage = this.recentPage;
        this.dataService.getData('device?page=' + page + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.devices = items['data'];
                this.loader = 'true';
            }, (err) => {
                this.devices = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })

    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss != 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('device?page=' + 1 + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.devices = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.devices = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        } else if (last_first === 'last' && this.paginDiss !== this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('device?page=' + this.lastPage + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.devices = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.devices = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
    readUrl_live(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.img_path_by_live = event.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }
    readUrl_session(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.img_path_by_session = event.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }else {
            this.img_path_by_session = '';
        }
    }
}

interface CreateEditObj {
    'name': any,
    'imei_number'?: string,
    'status': number,
    'type': number,
    'product_ids'?: [number],
    'style_ids'?: [number],
    'by_live_image'?: any,
    'by_session_image'?: any
}

interface EditProduct {
    'product_ids'?: any
}
