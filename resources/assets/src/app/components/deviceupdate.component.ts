import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {Router} from '@angular/router';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';

declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: './deviceupdate.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ])
    ]
})
export class DeviceupdateComponent implements OnInit {
    private devices: any;
    private user_type: string;
    private viewData: string;
    private lang: string;
    private oneobject: object;
    private oneupdate: any;
    private messageError: string;
    private loader: string;
    private message: string;
    private searchdevice: boolean = false;
    private messageSearch: string;
    private imeinumber: any;
    private hashcode: any;
    private sms: string;
    private closed: boolean = false;
    private no_result_mejjige: string;
    private no_result: boolean;
    private tabs: any = 'tabs1';
    private degriss: string = 'deg0';
    private status_list: any;
    private sortType: string;
    private sortType_status: string;

    constructor(private dataservice: RestDataService,
                private router: Router) {
    }

    ngOnInit() {
        if (localStorage.getItem('user_type') === 'administrator' || localStorage.getItem('user_type') === 'editor') {
            this.no_result = false;
            this.sortType = 'norm';
            this.sortType_status = '';
            this.lang = localStorage.getItem('activeLanguage');
            this.user_type = localStorage.getItem('user_type');
            this.dataservice.getData('devicesUpdates/getDeviceUpdates', this.lang).subscribe(items => {
                this.devices = items['data'];
                this.status_list_filter(items.status_list);
                if (items.message) {
                    this.devices = [];
                    this.no_result = true;
                    this.no_result_mejjige = items.message;
                }
            }, (err) => {
                this.devices = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataservice.error;
            });
        } else {
            this.router.navigateByUrl('/dashboard');
        }
    }

    status_list_filter(list) {
        this.status_list = [];
        for (const res in list) {
            this.status_list.push(res);
        }
    }

    // Message hide
    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    // Open Edit Modal
    openEditModal(id) {
        this.messageError = '';
        this.message = '';
        this.viewData = 'edit';
        this.oneupdate = '';
        this.dataservice.getData('GetDeviceUpdate/' + id, this.lang).subscribe(items => {
            this.oneupdate = items;
        }, (err) => {
            this.devices = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataservice.error;
        })
    }

    // Open View Modal
    openModal(id) {
        this.messageError = '';
        this.message = '';
        this.viewData = 'view';
        this.oneupdate = '';
        this.dataservice.getData('GetDeviceUpdate/' + id, this.lang).subscribe(items => {
            this.oneupdate = items;
        }, error => {
            this.messageError = this.dataservice.error;
            this.loader = 'true'
        });
    }

    // Update this device_update
    update(form, hash, imeinumber) {
        this.loader = '';
        this.closed = true;
        this.oneobject = {
            'imei_number': imeinumber,
            'hash': hash,
            'status': form.editstatus,
            'message': form.message,
            '_method': 'PUT'
        };
        this.sms = 'Updated';
        this.searrch_delete_result();
    }

    // search device
    select(imeinumber, hashcode) {
        this.searchdevice = true;
        this.imeinumber = imeinumber;
        this.hashcode = hashcode;
        this.loader = '';
        if ((hashcode == null || hashcode == '') && (imeinumber != null && imeinumber != '')) {
            this.dataservice.getData('deviceUpdate/searchByImeiNumberByHash?imei_number=' + imeinumber, this.lang)
                .subscribe(res => {
                    if (typeof res['message'] === 'undefined') {
                        this.devices = [res];
                        this.messageSearch = '';
                    }else {
                        this.devices = [];
                        this.messageSearch = 'No result';
                    }
                    this.loader = 'true';
                }, (err) => {this.loader = 'true'; });
        } else if ((imeinumber == null || imeinumber == '') && (hashcode != null && hashcode != '')) {
            this.dataservice.getData('deviceUpdate/searchByImeiNumberByHash?hash=' + hashcode, this.lang)
                .subscribe(res => {
                    if (typeof res['message'] === 'undefined') {
                        this.devices = [res];
                        this.messageSearch = '';
                    }else {
                        this.devices = [];
                        this.messageSearch = 'No result';
                    }
                    this.loader = 'true';
                }, (err) => {this.loader = 'true'; });
        } else if ((imeinumber != null && imeinumber != '') && (hashcode != null && hashcode != '')) {
            this.dataservice.getData('deviceUpdate/searchByImeiNumberByHash?imei_number=' + imeinumber + '&hash=' + hashcode, this.lang)
                .subscribe(res => {
                    if (typeof res['message'] === 'undefined') {
                        this.devices = [res];
                        this.messageSearch = '';
                    }else {
                        this.devices = [];
                        this.messageSearch = 'No result';
                    }
                    this.loader = 'true';
                }, (err) => {this.loader = 'true'; });
        } else  if ((imeinumber == null || imeinumber == '') && (hashcode == null || hashcode == '')) {
            this.dataservice.getData('devicesUpdates/getDeviceUpdates', this.lang)
                .subscribe(res => {
                    this.devices = res['data'];
                    this.messageSearch = '';
                    this.loader = 'true';
                }, (err) => {this.loader = 'true'; });
        }
    }
    refresh() {
        this.messageSearch = '';
        this.devices = null;
        this.ngOnInit();
    }

    // delete this device_update
    deleteId(hash) {
        this.loader = '';
        this.messageError = '';
        this.message = '';
        this.oneobject = {
            'hash': hash,
            '_method': 'DELETE'
        };
        this.sms = 'Removed';
        this.searrch_delete_result();
        // this.timOut();
    }

    // delete data from search table
    searrch_delete_result() {
        if (this.searchdevice == true) {
            this.dataservice.createData('DeviceUpdate', this.oneobject).subscribe(items => {
                if (this.hashcode == null || this.hashcode == '') {
                    this.dataservice.getData('deviceUpdate/searchByImeiNumberByHash?imei_number=' + this.imeinumber, this.lang)
                        .subscribe(res => {
                            this.devices = [res];
                            this.message = this.sms;
                            $('#myModal .close').click();
                            this.timOut();
                            this.degris();
                            this.loader = 'true';
                        });
                } else if (this.imeinumber == null || this.imeinumber == '') {
                    this.dataservice.getData('deviceUpdate/searchByImeiNumberByHash?hash=' + this.hashcode, this.lang)
                        .subscribe(res => {
                            this.devices = [res];
                            this.message = this.sms;
                            if (this.closed == true) {
                                $('#myModal .close').click();
                                this.timOut();
                                this.degris();
                            }
                            this.loader = 'true';
                        });
                } else if ((this.imeinumber != null && this.hashcode != null) || (this.imeinumber != '' && this.hashcode != '')) {
                    this.dataservice.getData('deviceUpdate/searchByImeiNumberByHash?imei_number=' + this.imeinumber + '&hash=' + this.hashcode, this.lang)
                        .subscribe(res => {
                            this.devices = [res];
                            this.message = this.sms;
                            if (this.closed == true) {
                                $('#myModal .close').click();
                                this.degris();
                                this.timOut();
                            }
                            this.loader = 'true';
                        });
                } else {
                }
            }, error => {
                this.messageError = this.dataservice.error;
                this.loader = 'true'
            });
        } else {
            this.dataservice.createData('DeviceUpdate', this.oneobject).subscribe(item => {
                this.dataservice.getData('devicesUpdates/getDeviceUpdates', this.lang).subscribe(items => {
                    this.devices = items['data'];
                    if (this.closed == true) {
                        $('#myModal .close').click();
                        this.timOut();
                        this.degris();
                    }
                    this.loader = 'true';
                    this.message = this.sms;
                });
            }, error => {
                this.messageError = this.dataservice.error;
                this.loader = 'true'
            });
        }
    }

    // delete this device_update all
    deleteAll(imeinumber) {
        this.loader = '';
        this.messageError = '';
        this.message = '';
        this.oneobject = {
            'imei_number': imeinumber,
            '_method': 'DELETE'
        };
        this.sms = 'Removed';
        this.searrch_delete_result();
    }
    filter(sort_name, sort) {
        this.sortType = sort;
        this.loader = '';
        this.dataservice.getData(sort_name + sort, this.lang)
            .subscribe((items) => {
                this.devices = items['data'];
                this.loader = 'true';
            }, (err) => {
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataservice.error;
            });
    }
    filter_status(sort_name, sort) {
        this.loader = '';
        if (sort === this.sortType_status) {
            this.sortType_status = '';
            sort_name = 'devicesUpdates/getDeviceUpdates';
            sort = '';
        } else {
            this.sortType_status = sort;
        }
        this.dataservice.getData(sort_name + sort, this.lang)
            .subscribe((items) => {
                this.devices = items['data'];
                this.loader = 'true';
            }, (err) => {
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataservice.error;
            });
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }

}
