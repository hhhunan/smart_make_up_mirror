import {Component, OnInit, ElementRef, AfterViewInit} from '@angular/core';
import {Image} from '../models/image';
import {RestDataService} from '../shared/service/rest-data.service';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'image.component.html',
    providers: [RestDataService],
    animations : [
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('300ms', [
                            animate('600ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})
export class ImageComponent implements OnInit {
    private images: Image[];
    private messageError: string;
    private message: string;
    private showCategory: any;
    private viewData: any;
    private loader: string;
    private user_type: string;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private paginDiss = 1;
    private fullPagination: any;
    public four: number;
    private fileImage: boolean;
    private fileEditImage: boolean;
    private sortType: string;
    private lang: string;
    private button_disable: boolean;
    private no_result: boolean;
    private no_result_mejjige: string;
    private search_params: string;
    private sort_params: string;
    private image_size: boolean;
    private position: string = 'hide';

    constructor(private dataService: RestDataService,
                private elem: ElementRef) {
    }

    ngOnInit() {
        this.image_size = true;
        this.search_params = '';
        this.sort_params = '';
        this.button_disable = false;
        this.pagination = [];
        this.fullPagination = [];
        this.four = 4;
        this.fileImage = false;
        this.fileEditImage = true;
        this.no_result = false;
        this.paginDiss = 1;
        this.user_type = localStorage.getItem('user_type');
        this.lang = localStorage.getItem('activeLanguage');
        this.dataService.getData('photo', 'en').subscribe((items) => {
            this.images = items['data'];
            this.sortType = items['sort_type'];
            if (items.message) {
                this.images = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
            $(document).ready(function () {
                $('.search-open').click(function () {
                    $('#search-toggle').toggle(500);
                });
            });
        }, (err) => {
            this.images = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });

    }

    // refresh component
    refresh() {
        this.messageSearch = '';
        this.images = null;
        this.ngOnInit();
    }
    // message hide
    timOut() {
        setTimeout(function () {
            this.message = '';
            this.messageError = ''
        }.bind(this), 4000);
    }
    // open view Modal
    open(image) {
        this.showCategory = null;
        this.showCategory = image;
        this.viewData = 'view';
    }
    // Sorting
    filter(sort_name, sort) {
        this.sort_params = '';
        this.sort_params += '&' + sort_name + '=' + sort;
        this.loader = '';
        this.dataService.getData('photo?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
            this.image_size = true;
            this.images = items['data'];
            this.sortType = items['sort_type'];
            this.loader = 'true';
        }, (err) => {
            this.images = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }
    // searching
    search(form) {
        this.search_params = '';
        for (const ress in form) {
            if (form[ress] !== '' && form[ress] !== null) {
                this.search_params += '&' + ress + '=' + form[ress];
            }
        }
        this.button_disable = true;
        this.loader = '';
        this.dataService.getData('photo?' + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
            this.image_size = true;
            this.fullPagination = [];
            this.pagination = [];
            if (typeof items['message'] === 'undefined') {
                this.images = items['data'];
            } else {
                this.images = [];
            }
            this.loader = 'true';
            this.paginDiss = 1;
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }

            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
            if (this.images[0] == null) {
                this.messageSearch = 'No result';
            } else {
                this.messageSearch = '';
            }
        }, (err) => {
            this.images = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }


    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationget(page);
                } else if (this.fullPagination[page] !== undefined && this.fullPagination[page + 1] === undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationget(page);
                } else if (page === this.recentPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationget(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationget(page);
            }
        }
    }


    prev_next(pr_next, form, sort) {
        if (pr_next === 'prev' && this.paginDiss !== 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss !== this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationget(page) {
        this.paginDiss = page;
        this.loader = '';
        this.lastPage = this.recentPage;
        this.dataService.getData('photo?page=' + page + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.image_size = true;
                this.images = items['data'];
                this.loader = 'true';
            }, (err) => {
                this.images = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss !== 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('photo?page=' + 1 + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.image_size = true;
                    this.images = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.images = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        } else if (last_first === 'last' && this.paginDiss !== this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('photo?page=' + this.lastPage + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.image_size = true;
                    this.images = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.images = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        }
    }

    image_size_funct() {
        if (this.image_size) {
            let yourImg = document.getElementsByClassName('scream');
            for (let i = 0; i < yourImg.length; i++) {
                if (yourImg[i]['height'] <= yourImg[i]['width']) {
                    yourImg[i]['width'] = '300';
                } else {
                    yourImg[i]['height'] = '200';
                }
            }
            this.image_size = false;
        }
    }
    animate_modal() {
        this.position = (this.position === 'hide' ? 'show' : 'hide');
    }
    animate_close_modal(event ) {
        if (event.srcElement.id == 'myModal') {
            this.position = (this.position === 'hide' ? 'show' : 'hide');
        }
    }
    close_modal_for_ubdate() {
        $('#myModal .close').click();
        this.position = (this.position === 'hide' ? 'show' : 'hide');
    }
}









