import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: './labels.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('600ms', [
                            animate('800ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})
export class LabelsComponent implements OnInit {
    private lang: string;
    private user_type: string;
    private language_: any;
    private labels: any;
    private viewData: string;
    private translation: any;
    private CreateEditObj: CreateEditObj;
    private loader: string;
    private messageError: string;
    private messageError_modal: string;
    private message: string;
    private showlabel: any;
    private viewlabel: any;
    private onelabel: any;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private paginDiss = 1;
    private fullPagination: any;
    public four: number;
    private no_result: boolean;
    private no_result_mejjige: string;
    private degriss: string = 'deg0';
    private tabs: any = 'tabs1';


    constructor(private dataService: RestDataService) {
    }

    ngOnInit() {
        this.no_result = false;
        this.pagination = [];
        this.fullPagination = [];
        this.four = 4;
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('labels', this.lang).subscribe(items => {
            this.labels = items;
            if (items.message) {
                this.labels = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
        }, (err) => {
            this.labels = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        this.labels = []
    }

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    openModal(id) {
        this.viewData = 'view';
        this.viewlabel = null;
        this.dataService.getData('labels/' + id, this.lang).subscribe(items => {
            this.viewlabel = items;
            for (let i = 0; i < this.viewlabel.length; i++) {
                this.onelabel = this.viewlabel[i];
            }
        });
    }

    openAddModal() {
        this.messageError_modal = '';
        $('#formadd').trigger('reset');
        this.viewData = 'add';
    }

    add(form) {
        this.loader = '';
        this.translation = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['add_name_' + this.language_[i].code];
            this.translation[this.language_[i].code] = name_lang;
        }
        this.CreateEditObj = {
            'input': form.addkeys,
            'translation': this.translation
        };
        this.dataService.createData('labels', this.CreateEditObj).subscribe(item => {
            this.dataService.getData('labels', this.lang).subscribe(items => {
                this.labels = items;
                this.loader = 'true';
                this.no_result = false;
                this.message = 'Created';
                this.degris();
                $('#myModal .close').click();
                $('#formadd').trigger('reset');
                this.timOut();
            }, (err) => {
                this.labels = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    deleteId(id) {
        this.loader = '';
        this.dataService.getDeleteId(id, 'labels').subscribe(items => {
            this.dataService.getData('labels', this.lang).subscribe(item => {
                this.labels = item;
                if (item['message']) {
                    this.labels = [];
                    this.no_result = true;
                    this.no_result_mejjige = item['message'];
                }
                this.loader = 'true';
                this.message = 'Removed';
                this.degris()
                this.timOut();
            }, (err) => {
                this.labels = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true'
        });
    }

    openEdit(id) {
        this.viewData = 'edit';
        this.messageError_modal = ''
        this.showlabel = null;
        this.dataService.getData('labels/' + id, '').subscribe(items => {
            this.showlabel = items;
        }, (error) => {
            this.messageError = this.dataService.error;
        });
    }

    update(form, id) {
        this.loader = '';
        this.translation = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['name_' + this.language_[i].code];
            this.translation[this.language_[i].code] = name_lang;
        }
        this.CreateEditObj = {
            'input': form.input,
            'translation': this.translation
        };
        this.dataService.editData('labels', this.CreateEditObj, id).subscribe(item => {
            this.dataService.getData('labels', this.lang).subscribe(items => {
                this.labels = items;
                this.loader = 'true';
                this.message = 'Updated';
                this.degris();
                $('#myModal .close').click();
                this.timOut();
            }, (err) => {
                this.labels = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true'
        });
    }

    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationget(page);
                } else if (this.fullPagination[page] != undefined && this.fullPagination[page + 1] == undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationget(page);
                } else if (page == this.lastPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationget(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationget(page);
            }
        }
    }

    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss != 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss != this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationget(page) {
        this.loader = '';
        this.paginDiss = page;
        this.lastPage = this.recentPage;
        this.dataService.getData('labels/' + page, this.lang).subscribe(
            (items) => {
                this.labels = items;
                this.loader = 'true';
            }, (error) => {
                this.labels = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
                this.messageError = this.dataService.error
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss != 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('labels/' + 1, this.lang).subscribe(
                (items) => {
                    this.labels = items;
                    this.loader = 'true';
                }, (err) => {
                    this.labels = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        } else if (last_first === 'last' && this.paginDiss != this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('labels/' + this.lastPage, this.lang).subscribe(
                (items) => {
                    this.labels = items;
                    this.loader = 'true';
                }, (err) => {
                    this.labels = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }

}

interface CreateEditObj {
    'input': string,
    'translation': any
}
