import {Component, OnInit, ElementRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RestDataService} from '../shared/service/rest-data.service';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
@Component({
    templateUrl: 'product-edit.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
        state('deg0', style({
            display: 'none',
            opacity: 0
        })),
        state('deg180', style({
            opacity: 1
        })),
        transition('deg0 => deg180', animate(1000, keyframes([
            style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
            style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
            style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
            style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
            style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
        ]))),
        transition('deg180 => deg0', animate(1000, keyframes([
            style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
            style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
            style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
            style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
            style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
        ])))
    ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ]),
        trigger('tabsdescription', [
            transition('tabsdesc1 => tabsdesc2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabsdesc2 => tabsdesc1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ])
    ]
})
export class ProductEditComponent implements OnInit {

    private products: any;
    public images: any;
    private message: string;
    private messageSelect: string;
    private messageError: string;
    private lang: string;
    private language_: any;
    public value: string[];
    public id: any;
    public imgActive: string[];
    private loader: string;

    public sessions: any;
    public sessionsId: any[] = [];
    public sessionsName: any;
    public sessionsData: Array<object> = this.sessions;
    public sessionsActiveName: any;
    public sessionsValue: any[] = [];

    public brands: any;
    public brandsId: any[] = [];
    public brandsName: any;
    public brandsData: Array<object> = this.brands;
    public brandsActiveName: any;
    public brandsValue: any[] = [];

    public category: any;
    public categoryId: any[] = [];
    public categoryName: any;
    public categoryData: Array<object> = this.category;
    public categoryActiveName: any;
    public categoryValue: any[] = [];

    private CreateEditObj: CreateEditObj;
    private EditProduct: EditProduct;
    private name: object;
    private description: object;
    private no_result: boolean;
    private status: number;
    private imageBase: any;
    private fileEditImage: boolean;
    private fileSize: boolean;
    private selectedLng: number;
    private myStatusData: any;
    private myStatus: boolean;
    private degriss: string = 'deg0';
    private tabs: any = 'tabs1';
    private tabs_description: any = 'tabsdesc1';
    private img_path: any = '';
    public img_size: number;
    constructor(private dataService: RestDataService,
                private route: ActivatedRoute,
                private router: Router,
                private elem: ElementRef) {
    }

    ngOnInit() {
        if (localStorage.getItem('user_type') === 'administrator' || localStorage.getItem('user_type') === 'editor') {
            this.img_size = this.dataService.img_size_in_byte;
            this.fileEditImage = true;
            this.fileSize = true;
            this.no_result = false;
            this.selectedLng = this.dataService.selectedLanguage;
            this.lang = localStorage.getItem('activeLanguage');
            this.language_ = JSON.parse(localStorage.getItem('languages'));
            this.dataService.getData('product/' + this.route.snapshot.params['id'], '')
                .subscribe((items) => {
                    this.products = items;
                    this.sessionsValue = [];
                    for (let i = 0; i < this.products.sessions.length; i++) {
                        this.sessionsActiveName = this.products.sessions[i].translation[this.selectedLng].name;
                        this.sessionsValue.push({
                            'id': this.products['sessions'][i].id.toString(),
                            'text': this.products['sessions'][i].translation[this.selectedLng].name
                        });
                    }

                    this.brandsValue = [];
                    for (let i = 0; i < this.products.brands.length; i++) {
                        this.brandsActiveName = this.products.brands[i].translation[this.selectedLng].name;
                        this.brandsValue.push({
                            'id': this.products['brands'][i].id.toString(),
                            'text': this.products['brands'][i].translation[this.selectedLng].name
                        });
                    }

                    this.categoryValue = [];
                    for (let i = 0; i < this.products.category.length; i++) {
                        this.categoryActiveName = this.products.category[i].translation[this.selectedLng].name;
                        this.categoryValue.push({
                            'id': this.products['category'][i].id.toString(),
                            'text': this.products['category'][i].translation[this.selectedLng].name
                        });

                    }
                    if (this.sessionsValue.length > 0 && this.brandsValue.length > 0 && this.categoryValue.length > 0) {
                        this.myStatus = true;
                    } else {
                        this.myStatus = false;
                    }
                });

            this.dataService.getData('GetSessionList', this.lang).subscribe((items) => {
                this.sessions = items;
                this.sessionsName = [];
                this.sessionsData = [];
                this.sessionsId = [];
                for (let i = 0; i < items.length; i++) {
                    this.sessionsName.push(items[i].translation[this.selectedLng].name);
                    this.sessionsData.push({'id': items[i].id.toString(), 'text': items[i].translation[0].name});
                }
            });

            this.dataService.getData('GetBrandList', this.lang).subscribe((items) => {
                this.brands = items;
                this.brandsName = [];
                this.brandsData = [];
                this.brandsId = [];
                for (let i = 0; i < items.length; i++) {
                    this.brandsName.push(items[i].translation[this.selectedLng].name);
                    this.brandsData.push({
                        'id': items[i].id.toString(),
                        'text': items[i].translation[this.selectedLng].name
                    });
                }
            });

            this.dataService.getData('GetCategoryList', this.lang).subscribe((items) => {
                this.category = items;
                this.categoryName = [];
                this.categoryData = [];
                this.categoryId = [];
                for (let i = 0; i < items.length; i++) {
                    this.categoryName.push(items[i].translation[this.selectedLng].name);
                    this.categoryData.push({
                        'id': items[i].id.toString(),
                        'text': items[i].translation[this.selectedLng].name
                    });
                }
            });

        } else {
            this.router.navigateByUrl('/components/products');
        }
    }

    timOut() {
        setTimeout(function () {
            this.messageSelect = '';
            this.messageError = ''
            this.degris();
        }.bind(this), 4000);
    }


    public selectedSessions(value: any): void {
        this.sessionsId.splice(this.sessionsId.indexOf(value.id), 1);
        this.sessionsId.push(value.id);
        this.EditProduct = {
            'session_ids': this.sessionsId
        };
        this.loader = '';
        this.dataService.createData('product_session/' + this.products['id'], this.EditProduct)
            .subscribe((items) => {
                this.messageSelect = 'Updated';
                this.loader = 'true';
                this.myStatusData = items.active;
                this.myStatus = true;
                this.timOut();
            }, error => {
                this.messageError = this.dataService.error;
                this.loader = 'true';
            })
    }

    public removedSessions(value: any): void {
        this.sessionsId.splice(this.sessionsId.indexOf(value.id), 1);
        this.loader = '';
        this.dataService.getDeleteId(value.id + '/' + this.products['id'], 'product_session').subscribe((items) => {
            this.messageSelect = 'Removed';
            this.loader = 'true';
            this.myStatusData = items.active;
            this.timOut();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    public selectedBrands(value: any): void {
        this.brandsId.splice(this.brandsId.indexOf(value.id), 1);
        this.brandsId.push(value.id);
        this.loader = '';
        this.EditProduct = {
            'brand_ids': this.brandsId
        };
        this.dataService.createData('product_brand/' + this.products['id'], this.EditProduct)
            .subscribe((items) => {
                this.messageSelect = 'Updated';
                this.loader = 'true';
                this.myStatusData = items.active;
                this.myStatus = true;
                this.timOut();
            }, error => {
                this.messageError = this.dataService.error;
                this.loader = 'true';
            })
    }

    public removedBrands(value: any): void {
        this.brandsId.splice(this.brandsId.indexOf(value.id), 1);
        this.loader = '';
        this.dataService.getDeleteId(value.id + '/' + this.products['id'], 'product_brand').subscribe((items) => {
            this.messageSelect = 'Removed';
            this.loader = 'true';
            this.myStatusData = items.active;
            this.timOut();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    public selectedCategory(value: any): void {
        this.categoryId.splice(this.categoryId.indexOf(value.id), 1);
        this.categoryId.push(value.id);
        this.loader = '';
        this.EditProduct = {
            'category_ids': this.categoryId
        };
        this.dataService.createData('product_category/' + this.products['id'], this.EditProduct)
            .subscribe((items) => {
                this.messageSelect = 'Updated';
                this.loader = 'true';
                this.myStatusData = items.active;
                this.myStatus = true;
                this.timOut();
            }, error => {
                this.messageError = this.dataService.error;
                this.loader = 'true';
            })
    }

    public removedCategory(value: any): void {
        this.categoryId.splice(this.categoryId.indexOf(value.id), 1);
        this.loader = '';
        this.dataService.getDeleteId(value.id + '/' + this.products['id'], 'product_category').subscribe((items) => {
            this.messageSelect = 'Removed';
            this.loader = 'true';
            this.myStatusData = items.active;
            this.timOut();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    public refreshValue(value: any): void {
        this.value = value;
    }
    onSubmit(id, form) {
        this.loader = '';
        this.messageError = '';
        this.name = {};
        this.description = {};
        for (let i = 0; i < this.language_.length; i++) {
            const value_lang = form['description_' + this.language_[i].code];
            const name_lang = form['name_' + this.language_[i].code];
            this.description[this.language_[i].code] = value_lang;
            this.name[this.language_[i].code] = name_lang;
        }
        if (form.editstatus == 1) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        const files = this.elem.nativeElement.querySelector('#selectFile').files;
        const file = files[0];
        if (file !== undefined) {
            this.CreateEditObj = {
                'name': this.name,
                'description': this.description,
                'status': this.status,
                'image': this.imageBase,
            };
        } else {
            this.CreateEditObj = {
                'name': this.name,
                'description': this.description,
                'status': this.status,
            };
        }

        this.dataService.editData('product', this.CreateEditObj, id).subscribe((res) => {
            this.loader = 'true';
            this.message = 'Updated';
            this.degris();
            this.timOut();
            this.ngOnInit();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });


    }
    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                } else {
                    this.fileSize = true;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl(event);
                    let reader = new FileReader();
                    reader.onload = function () {
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path = '';
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
    tab_description() {
        this.tabs_description = (this.tabs_description === 'tabsdesc1' ? 'tabsdesc2' : 'tabsdesc1');
    }
    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }
}

interface CreateEditObj {
    'name': any,
    'description': any,
    'status': number,
    'image'?: any,
}

interface EditProduct {
    'session_ids'?: any,
    'brand_ids'?: any,
    'category_ids'?: any,
    'device_ids'?: any
}
