import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {ActivatedRoute} from '@angular/router';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';
@Component({
    templateUrl: './viewsession.component.html',
    providers: [RestDataService],
    animations: [
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ]),
        trigger('tabsdescription', [
            transition('tabsdesc1 => tabsdesc2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabsdesc2 => tabsdesc1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('200ms', [
                            animate('500ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})
export class ViewsessionComponent implements OnInit {
    private session: any;
    private lang: string;
    private user_type: string;
    private video: any;
    private viewdata: string;
    private objlength: any;
    private stop: any;
    private error: any;
    private language: any;
    private sesions_type: any;
    private position: string = 'hide';
    private tabs: any = 'tabs1';
    private tabs_description: any = 'tabsdesc1';

    constructor(private dataService: RestDataService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.lang = localStorage.getItem('lang');
        this.user_type = localStorage.getItem('user_type');
        this.dataService.getData('session/' + this.route.snapshot.params['id'], '').subscribe((items) => {
            this.session = items;
            this.sesions_type = items.type;
        }, error => {
            this.error = this.dataService.error
        });
        this.dataService.getData('language', 'en').subscribe((items) => {
            this.language = items;
        });
    }

    view_video(id) {
        this.viewdata = 'view';
        this.video = {};
        this.objlength = Object.keys(this.video).length;
        this.dataService.getData('movie' + '/' + id, 'en').subscribe((items) => {
            this.video = items;
            this.objlength = Object.keys(this.video).length;
        }, error => {
            this.error = this.dataService.error
        });
    }

    stop_button() {
        this.stop = document.getElementById('stop');
        this.stop.pause();
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
    tab_description() {
        this.tabs_description = (this.tabs_description === 'tabsdesc1' ? 'tabsdesc2' : 'tabsdesc1');
    }

}
