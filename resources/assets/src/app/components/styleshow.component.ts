import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {ActivatedRoute} from '@angular/router';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
@Component({
    templateUrl: './styleshow.component.html',
    providers: [RestDataService],
    animations: [
        trigger('flyInOut',  [
            state('in', style({width: 120, transform: 'translateX(0)', opacity: 1})),
            transition('void => *', [
                style({width: '150px', transform: 'translateX(0px)', opacity: 0}),
                group([
                    animate('0.7s 700ms ease', style({
                        transform: 'translateX(0)',
                        width: '100%'
                    })),
                    animate('0.9s ease', style({
                        opacity: 1
                    }))
                ])
            ]),
            transition('* => void', [
                group([
                    animate('0.5s ease', style({
                        transform: 'translateX(0px)',
                        width: 150
                    })),
                    animate('0.9s 0.2s ease', style({
                        opacity: 0
                    }))
                ])
            ])
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ])
    ]
})
export class StyleshowComponent implements OnInit {

    private styles: any;
    private lang: string;
    private language_: any;
    private amimate: any;
    private tabs: any = 'tabs1';
    constructor(private dataService: RestDataService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.amimate = -1;
        this.lang = localStorage.getItem('activeLanguage');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('make_up_by_styles/' + this.route.snapshot.params['id'], '')
            .subscribe((items) => {
                this.styles = items;
                setTimeout(function () {
                    this.amimate = this.route.snapshot.params['id']
                }.bind(this), 1600);
            });
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
}
