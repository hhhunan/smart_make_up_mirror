import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {Session} from '../models/session';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'style-sessions.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ])
    ]
})
export class StyleSessionsComponent implements OnInit {
    private sessions: Session[];
    private lang: string;
    private loader: string;
    private message: string;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private user_type: string;
    private paginDiss = 1;
    private fullPagination: any;
    public four: number;
    private viewData: string;
    private onesession: any;
    private playlist: any;
    private playlistID: number[];
    private CreateEditObj: CreateEditObj;
    private showsession: any;
    private messageError: string;
    private messageError_modal: string;
    public language: any;
    private description: object;
    private name: object;
    private no_result: boolean;
    private no_result_mejjige: string;
    private no_playlist: boolean;
    private language_: any;
    private status: number;
    private degriss: string = 'deg0';
    private tabs: any = 'tabs1';
    constructor(private dataService: RestDataService) {
    }

    ngOnInit() {
        this.pagination = [];
        this.no_playlist = false;
        this.fullPagination = [];
        this.four = 4;
        this.no_result = false;
        this.user_type = localStorage.getItem('user_type');
        this.lang = localStorage.getItem('activeLanguage');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.dataService.getData('session?type=make_up_by_style', this.lang).subscribe((items) => {
            this.sessions = items['data'];
            if (items.message) {
                this.sessions = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
        }, (err) => {
            this.sessions = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        this.playlistID = [];
        this.dataService.getData('GetPlayList?page=all', 'en').subscribe(items => {
            this.playlist = items;
            if (!items.message) {
                for (let i = 0; i < this.playlist.length; i++) {
                    this.playlistID.push(this.playlist[i].id);
                }
            }else {
                this.no_playlist = true;
            }
        }, (err) => {
            this.playlist = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }
    openAddModal() {
        this.messageError_modal = '';
        this.viewData = 'add';
        $('#formadd').trigger('reset');
    }

    openedit(id) {
        this.viewData = 'edit';
        this.messageError_modal = '';
        this.showsession = null;
        this.dataService.getData('session/' + id, '').subscribe(items => {
            this.showsession = items;
        }, (err) => {
            this.showsession = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    change_language(lang) {
        if (lang !== localStorage.getItem('activeLanguage')) {
            this.loader = '';
            this.lang = lang;
            localStorage.removeItem('activeLanguage');
            localStorage.setItem('activeLanguage', lang);
            this.dataService.getData('session?page=' + this.paginDiss + '&type=make_up_by_style', lang).subscribe(
                (items) => {
                    this.sessions = items['data'];
                    this.loader = 'true';
                    if (items.message) {
                        this.sessions = [];
                        this.no_result = true;
                        this.no_result_mejjige = items.message;
                    }
                }, (err) => {
                    this.showsession = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                });
        }
    }

    onSubmit(form, id) {
        this.loader = '';
        this.description = {};
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            let value_lang = form['description_' + this.language_[i].code];
            let name_lang = form['name_' + this.language_[i].code];
            this.description[this.language_[i].code] = value_lang;
            this.name[this.language_[i].code] = name_lang;
        }
        if (form.editstatus == 1) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        this.CreateEditObj = {
            'name': this.name,
            'play_list_id': form.addplaylistedit,
            'status': this.status,
            'description': this.description,
            'type': 'make_up_by_style'
        };
        this.dataService.editData('session', this.CreateEditObj, id).subscribe((item) => {
            this.dataService.getData('session?page=' + this.paginDiss + '&type=make_up_by_style', this.lang).subscribe((items) => {
                this.sessions = items['data'];
                if (this.fullPagination.length !== items['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= items['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = items['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                    this.paginationData(1);
                }
                $('#myModal .close').click();
                this.message = 'Updated';
                this.degris();
                this.loader = 'true';
                this.timOut();
            }, (err) => {
                this.sessions = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    add(form) {
        this.loader = '';
        this.description = {};
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            let value_lang = form['description_' + this.language_[i].code];
            let name_lang = form['add_name_' + this.language_[i].code];
            this.description[this.language_[i].code] = value_lang;
            this.name[this.language_[i].code] = name_lang;
        }
        if (form.addstatus == null || form.addstatus == false) {
            this.status = 0;
        } else {
            this.status = 1;
        }
        this.CreateEditObj = {
            'name': this.name,
            'play_list_id': form.addplaylist,
            'status': this.status,
            'description': this.description,
            'type': 'make_up_by_style'
        };
        this.dataService.createData('session', this.CreateEditObj).subscribe(item => {
            this.dataService.getData('session?page=' + this.paginDiss + '&type=make_up_by_style', this.lang).subscribe((items) => {
                this.sessions = items['data'];
                this.no_result = false;
                if (this.fullPagination.length !== items['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= items['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = items['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                    this.paginationData(1);
                }
                $('#myModal .close').click();
                this.message = 'Created';
                this.degris();
                this.loader = 'true';
                this.timOut();
            }, (err) => {
                this.sessions = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    deleteId(id) {
        this.lastPage = '';
        this.paginDiss = 1;
        this.loader = '';
        this.dataService.getDeleteId(id, 'session').subscribe((items) => {
            this.dataService.getData('session?page=' + this.paginDiss + '&type=make_up_by_style', this.lang).subscribe((item) => {
                this.sessions = item['data'];
                if (item['message']) {
                    this.sessions = [];
                    this.no_result = true;
                    this.no_result_mejjige = item['message'];
                }
                if (this.fullPagination.length !== item['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= item['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = item['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                    this.paginationData(1);
                }
                this.message = 'Removed';
                this.degris();
                this.loader = 'true';
                this.timOut();
            }, (err) => {
                this.sessions = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    openModal(onesession) {
        this.viewData = 'view';
        this.onesession = [onesession];
    }

    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationget(page);
                } else if (this.fullPagination[page] != undefined && this.fullPagination[page + 1] == undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationget(page);
                } else if (page == this.lastPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationget(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationget(page);
            }
        }
    }

    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss != 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss != this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationget(page) {
        this.loader = '';
        this.paginDiss = page;
        this.lastPage = this.recentPage;
        this.dataService.getData('session?page=' + page + '&type=make_up_by_style', this.lang).subscribe(
            (items) => {
                this.sessions = items['data'];
                this.loader = 'true';
            }, (err) => {
                this.sessions = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss != 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('session?page=' + 1 + '&type=make_up_by_style', this.lang).subscribe(
                (items) => {
                    this.sessions = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.sessions = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        } else if (last_first === 'last' && this.paginDiss != this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('session?page=' + this.lastPage + '&type=make_up_by_style', this.lang).subscribe(
                (items) => {
                    this.sessions = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.sessions = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
}
interface CreateEditObj {
    'name': any,
    'play_list_id': number,
    'status': number,
    'description': any,
    'type': any,
}

