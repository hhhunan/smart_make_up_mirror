import {Component, OnInit, ElementRef} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {Video} from '../models/video';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group, query, stagger
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'style-video.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('input_animations', [
            transition('* => *', [
                query('input', style({width: '0%'})),
                query('input',
                    stagger('200ms', [
                            animate('500ms', style({width: '100%'})),
                        ]
                    ))
            ])
        ])
    ]
})
export class StyleVideoComponent implements OnInit {
    private videos: Video[];
    private lang: string;
    private user_type: string;
    private sortType: string;
    private showVideo: any;
    private viewData: any;
    private loader: string;
    private messageError: string;
    private messageError_modal: string;
    private message: string;
    private editcategories: any;
    public pagination: any;
    public recentPage: any;
    public lastPage: any;
    private paginDiss = 1;
    private fullPagination: any;
    public four: number;
    public video_valid: boolean;
    public video_valid_messige: boolean;
    public video_size: number = 50 * 1024 * 1024;
    public video_size_valid: boolean;
    public video_duration: any;
    public stop: any;
    public stop1: any;
    private messageSearch: string;
    private button_disable: boolean;
    private no_result: boolean;
    private no_result_mejjige: string;
    private search_params: string;
    private sort_params: string;
    private status: any;
    private degriss: string = 'deg0';

    constructor(private dataService: RestDataService,
                private elem: ElementRef) {
    }

    ngOnInit() {
        this.search_params = '';
        this.sort_params = '';
        this.button_disable = false;
        this.pagination = [];
        this.fullPagination = [];
        this.four = 4;
        this.paginDiss = 1;
        this.video_valid = false;
        this.no_result = false;
        this.video_valid_messige = false;
        this.video_size_valid = false;
        this.video_duration = '';
        this.lang = localStorage.getItem('activeLanguage');
        this.user_type = localStorage.getItem('user_type');
        this.dataService.getData('movie', this.lang).subscribe((items) => {
            this.videos = items['data'];
            if (items.message) {
                this.videos = [];
                this.no_result = true;
                this.no_result_mejjige = items.message;
            }
            this.sortType = items['sort_type'];
            for (let i = 1; i <= items['last_page']; i++) {
                this.fullPagination.push(i);
                this.recentPage = items['last_page'];
            }
            if (this.fullPagination.length > 5) {
                this.pagination = this.fullPagination.slice(0, 5);
            } else {
                this.pagination = this.fullPagination;
            }
            $(document).ready(function () {
                $('.search-open').click(function () {
                    $('#search-toggle').toggle(500);
                });
            });
        }, (err) => {
            this.videos = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }
    openPlay(id) {
        this.viewData = 'play';
        this.showVideo = [];
        this.dataService.getData('movie/' + id, '').subscribe((items) => {
            this.showVideo = [items];
        }, (err) => {
            this.showVideo = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    open(id) {
        this.viewData = 'view';
        this.showVideo = [];
        this.dataService.getData('movie/' + id, '').subscribe((items) => {
            this.showVideo = [items];
        }, (err) => {
            this.showVideo = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }

    openedit(id) {
        this.viewData = 'edit';
        this.messageError = '';
        this.messageError_modal = '';
        this.editcategories = [];
        this.video_valid_messige = false;
        this.video_size_valid = false;
        this.video_valid = false;
        this.dataService.getData('movie/' + id, '').subscribe((items) => {
            this.editcategories = [items];
        }, (err) => {
            this.editcategories = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
    }
    refresh() {
        this.messageSearch = '';
        this.videos = null;
        this.ngOnInit();
    }

    savebutton(form, id) {
        this.loader = '';
        let files = this.elem.nativeElement.querySelector('#selecteditFile').files;
        let formData = new FormData();
        let file = files[0];
        if (file) {
            formData.append('video', file, file.name);
            formData.append('duration', this.video_duration);
        }
        if (form.editstatus == 1) {
            formData.append('status', '1');
        } else {
            formData.append('status', '0');
        }
        formData.append('name', form.editname);
        formData.append('_method', 'PUT');
        formData.append('type', '1');
        this.dataService.createData('movie/' + id, formData).subscribe((res) => {
            (this.dataService.getData('movie?page=' + this.paginDiss, 'en').subscribe((items) => {
                this.videos = items['data'];
                this.loader = 'true';
                this.message = 'Updated';
                this.degris();
                $('#myModal .close').click();
                this.timOut();
            }, (err) => {
                this.videos = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            }));
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }

    addopen() {
        $('#formadd').trigger('reset');
        this.viewData = 'add';
        this.messageError = '';
        this.messageError_modal = '';
        this.video_valid_messige = false;
        this.video_size_valid = false;
        this.video_valid = false;
    }

    filter(sort_name, sort) {
        this.sort_params = '';
        this.sort_params += '&' + sort_name + '=' + sort;
        this.loader = '';
        this.dataService.getData('movie?page=' + this.paginDiss + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.videos = items['data'];
                this.sortType = items['sort_type'];
                this.loader = 'true';
            }, (err) => {
                this.videos = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
    }


    refreshPage() {
        this.messageSearch = '';
        $('#form')[0].reset();
        this.videos = null;
        this.ngOnInit();
    }

    search(form) {
        this.search_params = '';
        for (const ress in form) {
            if (form[ress] !== '' && form[ress] !== null) {
                this.search_params += '&' + ress + '=' + form[ress];
            }
        }
        this.button_disable = true;
        this.loader = '';
        this.dataService.getData('movie?' + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.fullPagination = [];
                this.pagination = [];
                if (typeof items['message'] === 'undefined') {
                    this.videos = items['data'];
                } else {
                    this.videos = [];
                }
                this.loader = 'true';
                this.paginDiss = 1;
                for (let i = 1; i <= items['last_page']; i++) {
                    this.fullPagination.push(i);
                    this.recentPage = items['last_page'];
                }

                if (this.fullPagination.length > 5) {
                    this.pagination = this.fullPagination.slice(0, 5);
                } else {
                    this.pagination = this.fullPagination;
                }
                if (this.videos[0] == null) {
                    this.messageSearch = 'No result';
                } else {
                    this.messageSearch = '';
                }
            }, (err) => {
                this.videos = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            });
    }

    deleteId(id) {
        this.loader = '';
        this.lastPage = '';
        this.dataService.getDeleteId(id, 'movie').subscribe((items) => (
            this.dataService.getData('movie?page=' +
                this.paginDiss +
                this.search_params
                + this.sort_params, this.lang).subscribe((item) => {
                this.videos = item['data'];
                if (item['message']) {
                    this.videos = [];
                    this.no_result = true;
                    this.no_result_mejjige = item['message'];
                }
                this.loader = 'true';
                this.message = 'Removed';
                this.degris();
                this.timOut();
                if (this.fullPagination.length !== item['last_page']) {
                    this.pagination = [];
                    this.fullPagination = [];
                    for (let i = 1; i <= item['last_page']; i++) {
                        this.fullPagination.push(i);
                        this.recentPage = item['last_page'];
                    }
                    if (this.fullPagination.length > 5) {
                        this.pagination = this.fullPagination.slice(0, 5);
                    } else {
                        this.pagination = this.fullPagination;
                    }
                    this.paginationData(1);
                }
            }, (err) => {
                this.videos = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
        ), error => {

            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    add(form) {
        let files = this.elem.nativeElement.querySelector('#selectFile').files;
        let formData = new FormData();
        let file = files[0];
        this.loader = '';
        if (form.addstatus == null || form.addstatus == false) {
            this.status = 0;
        } else {
            this.status = 1;
        }
        formData.append('video', file, file.name);
        formData.append('name', form.addname);
        formData.append('status', this.status);
        formData.append('duration', this.video_duration);
        formData.append('type', '1');
        this.dataService.createData('movie', formData).subscribe(
            (items) => (this.dataService.getData('movie?page=' + this.paginDiss, 'en')
                .subscribe((items) => {
                    this.videos = items['data'];
                    if (this.fullPagination.length !== items['last_page']) {
                        this.pagination = [];
                        this.fullPagination = [];
                        for (let i = 1; i <= items['last_page']; i++) {
                            this.fullPagination.push(i);
                            this.recentPage = items['last_page'];
                        }
                        if (this.fullPagination.length > 5) {
                            this.pagination = this.fullPagination.slice(0, 5);
                        } else {
                            this.pagination = this.fullPagination;
                        }
                    }
                    this.message = 'Created';
                    this.degris();
                    this.loader = 'true';
                    this.no_result = false;
                    $('#myModal .close').click();
                    this.timOut();
                }, (err) => {
                    this.videos = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })), (error) => {
                this.messageError_modal = this.dataService.error;
                this.loader = 'true';
            }
        );
    }

    paginationData(page) {
        if (page !== this.paginDiss) {
            this.loader = '';
            if (this.four < page) {
                if (this.fullPagination[page] && this.fullPagination[page + 1]) {
                    this.pagination = this.fullPagination.slice((page - 3), (page + 2));
                    this.paginationget(page);
                } else if (this.fullPagination[page] !== undefined && this.fullPagination[page + 1] === undefined) {
                    this.pagination = this.fullPagination.slice((page - 4), (page + 1));
                    this.paginationget(page);
                } else if (page === this.recentPage) {
                    this.pagination = this.fullPagination.slice((page - 5), (page));
                    this.paginationget(page);
                }
            } else {
                this.pagination = this.fullPagination.slice(0, 5);
                this.paginationget(page);
            }
        }
    }


    prev_next(pr_next) {
        if (pr_next === 'prev' && this.paginDiss !== 1) {
            this.paginationData(this.paginDiss - 1);
        } else if (pr_next === 'next' && this.paginDiss !== this.recentPage) {
            this.paginationData(this.paginDiss + 1);
        }
    }

    paginationget(page) {
        this.paginDiss = page;
        this.loader = ''
        this.lastPage = this.recentPage;
        this.dataService.getData('movie?page=' + page + this.search_params + this.sort_params, this.lang)
            .subscribe((items) => {
                this.videos = items['data'];
                this.loader = 'true';
            }, (err) => {
                this.videos = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = this.dataService.error;
            })
    }

    last_first_page(last_first) {
        if (last_first === 'first' && this.paginDiss !== 1) {
            this.loader = '';
            this.pagination = this.fullPagination.slice(0, 5);
            this.lastPage = this.recentPage;
            this.paginDiss = 1;
            this.dataService.getData('movie?page=' + 1 + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.videos = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.videos = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        } else if (last_first === 'last' && this.paginDiss !== this.recentPage) {
            this.loader = '';
            if (this.recentPage > 5) {
                this.pagination = this.fullPagination.slice((this.recentPage - 5), (this.recentPage));
            } else {
                this.pagination = this.fullPagination;
            }
            this.lastPage = this.recentPage;
            this.paginDiss = this.lastPage;
            this.dataService.getData('movie?page=' + this.lastPage + this.search_params + this.sort_params, this.lang)
                .subscribe((items) => {
                    this.videos = items['data'];
                    this.loader = 'true';
                }, (err) => {
                    this.videos = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                })
        }
    }

    valid(event) {
        if (event.srcElement.files[0] !== undefined) {
            let hthis = this;
            const file = event.srcElement.files[0].type
            if (file !== 'video/mp4' && file !== 'video/avi') {
                this.video_valid = false;
                this.video_valid_messige = true;
            } else {
                this.duration(event.srcElement.files[0], function (duration) {
                    hthis.video_duration = duration;
                });
                if (event.srcElement.files[0].size > this.video_size) {
                    this.video_size_valid = true;
                    this.video_valid = false;
                } else {
                    this.video_size_valid = false;
                    this.video_valid = true;
                }
                this.video_valid_messige = false;
            }
        }
    }

    valid_edit(event) {
        if (event.srcElement.files[0] !== undefined) {
            let hthis = this;
            const file = event.srcElement.files[0].type;
            if (file !== 'video/mp4' && file !== 'video/avi') {
                this.video_valid = true;
                this.video_valid_messige = true;
            } else {
                this.duration(event.srcElement.files[0], function (duration) {
                    hthis.video_duration = duration;
                });
                if (event.srcElement.files[0].size > this.video_size) {
                    this.video_size_valid = true;
                    this.video_valid = true;
                } else {
                    this.video_size_valid = false;
                    this.video_valid = false;
                }
                this.video_valid_messige = false;
            }
        } else {
            this.video_size_valid = false;
            this.video_valid = false;
            this.video_valid_messige = false;
        }
    }

    duration(event, call) {
        const reader = new FileReader();
        reader.onload = function () {
            const aud = new Audio(reader.result);
            aud.onloadedmetadata = function () {
                const decimalMinuteString = aud.duration;
                const date = new Date(null);
                date.setSeconds(decimalMinuteString);
                const result = date.toISOString().substr(11, 8);
                call(result);
            };
        };
        reader.readAsDataURL(event);
    }

    stop_button() {
        if (this.viewData === 'view') {
            this.stop = document.getElementById('stop');
            this.stop.pause();
        }else if (this.viewData === 'play') {
            this.stop1 = document.getElementById('stop1');
            this.stop1.pause();
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
}


