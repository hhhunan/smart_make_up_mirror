import {Component, OnInit, ElementRef} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';
import {Makeupstyle} from '../models/make_up_style';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    templateUrl: 'makeupmethods.component.html',
    providers: [RestDataService],
    animations: [
        trigger('degriss', [
            state('deg0', style({
                display: 'none',
                opacity: 0
            })),
            state('deg180', style({
                opacity: 1
            })),
            transition('deg0 => deg180', animate(1000, keyframes([
                style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
                style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
                style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
                style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
                style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
            ]))),
            transition('deg180 => deg0', animate(1000, keyframes([
                style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
                style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
                style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
                style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
                style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
            ])))
        ]),
        trigger('tabsRtigger', [
            transition('tabs1 => tabs2', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ]))),
            transition('tabs2 => tabs1', animate('500ms', keyframes([
                style({transform: 'rotateX(0)'}),
                style({transform: 'rotateX(45deg)'}),
                style({transform: 'rotateX(90deg)'}),
                style({transform: 'rotateX(135deg)'}),
                style({transform: 'rotateX(180deg)'}),
                style({transform: 'rotateX(225deg)'}),
                style({transform: 'rotateX(270deg)'}),
                style({transform: 'rotateX(315deg)'}),
                style({transform: 'rotateX(360deg)'}),
            ])))
        ])
    ]
})
export class MakeupMethodsComponent implements OnInit {
    private make_up_style: Makeupstyle[];
    private lang: string;
    private language_: any;
    private disablestatus: number;
    private loader: string;
    private messageError: string;
    private message: string;
    private editStyle: any;
    private messageError_modal: string;
    private name: object;
    private fileSize: boolean;
    private fileEditImage: boolean;
    private CreateEditObj: CreateEditObj;
    private imageBase: any;
    private no_result: boolean;
    private no_result_mejjige: string;
    private status: number;
    private user_type: string;
    private degriss: string = 'deg0';
    private tabs: any = 'tabs1';
    private img_path: any = '';
    public img_size: number
    constructor(private dataService: RestDataService,
                private elem: ElementRef) {
    }

    ngOnInit() {
        this.img_size = this.dataService.img_size_in_byte;
        this.fileEditImage = true;
        this.fileSize = true;
        this.no_result = false;
        this.lang = localStorage.getItem('activeLanguage');
        this.language_ = JSON.parse(localStorage.getItem('languages'));
        this.user_type = localStorage.getItem('user_type');
        this.dataService.getData('make_up_by_methods', this.lang).subscribe((items) => {
            this.make_up_style = items['data'];
            if (items['data'] == []) {
                this.make_up_style = [];
                this.no_result = true;
                this.loader = 'true';
                this.no_result_mejjige = 'No items';
            }
            this.tooltipImage();
        }, (err) => {
            this.make_up_style = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        })
    }

    // tooltype for images

    tooltipImage() {
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip({
                animated: 'fade',
                placement: 'bottom',
                html: true
            });
        });
    }

    timOut() {
        setTimeout(function () {
            this.degris();
            this.messageError = ''
        }.bind(this), 4000);
    }

    // Change language

    change_language(lang) {
        if (lang !== localStorage.getItem('activeLanguage')) {
            this.loader = '';
            this.lang = lang;
            localStorage.removeItem('activeLanguage');
            localStorage.setItem('activeLanguage', lang);
            this.dataService.getData('make_up_by_methods', lang).subscribe(
                (items) => {
                    this.make_up_style = items['data'];
                    this.loader = 'true';
                    this.tooltipImage();
                }, (err) => {
                    this.make_up_style = [];
                    this.no_result = true;
                    this.loader = 'true';
                    this.no_result_mejjige = this.dataService.error;
                });
        }
    }

    openEditModal(id) {
        this.img_path = '';
        this.fileEditImage = true;
        this.fileSize = true;
        this.messageError = '';
        this.messageError_modal = '';
        this.editStyle = null;
        this.dataService.getData('make_up_by_methods/' + id, '').subscribe((items) => {
            this.editStyle = items;
        }, (err) => {
            this.loader = 'true';
        });
    }

    // Update status from table
    changeStatus(status, id) {
        if (status === true) {
            this.CreateEditObj = {
                'status': 1,
            }
        } else {
            this.CreateEditObj = {
                'status': 0,
            }
        }
        this.disablestatus = id;
        this.dataService.editData('make_up_by_methods', this.CreateEditObj, id).subscribe(res => {
            this.message = 'Updated';
            this.degris()
            this.disablestatus = -1;
            this.timOut();
        }, error => {
            this.messageError = this.dataService.error;
            this.loader = 'true';
        });
    }

    onSumbit(form, id) {
        this.loader = '';
        this.name = {};
        for (let i = 0; i < this.language_.length; i++) {
            const name_lang = form['name_' + this.language_[i].code];
            this.name[this.language_[i].code] = name_lang;
        }
        if (form.editstatus == 1) {
            this.status = 1;
        } else {
            this.status = 0;
        }
        let files = this.elem.nativeElement.querySelector('#selectFile').files;
        let file = files[0];
        if (file !== undefined) {
            this.CreateEditObj = {
                'image': this.imageBase,
                'name': this.name,
                'status': this.status
            }
            this.send(id);
        } else {
            this.CreateEditObj = {
                'name': this.name,
                'status': this.status
            }
            this.send(id)
        }
    }

    send(id) {
        this.dataService.editData('make_up_by_methods', this.CreateEditObj, id).subscribe((items) => {
            this.dataService.getData('make_up_by_methods', this.lang).subscribe((item) => {
                this.make_up_style = item['data'];
            })
            this.loader = 'true';
            $('#myModal .close').click();
            this.message = 'Updated';
            this.degris();
            this.timOut();
        }, (error) => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        })
    }
    // validation image ang converting
    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                } else {
                    this.fileSize = true;
                    let _this = this;
                    let input = event.target;
                    _this.readUrl(event);
                    let reader = new FileReader();
                    reader.onload = function () {
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path = '';
        }
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
    tabs_() {
        this.tabs = (this.tabs === 'tabs1' ? 'tabs2' : 'tabs1');
    }
    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }
}

interface CreateEditObj {
    'name'?: any,
    'status'?: any,
    'image'?: any
}