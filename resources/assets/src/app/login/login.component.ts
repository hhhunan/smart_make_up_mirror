import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {AuthService} from '../shared/service/auth.service';
import {User} from '../models/user';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
    private disable: boolean = true;
    user: User = new User();
    error = '';

    constructor(private router: Router, private auth: AuthService) {
    }

    onLogin(): void {
        if (this.disable) {
            this.error = '';
            this.disable = false;
            this.auth.login(this.user)
                .then((user) => {
                    localStorage.setItem('token', user.json().access_token);
                    localStorage.setItem('refresh_token', user.json().refresh_token);
                    localStorage.setItem('user_type', user.json().user_type);
                    localStorage.setItem('role_name', user.json().role_label);
                    localStorage.setItem('image', user.json().user_image);
                    localStorage.setItem('name', user.json().user_name);
                    localStorage.setItem('user_id', user.json().user_id);
                    this.disable = true;
                    this.router.navigate(['/dashboard']);
                })
                .catch((err) => {
                    this.error = 'Incorrect username and/or password';
                    this.disable = true;
                });
        }
    }

}

