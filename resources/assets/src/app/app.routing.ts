import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HttpModule} from '@angular/http';


// Layouts
import {FullLayoutComponent} from './layouts/full-layout.component';
import {EnsureAuthenticated} from './shared/service/ensure-authenticated.service';
import {LoginRedirect} from './shared/service/login-redirect.service';
import {AuthService} from './shared/service/auth.service';


export const routes: Routes = [

    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
        canActivate: [EnsureAuthenticated]
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Home'
        },

        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule',
                canActivate: [EnsureAuthenticated]
            },
            {
                path: 'components',
                loadChildren: './components/components.module#ComponentsModule',
                canActivate: [EnsureAuthenticated]
            },
            // {
            //   path: 'widgets',
            //   loadChildren: './widgets/widgets.module#WidgetsModule',
            //   canActivate: [EnsureAuthenticated],
            // },
            {
                path: 'signup',
                loadChildren: './signup/signup.module#SignupModule',
                canActivate: [EnsureAuthenticated]
            },
        ]
    },
    {path: 'login', loadChildren: './login/login.module#LoginModule'},
    {path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule'},
    {path: '**', redirectTo: 'not-found'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes),
        HttpModule
    ],
    exports: [RouterModule],
    providers: [
        AuthService,
        EnsureAuthenticated,
        LoginRedirect
    ],
})
export class AppRoutingModule {
}
