import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {NAV_DROPDOWN_DIRECTIVES} from './shared/directive/nav-dropdown.directive';
import {SIDEBAR_TOGGLE_DIRECTIVES} from './shared/directive/sidebar.directive';
import {AsideToggleDirective} from './shared/directive/aside.directive';
import {BreadcrumbsComponent} from './shared/component/breadcrumb.component';
import {FullLayoutComponent} from './layouts/full-layout.component';


@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        AppRoutingModule,
        BrowserAnimationsModule
    ],
    declarations: [
        AppComponent,
        FullLayoutComponent,
        NAV_DROPDOWN_DIRECTIVES,
        BreadcrumbsComponent,
        SIDEBAR_TOGGLE_DIRECTIVES,
        AsideToggleDirective
    ],
    providers: [{
        provide: LocationStrategy,
        useClass: PathLocationStrategy
    }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
