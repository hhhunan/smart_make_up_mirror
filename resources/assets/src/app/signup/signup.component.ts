import {Component, OnInit, ElementRef} from '@angular/core';
import { Router } from '@angular/router';
import { RestDataService} from '../shared/service/rest-data.service';
import { AuthService } from '../shared/service/auth.service';
import {animate, keyframes, state, style, transition, trigger} from '@angular/animations';

declare var jquery: any;
declare var $: any;
@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    providers: [RestDataService],
    animations: [trigger('degriss', [
        state('deg0', style({
            display: 'none',
            opacity: 0
        })),
        state('deg180', style({
            opacity: 1
        })),
        transition('deg0 => deg180', animate(1000, keyframes([
            style({transform: 'rotateX(0)', display: 'block', opacity: 0.2, offset: 0}),
            style({transform: 'rotateX(45deg)', display: 'block', opacity: 0.4, offset: 0.4}),
            style({transform: 'rotateX(90deg)', display: 'block', opacity: 0.6, offset: 0.6}),
            style({transform: 'rotateX(135deg)', display: 'block', opacity: 0.8, offset: 0.8}),
            style({transform: 'rotateX(180deg)', display: 'block', opacity: 1, offset: 1}),
        ]))),
        transition('deg180 => deg0', animate(1000, keyframes([
            style({transform: 'rotateX(0)', opacity: 1, offset: 0}),
            style({transform: 'rotateX(45deg)', opacity: 0.8, offset: 0.2}),
            style({transform: 'rotateX(90deg)', opacity: 0.6, offset: 0.5}),
            style({transform: 'rotateX(135deg)', opacity: 0.4, offset: 0.8}),
            style({transform: 'rotateX(180deg)', opacity: 0.2, offset: 1}),
        ])))
    ])]
})
export class SignupComponent implements OnInit {
    private user: any[];
    private img_path: any = '';
    private img_size: number;
    private fileSize: boolean;
    private recuared_image: number;
    private imageBase: any;
    private fileEditImage: boolean;
    private messageError_modal: string;
    private CreateEditObj: CreateEditObj;
    private message: string;
    private loader: string;
    private degriss: string = 'deg0';

  constructor(private router: Router,
              private auth: AuthService,
              private dataService: RestDataService,
              private elem: ElementRef) {}

    ngOnInit() {
        this.img_size = this.dataService.img_size_in_byte;
        this.dataService.getData('myProfile', '').subscribe((items) => {
            this.user = items;
        })
    }
    openModal() {
        this.messageError_modal = '';
        this.img_path = '';
    }
    timOut() {
        setTimeout(function () {
            this.degris();
        }.bind(this), 4000);
    }
    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();

            reader.onload = (event: any) => {
                this.img_path = event.target.result;
            }

            reader.readAsDataURL(event.target.files[0]);
        }
    }
    onImageChangeFromFile(event) {
        if (event.srcElement.files[0] !== undefined) {
            let size = event.srcElement.files[0].size;
            let file = event.srcElement.files[0].type;
            if ((file == 'image/jpeg' || file == 'image/png')) {
                if (this.img_size <= size) {
                    this.fileSize = false;
                    this.img_path = '';
                }else {
                    this.recuared_image = 1;
                    this.fileSize = true;
                    let _this = this;
                    _this.readUrl(event);
                    let input = event.target;
                    let reader = new FileReader();
                    reader.onload = function(){
                        _this.imageBase = reader.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                this.fileEditImage = true;
            } else {
                this.recuared_image = -1;
                this.fileSize = true;
                this.fileEditImage = false;
                this.img_path = '';
            }
        } else {
            this.recuared_image = 1;
            this.fileSize = true;
            this.fileEditImage = true;
            this.img_path = '';
        }
    }
    onSumbit(form) {
      this.messageError_modal = '';
        this.loader = '';
        const files = this.elem.nativeElement.querySelector('#selectFile').files;
        const file = files[0];
        console.log(form.password);
        if (file !== undefined) {
            if (form.password === null || form.password == '') {
                this.CreateEditObj = {
                    'name': form.name,
                    'image': this.imageBase,
                };
            }else {
                this.CreateEditObj = {
                    'name': form.name,
                    'image': this.imageBase,
                    'password': form.password,
                    'confirm_password': form.confirm_password
                };
            }
        }else {
            if (form.password === null || form.password == '') {
                this.CreateEditObj = {
                    'name': form.name,
                };
            }else {
                this.CreateEditObj = {
                    'name': form.name,
                    'password': form.password,
                    'confirm_password': form.confirm_password
                };
            }

        }

        this.dataService.createData('editMyProfile', this.CreateEditObj).subscribe((res) => {
            this.dataService.getData('myProfile', '').subscribe((items) => {
                this.user = items;
                this.message = 'Updated';
                this.loader = 'true';
                this.timOut();
                this.degris();
                $('#myModal .close').click();
                localStorage.removeItem('image');
                localStorage.removeItem('name');
                localStorage.setItem('image', items.image);
                localStorage.setItem('name', items.name);
            }, (err) => {
                this.loader = 'true';
            });
        }, error => {
            this.messageError_modal = this.dataService.error;
            this.loader = 'true';
        });
    }
    degris() {
        this.degriss = (this.degriss === 'deg0' ? 'deg180' : 'deg0');
    }
}
interface CreateEditObj {
    'name'?: string,
    'image'?: any,
    'password'?: any;
    'confirm_password'?: any;
}