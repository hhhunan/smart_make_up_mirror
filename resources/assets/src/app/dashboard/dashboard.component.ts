import {Component, OnInit} from '@angular/core';
import {RestDataService} from '../shared/service/rest-data.service';

@Component({
    templateUrl: 'dashboard.component.html',
    providers: [RestDataService]
})
export class DashboardComponent implements OnInit {
    public sliders: Array<any> = [];
    private product_count: any;
    private style_count: any;
    private device_count: any;
    private device_update: any;
    private chartData: any;
    private chartData2: any;
    private chartData3: any;
    private product_is_ok: number;
    private style_is_ok: number;
    private device_is_ok: number;
    private chartData_bool: any = false;
    private count = 0;
    private four_ = 4;

    constructor(private dataService: RestDataService) {
    }

    chartOptions = {
        responsive: true,
        animation: {
            easing: 'easeInOutBack'
        }
    };

    chartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Septrember', 'October', 'November', 'December'];
    chartOptions2 = {
        responsive: true,
        animation: {
            easing: 'easeInOutBack'
        }
    };

    chartLabels2 = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Septrember', 'October', 'November', 'December'];
    chartOptions3 = {
        responsive: true,
        animation: {
            easing: 'easeInOutBack'
        }
    };


    chartLabels3 = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Septrember', 'October', 'November', 'December'];
    myColors = [
        {
            backgroundColor: 'rgba(8, 24, 54, 0.8)',
            borderColor: '',
            pointBackgroundColor: '#999',
            pointBorderColor: 'rgba(1, 58, 1, 100)',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(211, 58, 1, 1)'
        },
        // ...colors for additional data sets
    ];
    myColors2 = [
        {
            backgroundColor: 'rgba(6, 34, 77, 0.6)',
            borderColor: '',
            pointBackgroundColor: 'rgba(211, 58, 1, 1)',
            pointBorderColor: 'rgba(1, 58, 1, 100)',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(211, 58, 1, 1)'
        },
        // ...colors for additional data sets
    ];
    myColors4 = [
        {
            backgroundColor: 'rgba(6, 34, 77, 0.6)',
            borderColor: '',
            pointBackgroundColor: 'rgba(20, 58, 1, 1)',
            pointBorderColor: 'rgba(1, 58, 1, 100)',
            pointHoverBackgroundColor: '#ffcf44',
            pointHoverBorderColor: 'rgba(120, 58, 1, 1)'
        },
        // ...colors for additional data sets
    ];
    myColors3 = [
        {
            backgroundColor: [
                '#ff0000',
                '#e01e84',
                '#c758d0',
                '#9c46d0',
                '#8399eb',
                '#007ed6',
                '#5fb7d4',
                '#7cdddd',
                '#26d7ae',
                '#1baa2f',
                '#d5f30b',
                '#ffaf00',
                '#ff7300'
            ],
            borderColor: '',
            borderWidth: 1,
            pointBackgroundColor: 'rgba(211, 58, 1, 1)',
            pointBorderColor: 'rgba(1, 58, 1, 100)',
            pointHoverBackgroundColor: '',
            pointHoverBorderColor: 'rgba(211, 58, 1, 1)'
        },
        // ...colors for additional data sets
    ];

    ngOnInit(): void {
        this.chartData_bool = false;
        this.count = 0;
        this.product_count = [];
        this.style_count = [];
        this.device_count = [];
        this.device_update = [];
        this.chartData3 = [];
        this.chartData = [];
        this.chartData2 = [];
        this.dataService.getData('product/analytics', '').subscribe((items) => {
            for (let i = 0; i < items.created.length; i++) {
                this.product_count[items.created[i].month - 1] = items.created[i].created_count;
            };
            this.count_array_product(this.product_count);
        })
        this.dataService.getData('styles/analytics', '').subscribe((items) => {
            for (let i = 0; i < items.created.length; i++) {
                this.style_count[items.created[i].month - 1] = items.created[i].created_count;
            }
            this.count_array_style(this.style_count);
        });
        this.dataService.getData('device/analytics', '').subscribe((items) => {
            for (let i = 0; i < items.created.length; i++) {
                this.device_count[items.created[i].month - 1] = items.created[i].created_count;
            }
            this.count_array_device(this.device_count);
            if (items.device_updated.length !== 0) {
                for (let i = 0; i < items.device_updated.length; i++) {
                    for (let j = 0; j < items.device_updated[i].length; j++) {
                        this.device_update[items.device_updated[i][j].month - 1] = items.device_updated[i][j].updated_count;
                    }
                    this.count_array_device_update(this.device_update, items.device_updated[i][0].device_id, i, items.device_updated.length);
                }
            } else {
                this.count_array_device_update(this.device_update, '', 0, 0);
            }
        });
    }

    // chartData3 = [{data: [0, 0, 0, 4, 5, 4, 3, 0, 0, 0, 0, 1], label: 'Product'}, {data: [0, 0, 1, 6, 2, 4, 8, 0, 0, 3, 0, 1], label: 'Style'}];
    // chartData2 = [{data: [0, 1, 0, 4, 5, 4, 3, 0, 5, 0, 0, 1], label: 'device 1'}];
    // chartData = [{data: [0, 3, 0, 0, 5, 0, 3, 0, 2, 1, 0, 1], label: 'Device'}];
    count_array_style(style) {
        for (let i = 0; i <= 11; i++) {
            if (style[i] === undefined) {
                style[i] = 0;
            }
            this.style_is_ok = i;
        }
        this.chartData3.push({'data': this.style_count, label: 'Style'});
        if (this.style_is_ok === 11) {
            this.is_ok();
        }
    }

    count_array_product(product) {
        for (let i = 0; i <= 11; i++) {
            if (product[i] === undefined) {
                product[i] = 0;
            }
            this.product_is_ok = i;
        }
        this.chartData3.push({'data': this.product_count, label: 'Product'});
        if (this.product_is_ok === 11) {
            this.is_ok();
        }
    }

    count_array_device(device) {
        for (let i = 0; i <= 11; i++) {
            if (device[i] === undefined) {
                device[i] = 0;
            }
            this.device_is_ok = i;
        }
        this.chartData.push({'data': this.device_count, label: 'Device'});
        if (this.device_is_ok === 11) {
            this.is_ok();
        }
    }

    count_array_device_update(device, id, j, device_length) {
        for (let i = 0; i <= 11; i++) {
            if (device[i] === undefined) {
                device[i] = 0;
            }
        }
        this.chartData2.push({'data': this.device_update, label: 'Device ' + id});
        if (j + 1 === device_length) {
            this.is_ok();
        }else if (j === 0 && device_length === 0) {
            this.is_ok();
        }

    }

    onChartClick2() {
    }

    onChartClick() {
    }

    onChartClick3() {
    }
    is_ok () {
        this.count += 1;
        if (this.count === 4) {
            this.chartData_bool = true;
        }
    }
}
