import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RestDataService} from '../shared/service/rest-data.service';
import {TweenMax, Power2, TimelineLite} from 'gsap';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes, group
} from '@angular/animations';
declare var jquery: any;
declare var $: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: './full-layout.component.html',
    providers: [RestDataService],
    animations: [
        trigger('line', [
            transition('lineShow => lineHide', animate('1000ms ease', keyframes([
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(0%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(10%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(20%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(30%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(40%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(50%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(60%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(70%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(80%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(90%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(100%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(0%)'})
            ]))),
            transition('lineHide => lineShow', animate('1000ms ease', keyframes([
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(0%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(10%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(20%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(30%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(40%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(50%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(60%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(70%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(80%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(90%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(100%)'}),
                style({height: '3px', 'background-color': '#44BBFF', transform: 'translateX(0%)'})
            ]))),
        ]),
    ]
})
export class FullLayoutComponent implements OnInit {

    public disabled = false;
    private user_type: string;
    private name: string;
    private image: string;
    private role_name: string;
    public status: { isopen: boolean } = {isopen: false};
    private language_: any;
    private no_result: boolean;
    private no_result_mejjige: string;
    private loader: string;
    private line: string = 'lineShow';
    private menu_name: string = '';

    public toggled(open: boolean): void {
    }

    public toggleDropdown($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }

    constructor(private router: Router,
                private dataService: RestDataService) {
    }

    ngOnInit(): void {
        this.user_type = localStorage.getItem('user_type');
        this.image = localStorage.getItem('image');
        this.name = localStorage.getItem('name');
        this.role_name = localStorage.getItem('role_name');
        this.dataService.getData('language', '').subscribe((items) => {
            this.language_ = items;
            for (let i = 0; i < items.length; i++) {
                if (items[i].default === 1) {
                    localStorage.setItem('activeLanguage', items[i].code);
                }
            }
            localStorage.setItem('languages', JSON.stringify(this.language_));
        }, (err) => {
            this.language_ = [];
            this.no_result = true;
            this.loader = 'true';
            this.no_result_mejjige = this.dataService.error;
        });
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('body').toggleClass('sidebar-collapse');
            });
            $('#makup').on('show.bs.collapse', function () {
                $('#homeSubmenu2').collapse('hide');
                $('#homeSubmenu3').collapse('hide');
            });

            $('#homeSubmenu2').on('show.bs.collapse', function () {
                $('#homeSubmenu3').collapse('hide');
                $('#makup').collapse('hide');
            });

            $('#homeSubmenu3').on('show.bs.collapse', function () {
                $('#homeSubmenu2').collapse('hide');
                $('#homeSubmenu').collapse('hide');
            });

            const tl = new TimelineLite()
                , inter = 30
                , speed = 1
                , $text = $('.text');

            function animInfinite() {
                $('.text').each(function (index, val) {
                    index = index + 1;
                    TweenMax.fromTo(
                        $(this), speed, {autoAlpha: 0}, {autoAlpha: 0 + (0.01 * index), delay: 0.0333 * index});
                });
                TweenMax.to(
                    $('.text:nth-child(30)'), speed, {autoAlpha: 1.5, delay: 1}
                );
            }

            $('.typer input').keyup(function () {
                TweenMax.killAll(false, true, false);
                TweenMax.set($text, {autoAlpha: 0});
                $text.text(this.value);
                animInfinite();
            });
            animInfinite();
            if ($(window).width() <= 768) {
                $( 'body').addClass('sidebar-collapse');
            } else {
                $( 'body').removeClass('sidebar-collapse');
            }
        });
    }

    onLogout() {
        this.dataService.createData('logout', '').subscribe();
        window.localStorage.clear();
        this.router.navigate(['/login']);
    }

    user_info() {
        if (this.name !== localStorage.getItem('name')) {
            this.name = localStorage.getItem('name');
        } else if (this.image !== localStorage.getItem('image')) {
            this.image = localStorage.getItem('image');
        }
    }
    line_show(param) {
        if (this.menu_name === '' || this.menu_name !== param) {
            this.line = (this.line === 'lineShow' ? 'lineHide' : 'lineShow');
            this.menu_name = param;
        }
    }
}
