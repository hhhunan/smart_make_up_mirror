import { Video } from './video';
import { Session } from './session';

export class PlayList {
	id: number;
	movie_id: number;
	movie: Video[];
	session: Session[];
}