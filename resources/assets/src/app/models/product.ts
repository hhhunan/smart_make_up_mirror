import {Device} from './device';
import {Brand} from './brand';
import {Image} from './image';
import {Category} from './category';
import {Session} from './session';


export class Product {

    id: number;
    name: string;
    description: string;
    status: number;
    created_at: Date;
    updated_at: Date;
    devices: Device[];
    brands: Brand[];
    category: Category[];
    sessions: Session[];
    images: Image[];

    constructor() {
    }
}
