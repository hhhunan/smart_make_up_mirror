export class Session {
	id: number;
	name: string;
	status: number;
	created_at: Date;
	updated_at: Date;
}
