export class Brand  {
	id: number;
    name: string;
    status: number;
    created_at:Date;
    updated_at: Date;
    // pivot: {}
}