export class Users {
    id: number;
    image: string;
    name: string;
    role: {
        name: string;
        label: string;
    };
    email: number;
    created_at: Date;
    updated_at: Date;
}