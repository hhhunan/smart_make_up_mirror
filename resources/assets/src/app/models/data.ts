import { Device } from './device';
import { Brand } from './brand';
import { Image } from './image';

export class Data  {
    id:  number;
    name: string;
    description:string;
    status: number;
    created_at: Date;
    updated_at: Date;
    devices: Device[];
    brands: Brand[];
    images: Image[];
    product_category: {}
}