import {Product} from './product';

export class Device {
    id: number;
    name: string;
    client_id: number;
    secret: string;
    imei_number: string;
    type: number;
    status: number;
    created_at: Date;
    updated_at: Date;
    products: Product[];

    constructor() {
    }
}