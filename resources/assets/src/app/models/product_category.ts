export class ProductCategory{
	product_id: number;
    category_id: number;
    category: {
        id: number;
        name: string;
        status: number,
        created_at: Date;
        updated_at: Date;
    }
}