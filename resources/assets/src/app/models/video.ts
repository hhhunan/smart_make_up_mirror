export class Video {
	id: number;
	name: string;
	uri: string;
	format: string;
	duration: string;
	type:number;
	status: number;
	created_at: Date;
	updated_at: Date;
	// pivot: {};
}