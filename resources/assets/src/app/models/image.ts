export class Image {
	id: number;
	name: string;
	uri: string;
	type:number;
	status: number;
	created_at: Date;
	updated_at: Date;
	// pivot: {};
}