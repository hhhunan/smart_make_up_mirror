import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';

import {environment} from '../../../environments/environment.prod'
import {Router} from '@angular/router';

@Injectable()
export class RestDataService {

    private token: string;
    private Refresh_token;
    public selectedLanguage: number;
    public error: any;
    public img_size_in_byte: number;

    constructor(private http: HttpClient, private router: Router) {
        this.token = localStorage.getItem('token');
        this.Refresh_token = localStorage.getItem('refresh_token');
        this.selectedLanguage = 0;  // selected language for select 2
        this.img_size_in_byte = 1000000;
    }


    getData(dataUrl: string, lang: string) {
        let url = `${environment.endpoint}/${dataUrl}`;
        let header = new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.token)
            .set('Accept', 'application/json')
            .set('language', lang);
        return this.http.get(url, {
            headers: header
        }).catch((error: any) => {
            if (error.status === 400) {
                this.error = 'Your request is invalid…';
                return Observable.throw(new Error(error.status));
            }
            if (error.status === 401 && this.Refresh_token) {
                const url = `${environment.endpoint}/refresh`;
                const header = new HttpHeaders()
                    .set('Authorization', 'Bearer')
                    .set('Accept', 'application/json');
                return this.http.post(url, {
                    'refresh_token': this.Refresh_token
                }, {headers: header}).toPromise().then((it) => {
                    this.token = it['access_token'];
                    this.Refresh_token = it['refresh_token'];
                    localStorage.setItem('token', it['access_token']);
                    localStorage.setItem('refresh_token', it['refresh_token']);
                    let url = `${environment.endpoint}/${dataUrl}`;
                    let header = new HttpHeaders()
                        .set('Authorization', 'Bearer ' + this.token)
                        .set('Accept', 'application/json')
                        .set('language', lang);
                    return this.http.get(url, {
                        headers: header
                    }).toPromise().then();
                }).catch((err) => {
                    if (err.status == 401) {
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                    }
                });
            }
            if (error.status === 500) {
                this.error = 'Server error… Please contact with your website administrator.';
                return Observable.throw(new Error(error.status));
            }
        });
    }

    register(registration) {
        const url = `${environment.endpoint}/user`;
        const header = new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.token)
            .set('Accept', 'application/json');
        return this.http.post(url,
            registration, {
                headers: header
            }).catch((error: any) => {
            if (error.status === 422) {
                this.error = 'Your request is invalid…';
                return this.error, Observable.throw(new Error(error.status));
            } else if (error.status === 400) {
                this.error_validations(error.error.message);
                return this.error, Observable.throw(new Error(error.status));
            } else if (error.status === 401 && this.Refresh_token) {
                const url = `${environment.endpoint}/refresh`;
                const header = new HttpHeaders()
                    .set('Authorization', 'Bearer')
                    .set('Accept', 'application/json')
                return this.http.post(url, {
                    'refresh_token': this.Refresh_token
                }, {headers: header}).toPromise().then((it) => {
                    this.token = it['access_token'];
                    this.Refresh_token = it['refresh_token'];
                    localStorage.setItem('token', it['access_token']);
                    localStorage.setItem('refresh_token', it['refresh_token']);
                    let url = `${environment.endpoint}/user`;
                    let header = new HttpHeaders()
                        .set('Authorization', 'Bearer ' + this.token)
                        .set('Accept', 'application/json');
                    return this.http.post(url,
                        registration, {
                        headers: header
                    }).toPromise().then();
                }).catch((err) => {
                    if (err.status == 401) {
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                    }
                });
            }else if (error.status === 500) {
                this.error = 'Server error… Please contact with your website administrator.';
                return this.error, Observable.throw(new Error(error.status));
            }
        });
    }


    getDeleteId(id, dataUrl) {
        let url = `${environment.endpoint}/${dataUrl}/${id}`;
        let header = new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.token)
            .set('Accept', 'application/json');
        return this.http.delete(url, {
            headers: header
        }).catch((error: any) => {
            if (error.status === 400) {
                this.error_validations(error.error.message);
                return this.error, Observable.throw(new Error(error.status));
            }
            if (error.status === 401 && this.Refresh_token) {
                const url = `${environment.endpoint}/refresh`;
                const header = new HttpHeaders()
                    .set('Authorization', 'Bearer')
                    .set('Accept', 'application/json')
                return this.http.post(url, {
                    'refresh_token': this.Refresh_token
                }, {headers: header}).toPromise().then((it) => {
                    this.token = it['access_token'];
                    this.Refresh_token = it['refresh_token'];
                    localStorage.setItem('token', it['access_token']);
                    localStorage.setItem('refresh_token', it['refresh_token']);
                    let url = `${environment.endpoint}/${dataUrl}/${id}`;
                    let header = new HttpHeaders()
                        .set('Authorization', 'Bearer ' + this.token)
                        .set('Accept', 'application/json');
                    return this.http.delete(url,
                         {
                            headers: header
                        }).toPromise().then();
                }).catch((err) => {
                    if (err.status == 401) {
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                    }
                });
            }else if (error.status === 500) {
                this.error = 'Server error… Please contact with your website administrator.';
                return this.error, Observable.throw(new Error(error.status));
            }
        });
    }

    editData(dataUrl, edit, id) {
        let url = `${environment.endpoint}/${dataUrl}/${id}`;
        let header = new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.token)
            .set('content-type', 'application/json');
        return this.http.put(url,
            edit, {
                headers: header
            }).catch((error: any) => {
            if (error.status === 500) {
                this.error = 'Server error… Please contact with your website administrator.';
                return this.error, Observable.throw(new Error(error.status));
            } else if (error.status === 400) {
                this.error_validations(error.error.message);
                return this.error, Observable.throw(new Error(error.status));
            } else if (error.status === 401 && this.Refresh_token) {
                const url = `${environment.endpoint}/refresh`;
                const header = new HttpHeaders()
                    .set('Authorization', 'Bearer')
                    .set('Accept', 'application/json')
                return this.http.post(url, {
                    'refresh_token': this.Refresh_token
                }, {headers: header}).toPromise().then((it) => {
                    this.token = it['access_token'];
                    this.Refresh_token = it['refresh_token'];
                    localStorage.setItem('token', it['access_token']);
                    localStorage.setItem('refresh_token', it['refresh_token']);
                    let url = `${environment.endpoint}/${dataUrl}/${id}`;
                    let header = new HttpHeaders()
                        .set('Authorization', 'Bearer ' + this.token)
                        .set('content-type', 'application/json');
                    return this.http.put(url,
                        edit,
                        {
                            headers: header
                        }).toPromise().then();
                }).catch((err) => {
                    if (err.status == 401) {
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                    }
                });
            }
        });
    }

    createData(dataUrl, create) {
        let url = `${environment.endpoint}/${dataUrl}`;
        let header = new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.token)
            .set('Accept', 'application/json');
        return this.http.post(url,
            create, {
                headers: header
            }).catch((error: any) => {
            if (error.status === 500) {
                this.error = 'Server error… Please contact with your website administrator.';
                return this.error, Observable.throw(new Error(error.status));
            } else if (error.status === 400) {
                this.error_validations(error.error.message);
                return Observable.throw(new Error(error.status));
            } else if (error.status === 401 && this.Refresh_token) {
                const url = `${environment.endpoint}/refresh`;
                const header = new HttpHeaders()
                    .set('Authorization', 'Bearer')
                    .set('Accept', 'application/json')
                return this.http.post(url, {
                    'refresh_token': this.Refresh_token
                }, {headers: header}).toPromise().then((it) => {
                    this.token = it['access_token'];
                    this.Refresh_token = it['refresh_token'];
                    localStorage.setItem('token', it['access_token']);
                    localStorage.setItem('refresh_token', it['refresh_token']);
                    let url = `${environment.endpoint}/${dataUrl}`;
                    let header = new HttpHeaders()
                        .set('Authorization', 'Bearer ' + this.token)
                        .set('Accept', 'application/json');
                    return this.http.post(url,
                        create,
                        {
                            headers: header
                        }).toPromise().then();
                }).catch((err) => {
                    if (err.status == 401) {
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                    }
                });
            }
        });
    }
    restAPI(url: string, body: {}, lang: string) {
        const header = new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.token)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .set('language', lang);
        return this.http.put(url, body, {headers: header});
    }
    error_validations (error) {
        if ((typeof error) == 'object') {
            this.error = error[Object.keys(error)[0]][0];
        }else {
            this.error = error;
        }
    }
}