import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { User } from '../../models/user';
import {environment} from '../../../environments/environment.prod';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {
  private headers: Headers = new Headers({'Content-Type': 'application/json'});
  
  constructor(private http: Http) {}
  
  login(user: User): Promise<any> {
    localStorage.removeItem('token');
    let url: string = `${environment.endpoint}/login`;
        return this.http.post(url, 
              user, 
              {headers: this.headers})
           .toPromise();
  }

  register(user: User): Promise<any> {
    let url: string = `${environment.endpoint}/register`;
    return this.http.post(url, user, {headers: this.headers}).toPromise();
  }

  // ensureAuthenticated(token): Promise<any> {
  //   let url: string = `${this.BASE_URL}/status`;
  //   let headers: Headers = new Headers({
  //     'Content-Type': 'application/json',
  //     Authorization: `Bearer ${token}`
  //   });
  //   return this.http.get(url, {headers: headers}).toPromise();
  // }
}