(function ($) {
    var methods = {
        init: function (options) {
            var list = $(this),
                cells = [];
            var settings = $.extend({
                selector: 'li',
                dragClass: '_isDragged',
                dragBox: 'video_size',
                // placeholder: '<li class="col-md-custom-5" style="position: absolute">' +
                // 			 '<div style="height: 240px;"></div> </li>'
            }, options);
            function calculatePositions() {
                list.children(settings.selector).each(function (key, item) {
                    var offset = $(item).offset();
                    cells[key] = {
                        x1: offset.left,
                        y1: offset.top,
                        x2: offset.left + $(item).outerWidth(),
                        y2: offset.top + $(item).outerHeight(),
                    };
                    $(item).css({
                        left: cells[key].x1 - list.offset().left,
                        top: cells[key].y1 - list.offset().top
                    });
                });
            }
            function findPosition(x, y) {
                var newPositon = null;
                $.each(cells, function (key, item) {
                    if (x > item.x1 && x < item.x2 && y > item.y1 && y < item.y2) {
                        newPositon = key;
                        return;
                    }
                });
                return newPositon;
            }
            function insertItem(item, exclude, position) {
                if (position == 0) {
                    item.prependTo(list);
                } else {
                    item.insertAfter(list.children(settings.selector).not(item).not(exclude)[position - 1]);
                }
            }
            list.on('mousedown', settings.selector, function (e) {
                if(e.target.className !== settings.dragBox)
                    return true;

                calculatePositions();
                var draggedItem = $(this).addClass(settings.dragClass),
                    placeholder = $(settings.placeholder);
                var offset = {
                    top: e.pageY,
                    left: e.pageX
                };
                var prevPosition = position = draggedItem.index();
                placeholder.insertBefore(draggedItem);
                draggedItem.css({
                    top: e.pageY - offset.top,
                    left: e.pageX - offset.left
                });
                $(document)
                    .on('mousemove', function (e) {
                        draggedItem.css({
                            top: e.pageY - offset.top,
                            left: e.pageX - offset.left
                        });
                        var newPosition = findPosition(e.pageX, e.pageY);;
                        if (newPosition != position && newPosition != null) {
                            position = newPosition;
                            insertItem(placeholder, draggedItem, position);
                        }
                    })
                    .on('mouseup', function (e) {
                        insertItem(draggedItem, placeholder, position);
                        placeholder.remove();
                        draggedItem.removeClass(settings.dragClass);
                        $(this)
                            .off('mousemove')
                            .off('mouseup');
                        if (typeof (settings.onStop) == 'function')
                            settings.onStop(position + 1, prevPosition + 1, draggedItem);
                    });
            });
        }
    }
    $.fn.jesse = function (method) {
        return methods.init.apply(this, arguments);
    };
})(jQuery);
