<?php

use Illuminate\Database\Seeder;
Use Faker\Factory as Faker;

use App\Models\Device;
use App\Models\DeviceTranslation;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$fakerRU = Faker::create('ru_RU');
        $fakerEN = Faker::create();

        for($i = 1; $i < 2; $i++)
        {
            $device = new Device;
            $device->imei_number = $fakerEN->uuid;
            $device->status = 1;
            $device->type = 1;
            $device->secret_id = 1;
            $device->by_live_image_id =$i;
            $device->by_session_image_id =$i;
            $device->save();

            $trans = new DeviceTranslation;
            $trans->name = $fakerEN->name;
            $trans->device_id = $device->id;
            $trans->language_id = 1;
            $trans->save();

            $trans = new DeviceTranslation;
            $trans->name = $fakerRU->name;
            $trans->device_id = $device->id;
            $trans->language_id = 2;
            $trans->save();

        }
    }
}
