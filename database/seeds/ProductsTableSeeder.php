<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\ProductTranslation;
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fakerRU = Faker::create('ru_RU');
        $fakerEN = Faker::create();

        for($i = 9; $i < 11; $i++)
        {
            $product = new Product;
            $product->status = 1;
            $product->image_id = $i;
            $product->save();

            $trans = new ProductTranslation;
            $trans->name = $fakerEN->name;
            $trans->description = $fakerEN->text(150);
            $trans->product_id = $product->id;
            $trans->language_id = 1;
            $trans->save();

            $trans = new ProductTranslation;
            $trans->name = $fakerRU->name;
            $trans->product_id = $product->id;
            $trans->description = $fakerRU->text(150);
            $trans->language_id = 2;
            $trans->save();
        }
    }
}
