<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Models\Brand;
use App\Models\BrandTranslation;

class BrandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $fakerRU = Faker::create('ru_RU');
        $fakerEN = Faker::create();

        for($i = 6; $i < 9; $i++)
        {
            $brand = new Brand;
            $brand->status = 1;
            $brand->image_id =$i;
            $brand->save();

            $trans = new BrandTranslation;
            $trans->name = $fakerEN->name;
            $trans->brand_id = $brand->id;
            $trans->language_id = 1;
            $trans->save();

            $trans = new BrandTranslation;
            $trans->name = $fakerRU->name;
            $trans->brand_id = $brand->id;
            $trans->language_id = 2;
            $trans->save();

        }
    }
}