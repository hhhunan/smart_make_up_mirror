<?php

use App\Models\Image;
use Illuminate\Database\Seeder;
Use Faker\Factory as Faker;
use App\ImageTypes;

class ImagesTableSeeder extends Seeder
{
    use ImageTypes;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $filePath = public_path('/uploads/images');

        if (!File::exists($filePath)) {
            File::makeDirectory($filePath, $mode = 0777, true, true);
        }

//        $settingData = [
//            "country_flag" => [
//                "uri" => env('UPLOAD_IMAGE_PATH') . '/'."en.png",
//                "name" => "english_flag.png",
//                'type' => Image::$ImageTypeArray["flag"],
//                'file_size' => 1100,
//                'md5' => md5_file($filePath . '/'."en.png"),
//                'status' => 1,
//                'created_at' => $faker->dateTime('now'),
//                'updated_at' => $faker->dateTime('now'),
//            ],
//            "default_avatar" => [
//                "uri" =>  env('UPLOAD_IMAGE_PATH') . '/'. "avatar.jpg",
//                "id" => 2,
//                "name" => "profile",
//                'type' => Image::$ImageTypeArray["profile"],
//                'file_size' => 1100,
//                'status' => 1,
//                'created_at' => $faker->dateTime('now'),
//                'updated_at' => $faker->dateTime('now'),
//            ],
//        ];

//        foreach ($settingData as $key => $value)
//        {
//            DB::table('images')->insert($value);
//        }
        $imagesForGenerate = ['flag'=>2, 'profile'=>1, 'method'=>2, 'brand'=>3, 'product' =>2, 'style' => 2];
        foreach ($imagesForGenerate as $k =>$v) {
            for ($i = 1; $i <= $v; $i++) {
                $imageModel = new Image();
                if($k == 'flag'){
                    if($i == 1){
                        $image = 'en.png';
                    }else{
                        $image = 'ru.png';
                    }

                }elseif ($k == 'profile'){
                    $image = 'avatar.png';
                }
                else{
                    $image = $faker->image($filePath, 640, 480, 'fashion', false);
                }
                $imageModel->name = $image;
                $imageModel->md5 = md5_file($filePath . '/' . $image);
                $imageModel->uri = "/uploads/images" . '/' . $image;
                $imageModel->type = Image::$ImageTypeArray[$k];
                $imageModel->file_size = rand(999999,2000000);
                $imageModel->status = 1;
                $imageModel->created_at = $faker->dateTime('now');
                $imageModel->updated_at = $faker->dateTime('now');
                $imageModel->save();
            }
        }
    }
}
