<?php

use Illuminate\Database\Seeder;

class MakeUpMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
        public function run()
    {
        $Make_up_methods['en'] = ['Make up By Product', 'Make up By Style'];
        $Make_up_methods['ru'] = ['Макияж по продукту','Макияж по стилю'];
        $imageId = 4;
        $j = 0;
        foreach($Make_up_methods as $k =>$v)
        {
            $methods = new \App\Models\MakeUpMethod();
            $methods->status = 1;
            $methods->image_id = $imageId;
            $methods->save();
            $imageId += 1;

            $trans = new \App\Models\MakeUpMethodTranslation();
            $trans->name = $Make_up_methods['en'][$j];
            $trans->language_id = 1;
            $trans->make_up_method_id = $j+1;
            $trans->save();

            $trans = new \App\Models\MakeUpMethodTranslation();
            $trans->name = $Make_up_methods['ru'][$j];
            $trans->language_id = 2;
            $trans->make_up_method_id = $j+1;
            $trans->save();
            $j+=1;

        }
    }
}
