<?php

use App\Models\MakeUpStyleTranslation;
use Illuminate\Database\Seeder;
Use Faker\Factory as Faker;

class MakeUpStylesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fakerRU = Faker::create('ru_RU');
        $fakerEN = Faker::create();

        for($i = 11; $i < 13; $i++) {
            $style = new \App\Models\MakeUpStyle();
            $style->status = 1;
            $style->image_id = $i;
            $style->created_at = $fakerEN->dateTime('now');
            $style->updated_at = $fakerEN->dateTime('now');
            $style->save();

            $trans = new MakeUpStyleTranslation();
            $trans->name = $fakerEN->name;
            $trans->make_up_style_id = $i - 10;
            $trans->language_id = 1;
            $trans->save();

            $trans = new MakeUpStyleTranslation();
            $trans->name = $fakerRU->name;
            $trans->make_up_style_id = $i - 10;
            $trans->language_id = 2;
            $trans->save();
        }
    }
}
