<?php

use Illuminate\Database\Seeder;
use \App\Models\Role;

class RolesTableSeeder extends Seeder
{
    public $roles = [
        [ "name" => "administrator", "label" => "Administrator" ],
        [ "name" => "editor", "label" => "Moderator" ],
        [ "name" => "reviewer", "label" => "User" ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $key => $value)
        {
            $role = new Role;
            $role->id = $key + 1;
            $role->name = $value["name"];
            $role->label = $value["label"];
            $role->save();
        }
    }
}
