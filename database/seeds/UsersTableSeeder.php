<?php

use Illuminate\Database\Seeder;
use App\ImageTypes;

class UsersTableSeeder extends Seeder
{
    use ImageTypes;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'Imirror',
            'email'=> 'imirror_admin@imirror.com',
            'password'=>bcrypt('admin12345'),
            'role_id' => 1,
            'image_id' => 3,
        ]);
        \DB::table('users')->insert([
            'name' => 'Admin',
            'email'=> 'administrator@mail.com',
            'password'=>bcrypt('administrator'),
            'role_id' => 1,
            'image_id' => 3,
        ]);

    }
}
