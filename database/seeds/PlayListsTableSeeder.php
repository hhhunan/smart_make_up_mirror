<?php

use Illuminate\Database\Seeder;

class PlayListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ordering = 1;
        $id = 1;
        for ($i = 1; $i < 9; $i++ ){
            $pl = new \App\Models\PlayList();
            $pl -> id = $id;
            $pl -> movie_id = $i;
            $pl -> ordering = $ordering;
            $pl->save();
            if($i%2 == 0){
                $ordering =1;
                $id++;
            }else{
                $ordering++;
            }
        }



        //factory(App\Models\PlayList::class, 11)->create();
    }
}
