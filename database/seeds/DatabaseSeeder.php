<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ImagesTableSeeder::class);
        $this->call(Language::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MakeUpMethodsTableSeeder::class);
        $this->call(MoviesTableSeeder::class);
        $this->call(PlayListsTableSeeder::class);
        $this->call(SessionsTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductBrandsTableSeeder::class);
        $this->call(ProductCategorysTableSeeder::class);
        $this->call(DevicesTableSeeder::class);
        $this->call(DeviceConfigTableSeeder::class);
        $this->call(DeviceProductsTableSeeder::class);
        $this->call(MakeUpStylesTableSeeder::class);
        $this->call(MakeUpStyleSeesions::class);
        $this->call(ProductSeesions::class);
        $this->call(DeviceMakeUpStyles::class);

//        $this->call(DevicesTableSeeder::class);
//        $this->call(DeviceConfigTableSeeder::class);
//        $this->call(MoviesTableSeeder::class);
//        $this->call(BrandsTableSeeder::class);
//        $this->call(CategoriesTableSeeder::class);
//        $this->call(ProductsTableSeeder::class);
//        $this->call(ProductCategorysTableSeeder::class);
//        $this->call(ProductImagesTableSeeder::class);
//        $this->call(PlayListsTableSeeder::class);
//        $this->call(SessionsTableSeeder::class);
//        $this->call(DeviceProductsTableSeeder::class);
//        $this->call(ProductBrandsTableSeeder::class);
    }
}
