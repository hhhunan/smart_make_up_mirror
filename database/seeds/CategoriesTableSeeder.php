<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\CategoryTranslation;
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fakerRU = Faker::create('ru_RU');
        $fakerEN = Faker::create();

        for($i = 1; $i < 5; $i++)
        {
            $categorie = new Category;
            $categorie->status = 1;
            $categorie->save();

            $trans = new CategoryTranslation;
            $trans->name = $fakerEN->name;
            $trans->categorie_id = $categorie->id;
            $trans->language_id = 1;
            $trans->save();

            $trans = new CategoryTranslation;
            $trans->name = $fakerRU->name;
            $trans->categorie_id = $categorie->id;
            $trans->language_id = 2;
            $trans->save();
        }
    }
}
