<?php

use Illuminate\Database\Seeder;

class ProductSeesions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i =1; $i<3; $i++){
            $productSession = new \App\Models\ProductSession();
            $productSession ->product_id = $i;
            $productSession ->session_id = $i;
            $productSession->save();
        }
    }
}
