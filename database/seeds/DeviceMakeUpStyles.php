<?php

use Illuminate\Database\Seeder;

class DeviceMakeUpStyles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i =1; $i<3; $i++){
            $DeviceMakeUpStyle = new \App\Models\DeviceMakeUpStyle();
            $DeviceMakeUpStyle ->device_id = 1;
            $DeviceMakeUpStyle ->make_up_style_id = $i;
            $DeviceMakeUpStyle->save();
        }
    }
}
