<?php

use Illuminate\Database\Seeder;

class MakeUpStyleSeesions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i =1; $i<3; $i++){
            $styleSession = new \App\Models\MakeUpStyleSession();
            $styleSession ->make_up_style_id = $i;
            $styleSession ->session_id = $i+2;
            $styleSession->save();
        }
    }
}
