<?php

use Illuminate\Database\Seeder;
use App\ImageTypes;

class Language extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            "English" => [ "code" => "en", "default" => "1", "image_id" => 1, 'status'=>1],
            "Русский" => [ "code" => "ru", "default" => "0", "image_id" => 2, 'status'=>1]
        ];
        foreach($array as $key => $val)
        { 
            DB::table('languages')->insert([
                "name" => $key,
                "status" => $val['status'],
                "default" => $val['default'],
                "image_id" => $val['image_id'],
                "code" => $val['code']
            ]);
        }
    }
}
