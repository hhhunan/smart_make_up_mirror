<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        for ($i = 0; $i < 8; $i++) {
            $name = str_random(7) . ".mp4";
            DB::table('movies')->insert([
                'name' => $name,
                'uri' => env('UPLOAD_VIDEO_PATH') .'/' . $name,
                'MD5' => md5(str_random(32)),
                'format' => "mp4",
                'duration' => $faker->time('m:s'),
                'type' => 1,
                'file_size' => 100000,
                'status' => 1,
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
            ]);
        }
    }
}
