<?php

use Illuminate\Database\Seeder;

class DeviceProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\DeviceProduct::class, 2)->create();
    }
}
