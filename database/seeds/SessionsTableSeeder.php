<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Models\PlayList;
use App\Models\Session;
use App\Models\SessionTranslation;

class SessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fakerRU = Faker::create('ru_RU');
        $fakerEN = Faker::create();

        $pl = PlayList::groupBy('id')->get();
//        dd($pl->toArray());

        for($i = 0; $i < 4; $i++)
        {
            $session = new Session;
            $session->play_list_id = $pl[$i]["id"];
            $session->status = 1;
            if($i < 2){
                $session->type = 'make_up_by_product';
            }else{
                $session->type = 'make_up_by_style';
            }
            $session->save();

            $trans = new SessionTranslation;
            $trans->name = $fakerEN->name;
            $trans->description = $fakerEN->name;
            $trans->session_id = $session->id;
            $trans->language_id = 1;
            $trans->save();

            $trans = new SessionTranslation;
            $trans->name = $fakerRU->name;
            $trans->description = $fakerRU->text(180);
            $trans->session_id = $session->id;
            $trans->language_id = 2;
            $trans->save();

        }    
    }
}
