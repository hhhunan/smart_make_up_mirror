<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeviceConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $value = array(
            'processor', 'monitor size', 'windows', 'mac'
        );

        $fakerData = array(
            'info' => array(
                'OS Version' => 'Windows 10 x64 (Build 1234)', 
                'Monitor Size' => '1024x768', 
                'Processor' => 'Core i5',
                'Processor' => 'AMD Am2900 (Bulbulator)'
            ),
            'skins' => array(
                'dark' => 'R33 G33 B33',
                'white' => 'R180 G180 B180',
                'blue' => 'R2 G117 B216'
            ),
            'volume' => array(
                'default' => '10%',
                'custom' => '47%'
            )
        );
        $id =1;
        foreach ($fakerData as $group => $value1) {

            foreach ($value1 as $type => $value) {
                DB::table('device_configs')->insert([
                    'group' => $group,
                    'type' => $type,
                    'value' => $value,
                    'device_id' => $id,
                    'status' => 1,
                    'created_at' => $faker->dateTime('now'),
                    'updated_at' => $faker->dateTime('now'),
                ]);
            }
        }
    }
}