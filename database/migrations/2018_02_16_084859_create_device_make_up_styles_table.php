<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceMakeUpStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_make_up_styles', function (Blueprint $table) {
            $table->integer('device_id', false, true);
            $table->integer('make_up_style_id', false, true);
            $table->foreign('device_id')->references('id')->on('devices')->onDelete('cascade');
            $table->foreign('make_up_style_id')->references('id')->on('make_up_styles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_make_up_styles');
    }
}
