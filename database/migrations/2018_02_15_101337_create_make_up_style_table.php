<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakeUpStyleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('make_up_styles', function (Blueprint $table) {
                $table->increments('id');
                $table->smallInteger('status')->default(1);
            $table->integer('image_id', false, true)->unsigned();
            $table->foreign('image_id', 'style_images_foreign')->references('id')->on('images');
            $table->integer('make_up_method_id')->unsigned()->default(2);
            $table->foreign('make_up_method_id')->references('id')->on('make_up_methods');


            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('make_up_styles');
    }
}
