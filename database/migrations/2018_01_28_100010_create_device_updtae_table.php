<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceUpdtaeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash','64');
            $table->integer('status');
            $table->text('data');
            $table->integer('device_id')->unsigned()->nullable();
            $table->foreign('device_id')->references('id')->on('devices')->onDelete("Set null");
            $table->string('message','255')->nullable();
            $table->timestamp('modify_update_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('update_date_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_updates');
    }
}
