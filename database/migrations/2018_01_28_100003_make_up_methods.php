<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUpMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('make_up_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('status')->default(1);
            $table->integer('image_id', false, true)->unsigned();
            $table->foreign('image_id','image_id_foreign')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('make_up_methods');
    }
}
