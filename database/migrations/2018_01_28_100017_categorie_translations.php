<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategorieTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorie_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('categorie_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->foreign('categorie_id')->references('id')->on('categories')->onDelete("cascade");
            $table->foreign('language_id')->references('id')->on('languages')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorie_translations');
    }
}
