<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakeUpStyleTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('make_up_style_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('make_up_style_id')->unsigned();
            $table->foreign('make_up_style_id')->references('id')->on('make_up_styles')->onDelete("cascade");
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('make_up_style_translations');
    }
}
