<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Labels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_labels', function (Blueprint $table) {
            $table->integer('id', false, false)->default(1);
            $table->string('input');
            $table->string('translation');
            $table->integer('language_id', false, true)->nullable();
            $table->foreign('language_id','languages_id_foreign')->references('id')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
