<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imei_number', 100)->unique();
            $table->smallInteger('type')->default(1);
            $table->smallInteger('status')->default(0);
            $table->timestamp('modify_status_date');
            $table->integer('secret_id', false, true)->unique()->nullable();
            $table->integer('by_live_image_id', false, true)->unique()->nullable();
            $table->foreign('by_live_image_id','device_live_image_id_foreign')->references('id')->on('images')->onDelete('Set null');
            $table->integer('by_session_image_id', false, true)->unique()->nullable();
            $table->foreign('by_session_image_id','device_session_image_id_foreign')->references('id')->on('images')->onDelete('Set null');
//            $table->foreign('secret_id')->references('id')->on('oauth_clients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
