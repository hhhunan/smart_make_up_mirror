<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('play_lists', function (Blueprint $table) {
            $table->integer('id', false, true);
            $table->index('id');
            $table->integer('ordering')->default(1);
            $table->integer('movie_id', false, true);
            $table->foreign('movie_id', "movie_id_fork")->references('id')->on('movies')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('play_lists');
    }
}
