<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakeUpStyleSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('make_up_style_sessions', function (Blueprint $table) {
            $table->integer('make_up_style_id', false, true);
            $table->integer('session_id', false, true);
            $table->unique(['make_up_style_id', 'session_id']);
            $table->foreign('make_up_style_id')->references('id')->on('make_up_styles')->onDelete('cascade');
            $table->foreign('session_id')->references('id')->on('sessions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('make_up_style_sessions');
    }
}
