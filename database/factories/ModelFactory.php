<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

//$factory->define(App\Models\Movie::class, function (Faker\Generator $faker) {
//    return [
//        'name' => $faker->company,
//        'uri' => $faker->url,
//        'format' => $faker->mimeType,
//        'duration' => $faker->time('m:s'),
//        'type'=>1,
//        'status'=>1,
//    ];
//});
$factory->define(App\Models\Brand::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'status' => 1,
    ];
});

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'status' => 1,
    ];
});
$factory->define(App\Models\PlayList::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->numberBetween(1, 10),
        'movie_id' => rand(1,\App\Models\Movie::max('id')),
        'ordering' => rand()
    ];
});
$factory->define(App\Models\Session::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'play_list_id' => rand(1,\App\Models\PlayList::max('id')),
        'status'=> 1
    ];
});
$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        // 'name' => $faker->name,
        // 'description'=>$faker->text(),
        'status'=> 1
    ];
});
$factory->define(App\Models\ProductCategory::class, function (Faker\Generator $faker) {
    return [
        'product_id' => rand(1,\App\Models\Product::max('id')),
        'category_id'=>rand(1,\App\Models\Category::max('id')),
    ];
});
$factory->define(App\Models\ProductImage::class, function (Faker\Generator $faker) {
    return [
        'product_id' => rand(1,\App\Models\Product::max('id')),
        'image_id'=>rand(1,\App\Models\Image::max('id')),
    ];
});
$factory->define(App\Models\DeviceProduct::class, function (Faker\Generator $faker) {
    return [
        'device_id' => rand(1,\App\Models\Device::max('id')),
        'product_id' => rand(1,\App\Models\Product::max('id')),
    ];
});
$factory->define(App\Models\ProductBrand::class, function (Faker\Generator $faker) {
    return [
        'product_id' => rand(1,\App\Models\Product::max('id')),
        'brand_id'=>rand(1,\App\Models\Brand::max('id')),
    ];
});