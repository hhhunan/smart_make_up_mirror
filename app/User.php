<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Role;
use App\Models\Image;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * socialAccounts
     * @return mixed 
     */
    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * Role
     * @return mixed 
     */
    public function Role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
     * Role
     * @return mixed 
     */
    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }
    
	
	/**
	 * POST
	 * @var mixed
	 */
	public $ruleForCreate = [
        'name' => 'min:3|max:32',
        'password' => 'min:6|max:32',
        'confirm_password' => 'min:6|max:32|same:password',
        'email' => 'required|email|max:25|unique:users',
        'image' => 'required|min:150',
        "role_id" => 'numeric'
    ];
    
    /**
     * PUT
     * @var mixed
     */
    public $ruleForEdit = [
        'name' => 'min:3|max:32',
        'password' => 'min:6|max:32',
        'confirm_password' => 'min:6|max:32|same:password',
        'image' => 'min:150',
        "role_id" => 'numeric'
    ];
}
