<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Lang;
use Illuminate\Database\QueryException;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // Not application/json Accept
//         if ($request->expectsJson()) return parent::render($request, $e);

        // Accept application/json
        switch ($e) {
            case $e instanceof ModelNotFoundException:
                $param = $request->route()->parameters;
                return response()->json(['message' =>
                    Lang::get('custom.record_not_found', ['id' => array_values($param)[0], 'model' => array_keys($param)[0]])], 200);
                break;
            case $e instanceof NotFoundHttpException:
                return redirect("/not-found");
                break;
            case $e instanceof QueryException:
                return $this->queryException($e, $e->errorInfo, $request);
                break;
            case $e instanceof ValidationException:
                return response()->json(['message' => $e->getMessage(), 'errors' => $e->validator->errors()], 500);
                break;
            default:
                return response()
                    ->json(env('APP_DEBUG') ? ["catch" => $e->getMessage()] : ['message' => "The error prompted."], 500);
                break;
        }
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        return redirect()->guest("/login");
    }

    /**
     * @return sql query exception
     */
    public function queryException($e, $errorInfo, $request)
    {
        switch ($errorInfo[1]) {
            case 1452:
                return response()->json(['message' => Lang::get('custom.error_created')], 500);
                break;
            case 1054:
                return response()->json(['message' => Lang::get('custom.error_field')], 500);
                break;
            case 1052:
                $param = $request->route()->parameters;
                return response()->json(['message' =>
                    Lang::get('custom.record_not_found', ['id' => array_values($param)[0], 'model' => array_keys($param)[0]])], 500);
                break;
            default:
                return response()->json(env('APP_DEBUG') ? ['message' => $e->getMessage(), "db_error_info" => $errorInfo] : ["message" => "DB Error."], 500);
                break;
        }
    }
}
