<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{

    protected $guarded = [];

    /**
     * user
     * @return mixed 
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
