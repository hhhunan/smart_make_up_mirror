<?php

namespace App\Http\Middleware;

use App\Models\Device;
use Closure;
use App\OAuthClient;
use Illuminate\Support\Facades\Validator;

class DeviceAuth
{
    /**
     * Handle an Device Authentication.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try
        {
            $validation1 = Validator::make($request->all(), array(
                "client_id" => "required|numeric",
                "imei_number" => "required|min:10",
                "client_secret" => "required|min:40|max:40"
            ));
            if ($validation1->fails()) {
                return response()->json(['message' => $validation1->errors()->getMessages()], 400);
            }

            $id = $request->get("client_id");
            $key = $request->get("client_secret");
            $imei = $request->get("imei_number");

            $device = Device::where("imei_number", $imei);
            $client = OAuthClient::where("id", $id)->where("secret", $key);

            if($client->exists() && $device->exists())
            {
                if($client->where("id", $device->value("secret_id"))->exists())
                {
                    return $next($request);
                }
            }
            return response()->json(['message' => "Authentication error.."], 401);
        }
        catch (\Exception $e) {
            return response()->json(['message' => "The error prompted." /*, "catch" => $e->getMessage()*/ ], 400);
        }
    }
}
