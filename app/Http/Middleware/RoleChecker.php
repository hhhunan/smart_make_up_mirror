<?php
namespace App\Http\Middleware;

use App\Models\Role;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\RoleSettings;

class RoleChecker extends RoleSettings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // try
        // {
            \App::setLocale($request->header("language"));

            $status = $this->check($request, $this->settings);
            
            if ($status) {
                 return $next($request);
            }

            return response(\Lang::get('custom.access') . " - [" . implode(", ", $status) . "]", 403);
        // } catch (\Exception $e) {
            // return response()->json(['message' => "ROLE: This API is not configured in the access file." , "catch" => $e->getMessage() ], 500);
        // }
    }

    /*
     * Processing roles.
     * */
    public function check($request, $settings)
    {
        // try
        // {
            // Retrieving the authenticated user
            $user = Auth::user();

            // Search role_id in the database and return the role name.
            $user_role = Role::where('id', $user->role_id)->value('name');

            // The request is processed and broken into arrays.
            $_uses = explode('@', $request->route()->getAction()["uses"]);
            $_controller = explode('s\\', $_uses[0])[1];
            $function_name = $_uses[1];

            // checking user's access of methods.
            if (array_search($user_role, $settings[$function_name]["roles"]) === false) {
                return $settings[$function_name]["roles"]; // Denial of access
            }
            
            if (isset($settings[$function_name]["controller"][$_controller])) {
                if (isset($settings[$function_name]["controller"][$_controller]["roles"])) {
                    // checking the user's access of controller.
                    if (!isset($settings[$function_name]["controller"][$_controller]["roles"][$user_role])) {
                        return array_keys($settings[$function_name]["controller"][$_controller]["roles"]); // Denial of access
                    }

                    // сhecks whether it is worth checking further.
                    if ($settings[$function_name]["controller"][$_controller]["roles"][$user_role] === false) {
                        return true; // Access is allowed
                    }
                }

                if (isset($settings[$function_name]["controller"][$_controller]["route"])) {
                    // checking the user's access of id.
                    if ($user->id != $request->route($settings[$function_name]["controller"][$_controller]["route"])) {
                        return array_keys($settings[$function_name]["controller"][$_controller]["roles"]); // Denial of access
                    }
                }
            }

            return true; // Access is allowed
        // } catch (\Exception $e) {
            // return response()->json(['message' => "ROLE: This API is not configured in the access file." , "catch" => $e->getMessage() ], 500);
        // }
    }
}
