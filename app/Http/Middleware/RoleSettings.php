<?php
namespace App\Http\Middleware;

class RoleSettings
{
    public $roles = [
        [ "name" => "administrator", "label" => "Administrator" ],
        [ "name" => "editor", "label" => "Moderator" ],
        [ "name" => "reviewer", "label" => "User" ],
    ];
    public $settings = [
        "index" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
            "controller" => [
                "UserController" => [
                    "roles" => [
                        "administrator" => false,
                    ],
                ],
            ],
        ],
        "show" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
            "controller" => [
                "UserController" => [
                    "roles" => [
                        "administrator" => false,
                    ],
                ],
            ],
        ],
        "update" => [
            "roles" => [
                "editor",
                "administrator",
            ],
            "controller" => [
                "UserController" => [
                    "roles" => [
                        "administrator" => false,
                    ],
                    "route" => "reviewer"
                ],
            ],
        ],
        "get_user" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "getProduct" => [
            "roles" => [
                "reviewer", "editor",
                "administrator",
            ],
        ],
        "getStyle" => [
            "roles" => [
                "reviewer", "editor",
                "administrator",
            ],
        ],
        "getDevice" => [
            "roles" => [
                "reviewer", "editor",
                "administrator",
            ],
        ],
        "getBrand" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "setStatus" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "getSession" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "getCategory" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "store" => [
            "roles" => [
                "administrator",
            ],
        ],
        "destroy" => [
            "roles" => [
                "administrator",
            ],
        ],
        "addProducts" => [
            "roles" => [
                "administrator",
            ],
        ],
        "addStyle" => [
            "roles" => [
                "administrator",
            ],
        ],
        "editMyProfile" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "UserPersonalData" => [
            "roles" => [
                "administrator",
            ],
        ],
        "UserRoleSystem" => [
            "roles" => [
                "administrator",
            ],
        ],
        "UserPassword" => [
            "roles" => [
                "administrator",
            ],
        ],
        "destroy_device" => [
            "roles" => [
                "administrator",
            ],
        ],
        "destroy_device_style" => [
            "roles" => [
                "administrator",
            ],
        ],
        "destroy_brand" => [
            "roles" => [
                "administrator",
            ],
        ],
        "destroy_session" => [
            "roles" => [
                "administrator",
            ],
        ],
        "destroy_category" => [
            "roles" => [
                "administrator",
            ],
        ],
        "addBrands" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "addCategory" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "addDevices" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "getGroup" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "addSession" => [
            "roles" => [
                "administrator",
            ],
        ],
        "getType" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "getPlayList" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "getMovieList" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "getStatus" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "logout" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],
        "getAllImagesByType" => [
            "roles" => [
                "reviewer",
                "editor",
                "administrator",
            ],
        ],

        "GenerateSecretKey" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "getImageTypes" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "getDevicesAndUpdates" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "searchByImaiNumberByHash" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "editDeviceUpdate" => [
            "roles" => [
                "administrator",
            ],
        ],
        "destroyDeviceUpdate" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "getDeviceUpdate" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "destroy_device_product" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "analytic" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "updated" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "getDeviceUpdate_show" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "getUpdateDate" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
        "getUpdateStatus" => [
            "roles" => [
                "editor",
                "administrator",
            ],
        ],
    ];
}
