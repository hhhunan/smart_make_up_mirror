<?php

namespace App\Http\Controllers;

use App\AddonLib;
use App\Models\MakeUpMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Device;
use App\Models\DeviceLabel;
use App\Models\DeviceUpdate;
use Illuminate\Validation\Rule;
use DB;

class DeviceUpdateData extends Controller
{
    public $lang_code;

    public $deviceStatusArray = [
        'undefined' => 0,
        'successfully' => 1,
        'error' => 2,
    ];

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
    }

    public function CreateUpdate($device, $data_hash)
    {

        $device_update = new DeviceUpdate();

        $device_update->device_id = $device['id'];
        $device_update->status = 0;
        $device_update->modify_update_date = date('Y-m-d h:i:s', time());
        $device_update->hash = $data_hash;
        $device_update->data = $device;
        if (!$device_update->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 500);
        }

        $device['hash'] = DeviceUpdate::select("hash", "device_id")->orderBy('device_id', 'desc')->first();
        return response()->json($device, 200);
    }

    public function UpdateDate($device)
    {
        $device_update = DeviceUpdate::where('device_id', $device['id'])->first();
        $device_update->modify_update_date = date('Y-m-d h:i:s', time());
        if ($device_update = DeviceUpdate::where('device_id', $device['id'])->where('status', '0')) {
            return response()->json($device, 200);
        }
        if (!$device_update->save()) {

            return response()->json(['message' => \Lang::get('custom.error_save')], 500);
        }
    }

    public function UpdateDateStatus($device)
    {
        $device_update = DeviceUpdate::where('device_id', $device['id'])->first();
        $device_update->update_date_status = date('Y-m-d h:i:s', time());
        if (!$device_update->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 500);
        }
    }

    public function data_version(Request $request)
    {
        $validation1 = Validator::make($request->all(), array(
            "imei_number" => "required|min:10"
        ));
        if ($validation1->fails()) {
            return response()->json(['message' => $validation1->errors()->getMessages()], 400);
        }

        // Get Device by imei number
        $device = Device::where('imei_number', $request->get('imei_number'))->first();

        // Returned, not found items messages.
        if (!$device) {
            return response()->json(['message' => \Lang::get('custom.no_item')], 500);
        }

        $id = $device['id'];
        // Get all db data and marge
        $device = Device::with([
            'by_live_image',
            'by_session_guide_image',
            'products' => function ($query) {
                $query->where('status', '1');
                $query->with([
                    'sessions' => function ($query) {
                        $query->with(["translation" => function ($query) {
                            $query->with('language')->whereHas('language', function ($query) {
                                $query->where('status', '1');
                            });
                        }]);
                        $query->where('status', '1');
                        $query->with(['play_list' => function ($query) {
                            $query->with(['movie' => function ($query) {
                                $query->where('status', '1');
                            }]);
                        }]);
                    },
                    'brands' => function ($query) {
                        $query->with('images');
                        $query->with(["translation" => function ($query) {
                            $query->with('language')->whereHas('language', function ($query) {
                                $query->where('status', '1');
                            });
                        }]);
                        $query->where('status', '1');
                    },
                    'images' => function ($query) {
                        $query->where('status', '1');
                    },

                    "translation" => function ($query) {
                        $query->with('language')->whereHas('language', function ($query) {
                            $query->where('status', '1');
                        });
                    }
                ]);
            },
            'styles' => function ($query) {
                $query->where('status', '1');
                $query->with([
                    'sessions' => function ($query) {
                        $query->with(["translation" => function ($query) {
                            $query->with('language')->whereHas('language', function ($query) {
                                $query->where('status', '1');
                            });
                        }]);
                        $query->where('status', '1');
                        $query->with(['play_list' => function ($query) {
                            $query->with(['movie' => function ($query) {
                                $query->where('status', '1');
                            }]);
                        }]);
                    },
                    'images',
                    "translation" => function ($query) {
                        $query->with('language')->whereHas('language', function ($query) {
                            $query->where('status', '1');
                        });
                    }

                ]);
            },

            'device_config',
            "translation" => function ($query) {
                $query->with('language')->whereHas('language', function ($query) {
                    $query->where('status', '1');
                });
            }


        ])->findOrFail($id);
        $device['Make_Up_Method'] =
            MakeUpMethod::with(["translation" => function ($query) {
                $query->with('language')->whereHas('language', function ($query) {
                    $query->where('status', '1');
                });
            }, 'images'])->get();
//

        $device['label'] = $labels = DeviceLabel::select(
            "device_labels.id",
            "device_labels.input",
            "device_labels.translation",
            "languages.code",
            "languages.name"
        )->join("languages", "device_labels.language_id", "=", "languages.id")->get()->groupBy('input')->values();

        $data_hash = hash('sha256', json_encode($device->toArray()));

        // Check exists device id ind device update table
        $hash = $request->get('hash');

        $update = DeviceUpdate::getQuery();
        if ($request->has('hash')) {
            if (!$update->where('hash', $hash)->where('device_id', $id)->exists()) {

                return response()->json(['message' => "Error hash code."], 400);

            }
        } else {
            if (!$update = DeviceUpdate::where('device_id', $id)->where('status', '1')->exists()) {
                $device_update = new DeviceUpdate();
                $device_update->device_id = $device['id'];
                $device_update->status = 0;
                $device_update->modify_update_date = date('Y-m-d h:i:s', time());
                $device_update->hash = $data_hash;
                $device_update->data = $device;
                if (!$device_update->save()) {
                    return response()->json(['message' => \Lang::get('custom.error_save')], 500);
                }

                $device['hash'] = DeviceUpdate::select("hash", "device_id")->orderBy('device_id', 'desc')->first();
                return response()->json($device, 200);
//                return response()->json(['message' => "Error hash code."], 400);

            }
            return response()->json(['message' => "Error hash code."], 400);

        }

        if ($update->where('device_id', $id)->exists()) {
            // Starting validator
            $validation2 = Validator::make($request->all(), array(
                "hash" => "required|min:64|max:64"
            ));
            if ($validation2->fails()) {
                return response()->json(['message' => $validation2->errors()->getMessages()], 400);

            }

            if ($hash == $data_hash) {
                // update device last update status
                $this->UpdateDate($device);

                return response()->json(['message' => \Lang::get('custom.latest_updates')], 201);
            } else {
                if (DeviceUpdate::where('hash', $data_hash)->exists()) {

                    $this->UpdateDate($device);

                    return response()->json(['message' => \Lang::get('custom.latest_updates')], 201);
                }
                // CREATE
                return $this->CreateUpdate($device, $data_hash);
            }
        }

        // CREATE
        return $this->CreateUpdate($device, $data_hash);
    }

    public function change_status(Request $request)
    {
        // Starting validator.
        $validation1 = Validator::make($request->all(), array_add([
            "imei_number" => "required|min:10",
            "hash" => "required|min:64|max:64",
            "status" => "required|numeric",
            "message" => "required|min:2|max:255",
        ], 'status', Rule::in(array_keys($this->deviceStatusArray))));
        if ($validation1->fails()) {
            return response()->json(['message' => $validation1->errors()->getMessages()], 400);
        }

        // Get device by imei number
        $hash = $request->get('hash');
        $device = Device::where('imei_number', $request->get('imei_number'))->first();

        // Device check exists item.
        if (!$device) {
            return response()->json(['message' => \Lang::get('custom.no_item')], 500);
        }

        // Change status
        if ($request->has('status')) {
            DeviceUpdate::where('device_id', $device->id)->where("hash", $hash)->update([
                'status' => $this->deviceStatusArray[$request->get('status')],
                'message' => $request->get('message')
            ]);
            $this->UpdateDateStatus($device);
            return response()->json(['message' => \Lang::get('custom.success_updated')], 200);
        }

        // Returned invalid status messages
        return response()->json(['message' => "Invalid status."], 400);
    }

    public function getDeviceUpdate_show($id)
    {
        // Get device by ID or All
        $devices = DeviceUpdate::find($id);
        if ($devices->exists()) {
            $devices['status'] = array_search($devices->status, $this->deviceStatusArray);
            $devices['status_list'] = $this->deviceStatusArray;
            return $devices;
        }

        // Returned not items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 400);
    }

    public function getDevicesAndUpdates()
    {
        // get all device updates
        $devices = Device::with('translation', 'device_updates')->paginate(10)->toArray();
        array_set($devices, 'status_list', $this->deviceStatusArray);
        if (!$devices) {
            return response()->json(['message' => \Lang::get('custom.no_item')], 400);

        }
        return $devices;
    }

    public function getUpdateDate(Request $request)
    {
        $validation1 = Validator::make($request->all(), array_add([], 'sort', Rule::in('asc', 'desc','norm')));
        if ($validation1->fails()) {
            return response()->json(['message' => $validation1->errors()->getMessages()], 400);
        }

        if ($request->get('sort') === 'asc') {

//            return DB::table('device_updates')->orderBy('modify_update_date', 'asc')->get();
            return Device::with(["translation" => function ($query) {
                if ($this->lang_code) {
                    $query->with('language')->whereHas('language', function ($query) {
                        $query->where('code', $this->lang_code);
                    });
                } else
                    $query->with('language');
            }, 'device_updates' => function ($query) {
                $query->orderBy('modify_update_date', 'asc');
            }
            ])->paginate(12);
        }

        if ($request->get('sort') === 'desc') {

//            return DB::table('device_updates')->orderBy('modify_update_date', 'desc')->get();
            return   Device::with(["translation" => function ($query) {
                if ($this->lang_code) {
                    $query->with('language')->whereHas('language', function ($query) {
                        $query->where('code', $this->lang_code);
                    });
                } else
                    $query->with('language');
            }, 'device_updates' => function ($query) {
                $query->orderBy('modify_update_date', 'desc');
            }
            ])->paginate(12);

        }
        if ($request->get('sort') === 'norm') {

//            return DB::table('device_updates')->get();
            return   Device::with(["translation" => function ($query) {
                if ($this->lang_code) {
                    $query->with('language')->whereHas('language', function ($query) {
                        $query->where('code', $this->lang_code);
                    });
                } else
                    $query->with('language');
            }, 'device_updates'])->paginate(12);

        }

    }

    public function getUpdateStatus(Request $request)
    {
        $validation1 = Validator::make($request->all(), array_add([], 'Updated_status', Rule::in('undefined', 'successfully', 'error')));
        if ($validation1->fails()) {
            return response()->json(['message' => $validation1->errors()->getMessages()], 400);
        }
        if ($request->get('Updated_status') != null || $request->get('Updated_status') != '') {

//            return DB::table('device_updates')->where('status', '=', $this->deviceStatusArray[$request->get('Updated_status')])->get();
         return   Device::with(["translation" => function ($query) {
                if ($this->lang_code) {
                    $query->with('language')->whereHas('language', function ($query) {
                        $query->where('code', $this->lang_code);
                    });
                } else
                    $query->with('language');
            }, 'device_updates' => function ($query) use ($request) {
                $query->where('status', '=', $this->deviceStatusArray[$request->get('Updated_status')]);
            }
            ])->paginate(12);
        }
        return response()->json(['message' => \Lang::get('custom.no_item')], 400);

    }


}

