<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Language;
use App\Models\CategoryTranslation;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\AddonLib;
use DB;

class CategoryController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get Language id by LangCode (en/es/ru..)
        $lang_id = Language::where('code', $this->lang_code ? $this->lang_code : "en")->value('id');

        // Get all Categories
        $category = Category::with(["translation" => function ($query) use ($lang_id) {
            $query->with('language')->where('language_id', $lang_id);
        }]);

        // Categories translations options
        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($category, [
                "join" => [
                    "select" => "categories.*",
                    "table" => "categorie_translations",
                    "first" => "categories.id",
                    "second" => "categorie_translations.categorie_id",
                    "type" => "inner"
                ],
                "search" => $this->addon->checkUriParameter($request, 'search') ? [
                    "column" => "categorie_translations.name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? [
                    "column" => "categorie_translations.name",
                    "flag" => $request->get('sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }
        else
        {
            $category->orderBy('id', 'desc');
        }

        // Pagination
        $page = $category->paginate(10)->toArray();
        if (sizeof($page['data'])) {
            $page['sort_type'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            return $page;
        }

        // Returned, not found items messages.
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Create new Object Category.
        $category = new Category();

        // Starting validator.
        $validation = Validator::make($request->all(), array_add($category->ruleForCreate, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $category->status = $request->has('status') ? $request->get('status') : 0;

        // Saved Category object
        if (!$category->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // Starting transaction
        DB::transaction(function () use ($category, $request) {
            // category transaction
            $languages = Language::pluck('code', 'id');
            $translations = [];
            foreach ($languages as $lngId => $lngCode) {
                if (array_key_exists($lngCode, $request->get('name'))) {
                    $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                    $translations[$lngId]['categorie_id'] = $category->id;
                    $translations[$lngId]['language_id'] = $lngId;
                }
            }
            $category->translationForSync()->sync($translations);
        }, 3);

        // Returned created success messages
        return response()->json(["messages" => \Lang::get('custom.success_creat')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Category db joins
        return Category::with(["translation" => function ($query) {
            if ($this->lang_code)
                $query->with('language')->whereHas('language', function ($query) {
                $query->where('code', $this->lang_code);
            });
            else
                $query->with('language');
        }])->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Starting validator
        $validation = Validator::make($request->all(), array_add((new Category)->ruleForEdit, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $category = Category::where("id", $id)->first();
        $category->status = $request->has('status') ? $request->get('status') : $category->status;

        // Saved category object.
        if (!$category->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // Starting transaction
        if ($request->get('name')) {
            DB::transaction(function () use ($category, $request) {
                $languages = Language::pluck('code', 'id');
                $translations = [];
                foreach ($languages as $lngId => $lngCode) {
                    if (array_key_exists($lngCode, $request->get('name'))) {
                        $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                        $translations[$lngId]['categorie_id'] = $category->id;
                        $translations[$lngId]['language_id'] = $lngId;
                    }
                }
                $category->translationForSync()->sync($translations);
            }, 3);
        }

        // Returned success updated messages.
        return response()->json(["messages" => \Lang::get('custom.success_updated')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::where("id", $id);

        $Product_id=ProductCategory::where('category_id',$id)->value('product_id');
        if ($category->exists()) {
            // Destroyed category in products.
            ProductCategory::where('category_id', $id)->delete();

            if ($category->delete()) {
                if(!ProductCategory::where('product_id',$Product_id)->count())
                    Product::where('id', $Product_id)->update(['status' => '0']);
                // Returning success destroy messages.
                return response()->json(['message' => \Lang::get('custom.destroy')], 200);
            }
        }
        // Returned error updated messages.
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }

    /**
     * getCategory
     * @param mixed $request
     * @return mixed
     */
    public function getCategory()
    {
        // Get all Categories
        return Category::select('categories.id')->with(["translation" => function ($query) {
            $query->with('language')->whereHas('language', function ($query) {
                $query->where('code', "en");
            });
        }])->get();
    }
}
