<?php

namespace App\Http\Controllers;

use App\Models\MakeUpMethod;
use Illuminate\Http\Request;
use App\Models\MakeUpStyle;
use App\Models\MakeUpStyleSession;
use App\Models\MakeUpMethodImage;
use App\Models\Language;
use App\Models\MakeUpMethodTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\AddonLib;
use DB;


class MakeUpMethodController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    public function index()
    {
        $lang_id = Language::where('code', $this->lang_code ? $this->lang_code : "en")->value('id');
        $MakeUpStyle =
            MakeUpMethod::with([
                "translation" => function ($query) use ($lang_id) {
                    $query->with('language')->where('language_id', $lang_id);
                },
                'images',
                    ]);


        return $MakeUpStyle->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $MakeUpMethod = MakeUpMethod::where('id', $id)->with([
            'images',
                "translation" => function ($query) {
                if ($this->lang_code)
                    $query->with('language')->whereHas('language', function ($query) {
                        $query->where('code', $this->lang_code);
                    });
                else
                    $query->with('language');
            }

        ]);

        if ($MakeUpMethod->exists()) {
            return $MakeUpMethod->first();
        }

        // returned no items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return count($request->all());

        $validation = Validator::make($request->all(), array_add((new MakeUpMethod())->ruleForEdit, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        $MakeUpMethod = MakeUpMethod::where("id", $id)->first();

        $MakeUpMethod->status = $request->has('status') ? $request->get('status') : $MakeUpMethod->status;

        if($request->has('image')) {
            $image = $this->addon->fileUploader($request->get('image'), 'method', true, $MakeUpMethod->image_id);
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $MakeUpMethod->image_id = $image;
        }

        if (!$MakeUpMethod->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // starting translations
        if ($request->get('name')) {
            DB::transaction(function () use ($MakeUpMethod, $request) {

                $languages = Language::pluck('code', 'id');
                $translations = [];
                foreach ($languages as $lngId => $lngCode) {
                    if (array_key_exists($lngCode, $request->get('name'))) {
                        $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                        $translations[$lngId]['make_up_method_id'] = $MakeUpMethod->id;
                        $translations[$lngId]['language_id'] = $lngId;
                    }
                }
                $MakeUpMethod->translationForSync()->sync($translations);

            }, 3);
        }
        return response()->json(["messages" => \Lang::get('custom.success_updated')], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
