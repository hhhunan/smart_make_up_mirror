<?php

namespace App\Http\Controllers;

use App\Models\DeviceMakeUpStyle;
use Illuminate\Http\Request;
use App\Models\MakeUpStyle;
use App\Models\MakeUpStyleSession;
use App\Models\Device;
use App\Models\MakeUpStyleImage;
use App\Models\Language;
use App\Models\MakeUpStyleTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\AddonLib;
use DB;
use Illuminate\Support\Facades\Storage;


class MakeUpStyleController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    public function index(Request $request)
    {
        $lang_id = Language::where('code', $this->lang_code ? $this->lang_code : "en")->value('id');
        $MakeUpStyle =
            MakeUpStyle::with([
                'images',
                'devices' => function ($query) {
                    (new AddonLib)->translation($query, $this->lang_code);
                },
                'sessions' => function ($query) {
                    (new AddonLib)->translation($query, $this->lang_code);
                    $query->with(['play_list' => function ($query) {
                        $query->with(['movie']);
                    }]);
                },
                "translation" => function ($query) use ($lang_id) {
                    $query->with('language')->where('language_id', $lang_id);
                }
            ]);
        // Style options
        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($MakeUpStyle, [
                "join" => ["select" => "make_up_styles.*", "table" => "make_up_style_translations",
                    "first" => "make_up_styles.id", "second" => "make_up_style_translations.make_up_style_id", "type" => "inner"
                ],
                "search" => $this->addon->checkUriParameter($request, 'search') ? ["column" => "make_up_style_translations.name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? ["column" => "make_up_style_translations.name",
                    "flag" => $request->get('sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }
        // Session options
        if ($this->addon->checkUriParameter($request, 'session_search') || $this->addon->checkUriParameter($request, 'session_sort')) {
            $MakeUpStyle->join('make_up_style_sessions', 'make_up_styles.id', '=', 'make_up_style_sessions.make_up_style_id', 'left');
            $this->addon->customSortSearch($MakeUpStyle, [
                "join" => ["select" => "make_up_styles.*", "table" => "session_translations",
                    "first" => "session_translations.session_id", "second" => "make_up_style_sessions.session_id", "type" => "left"
                ],
                "search" => $this->addon->checkUriParameter($request, 'session_search') ? ["column" => "session_translations.name",
                    "pattern" => $request->get('session_search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'session_sort') ? ["column" => "session_translations.name",
                    "flag" => $request->get('session_sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }

        // Devices options
        if ($this->addon->checkUriParameter($request, 'device_search') || $this->addon->checkUriParameter($request, 'device_sort')) {
            $MakeUpStyle->join('device_make_up_styles', 'make_up_styles.id', '=', 'device_make_up_styles.make_up_style_id', 'left');
            $this->addon->customSortSearch($MakeUpStyle, [
                "join" => ["select" => "make_up_styles.*", "table" => "device_translations",
                    "first" => "device_translations.device_id", "second" => "device_make_up_styles.device_id", "type" => "left"
                ],
                "search" => $this->addon->checkUriParameter($request, 'device_search') ? ["column" => "device_translations.name",
                    "pattern" => $request->get('device_search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'device_sort') ? ["column" => "device_translations.name",
                    "flag" => $request->get('device_sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }
        if (!($this->addon->checkUriParameter($request, 'session_search')
            || $this->addon->checkUriParameter($request, 'device_search')
            || $this->addon->checkUriParameter($request, 'search'))) {
            $MakeUpStyle->orderBy('id', 'desc');
        }

        $MakeUpStyle->groupBy('make_up_styles.id');


        // paginate to Array
        $page = $MakeUpStyle->paginate(10)->toArray();
        if (sizeof($page['data'])) {

            // marge sort type in paginate
            $page['sort_type_style'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            $page['sort_type_session'] = $this->addon->checkUriParameter($request, 'session_sort') ? $request->get('session_sort') : "NORMAL";
            $page['sort_type_device'] = $this->addon->checkUriParameter($request, 'device_sort') ? $request->get('device_sort') : "NORMAL";

            return $page;
        }

        // returned no items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);

//        return $MakeUpStyle->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create new Product object
        $MakeUpStyle = new MakeUpStyle();

        // starting validation
        $validation = Validator::make($request->all(), array_add($MakeUpStyle->ruleForCreate, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $MakeUpStyle->status = $request->has('status') ? $request->get('status') : 0;
        if($request->has('image')){
            $image = $this->addon->fileUploader($request->get('image'), 'style');
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $MakeUpStyle->image_id = $image;
        }
        // saved Product object
        if (!$MakeUpStyle->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // starting translations
        DB::transaction(function () use ($MakeUpStyle, $request) {
            $languages = Language::pluck('code', 'id');
            $translations = [];
            foreach ($languages as $lngId => $lngCode) {
                if (array_key_exists($lngCode, $request->get('name'))) {
                    $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                    $translations[$lngId]['make_up_style_id'] = $MakeUpStyle->id;
                    $translations[$lngId]['language_id'] = $lngId;
                }
            }
            $MakeUpStyle->translationForSync()->sync($translations);


            if ($request->has('session_ids')) {
                $MakeUpStyle->sessions()->sync($request->get('session_ids'));
            }
        }, 3);
        if(!$MakeUpStyle->sessions()->exists())
        {
            $MakeUpStyle->status=0;
            $MakeUpStyle->save();
        }

        // returned successfully messages
        return response()->json(["messages" => \Lang::get('custom.success_creat')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $MakeUpStyle = MakeUpStyle::where('id', $id)->with([
            'sessions' => function ($query) {
                (new AddonLib)->translation($query, $this->lang_code);
                $query->with(['play_list' => function ($query) {
                    $query->with(['movie']);
                }]);
            },
            'devices' => function ($query) {
                (new AddonLib)->translation($query, $this->lang_code);
            },
            'images',
            "translation" => function ($query) {
                if ($this->lang_code)
                    $query->with('language')->whereHas('language', function ($query) {
                        $query->where('code', $this->lang_code);
                    });
                else
                    $query->with('language');
            }
        ]);

        if ($MakeUpStyle->exists()) {
            return $MakeUpStyle->first();
        }

        // returned no items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // starting validation
        $validation = Validator::make($request->all(), array_add((new MakeUpStyle)->ruleForEdit, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $MakeUpStyle = MakeUpStyle::where("id", $id)->first();
        $MakeUpStyle->status = $request->has('status') ? $request->get('status') : $MakeUpStyle->status;

        if($request->has('image')) {
            $image = $this->addon->fileUploader($request->get('image'), 'style', true, $MakeUpStyle->image_id);
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $MakeUpStyle->image_id = $image;
        }

        if (!$MakeUpStyle->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // starting translations
        if ($request->get('name')) {
            DB::transaction(function () use ($MakeUpStyle, $request) {

                $languages = Language::pluck('code', 'id');
                $translations = [];
                foreach ($languages as $lngId => $lngCode) {
                    if (array_key_exists($lngCode, $request->get('name'))) {
                        $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                        $translations[$lngId]['make_up_style_id'] = $MakeUpStyle->id;
                        $translations[$lngId]['language_id'] = $lngId;
                    }
                }
                $MakeUpStyle->translationForSync()->sync($translations);


//                if (!$request->has('session_ids') ||  $request->get('session_ids').isEmpty()) {
//                        $MakeUpStyle->status=0;
//                    $MakeUpStyle->save();
//                }
                    /*  Product Sessions  */
                if ($request->has('session_ids')) {
                    $MakeUpStyle->sessions()->sync($request->get('session_ids'));
                }

            }, 3);
        }
        if(!$MakeUpStyle->sessions()->exists())
        {
            $MakeUpStyle->status=0;
            $MakeUpStyle->save();
        }
        return response()->json(["messages" => \Lang::get('custom.success_updated')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $MakeUpStyle =MakeUpStyle::findOrfail($id);
        if(isset($MakeUpStyle->images->uri)){
            $path = $MakeUpStyle->images->uri;
            if (file_exists(public_path($path))) {
                unlink(public_path($path));
            }
        }
        $device_id = DeviceMakeUpStyle::where('make_up_style_id', $id)->value('device_id');

        if ($MakeUpStyle->delete()) {

            if (!DeviceMakeUpStyle::where('device_id', $device_id)->count())
                Device::where("id", $device_id)->update(['status' => '0']);
            // Returning success destroy messages.
            return response()->json(['message' => \Lang::get('custom.destroy')], 200);
        }
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }
    public function analytic()
    {
        $data['created'] = DB::table('make_up_styles')
            ->select(DB::raw('count(*) as created_count, MONTH(created_at) month, YEAR(created_at) YEAR'))
            ->groupBy('month')
            ->get();
        return $data;

    }
}
