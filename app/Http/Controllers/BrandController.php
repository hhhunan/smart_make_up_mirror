<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\BrandTranslation;
use App\Models\Image;
use App\Models\Language;
use App\Models\Product;
use App\Models\ProductBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\AddonLib;
use DB;

class BrandController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get Language id by LangCode (en/es/ru..)
        $lang_id = Language::where('code', $this->lang_code ? $this->lang_code : "en")->value('id');

        // Get all brands
        $brand = Brand::with(['images', "translation" => function ($query) use ($lang_id) {
            $query->with('language')->where('language_id', $lang_id);
        }]);

        // Brands translation options
        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($brand, [
                "join" => [
                    "select" => "brands.*",
                    "table" => "brand_translations",
                    "first" => "brands.id",
                    "second" => "brand_translations.brand_id",
                    "type" => "inner"
                ],
                "search" => $this->addon->checkUriParameter($request, 'search') ? [
                    "column" => "brand_translations.name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? [
                    "column" => "brand_translations.name",
                    "flag" => $request->get('sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }
        else
        {
            $brand->orderBy('id', 'desc');
        }
        
        // Paginations
        $page = $brand->paginate(10)->toArray();
        if (sizeof($page['data'])) {
            $page['sort_type'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            return $page;
        }

        // Returned, not found items messages.
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Create new Object Brand.
        $brand = new Brand();
        
        // Starting validator.
        $validation = Validator::make($request->all(), array_add($brand->ruleForCreate, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $brand->status = $request->has('status') ? $request->get('status') : 1;

        if($request->has('image')){
            $image = $this->addon->fileUploader($request->get('image'), 'brand');
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $brand->image_id = $image;
        }

        // Saved Brand object
        if (!$brand->save()) {
            // Returned created error messages
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // Starting transaction
        DB::transaction(function () use ($brand, $request) {
            // Brand transaction
            $languages = Language::pluck('code', 'id');
            $translations = [];
            foreach ($languages as $lngId => $lngCode) {
                if (array_key_exists($lngCode, $request->get('name'))) {
                    $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                    $translations[$lngId]['brand_id'] = $brand->id;
                    $translations[$lngId]['language_id'] = $lngId;
                }
            }
            $brand->translationForSync()->sync($translations);
        }, 3);

        // Returned created success messages
        return response()->json(["messages" => \Lang::get('custom.success_creat')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Brand db joins
        $brand = Brand::with(['images', 'products' => function ($query) {
            $this->products_counts = $query->count('id');
            $query->with('images');
        }, "translation" => function ($query) {
            if ($this->lang_code)
                $query->with('language')->whereHas('language', function ($query) {
                $query->where('code', $this->lang_code);
            });
            else
                $query->with('language');
        }])->findOrFail($id);

        // Marge product counts
        $brand["products_counts"] = $this->products_counts;
        return $brand;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Starting validator
        $validation = Validator::make($request->all(), array_add((new Brand)->ruleForEdit, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        $brand = Brand::where("id", $id)->first();

        $brand->status = $request->has('status') ? $request->get('status') : $brand->status;

        if($request->has('image')) {
            $image = $this->addon->fileUploader($request->get("image"), 'brand', true, $brand->image_id);
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $brand->image_id = $image;
        }
        
        // Saved brand object.
        if (!$brand->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }
        
        // Starting transaction
        if ($request->get('name')) {
            DB::transaction(function () use ($brand, $request) {
                $languages = Language::pluck('code', 'id');
                $translations = [];
                foreach ($languages as $lngId => $lngCode) {
                    if (array_key_exists($lngCode, $request->get('name'))) {
                        $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                        $translations[$lngId]['brand_id'] = $brand->id;
                        $translations[$lngId]['language_id'] = $lngId;
                    }
                }
                $brand->translationForSync()->sync($translations);
            }, 3);
        }

        // Returned success updated messages.
        return response()->json(["messages" => \Lang::get('custom.success_updated')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $brand = Brand::findOrFail($id);
        // remove files
        if (file_exists(public_path($brand->images->uri))) {
            unlink(public_path($brand->images->uri));
        }
        if ($brand->exists()) {

            $product_id = ProductBrand::where('brand_id', $id)->value('product_id');
            // Destroyed brand in products.
            ProductBrand::where('brand_id', $id)->delete();


            if ($brand->delete()) {
                if (!ProductBrand::where('product_id', $product_id)->count())
                    Product::where("id", $product_id)->update(['status' => '0']);

                // Returning success destroy messages.
                return response()->json(['message' => \Lang::get('custom.destroy')], 200);
            }
    }
        // Returned error updated messages.
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }

    /**
     * getBrand
     * @param mixed $request
     * @return mixed
     */
    public function getBrand()
    {
        // Get all Brands
        return Brand::select('brands.id')->with(["translation" => function ($query) {
            $query->with('language')->whereHas('language', function ($query) {
                $query->where('code', "en");
            });
        }])->get();
    }
}
