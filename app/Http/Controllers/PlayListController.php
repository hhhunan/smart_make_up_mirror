<?php

namespace App\Http\Controllers;

use App\AddonLib;
use App\Models\PlayList;
use Illuminate\Http\Request;

class PlayListController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // call all PlayList of movies
        $playList = PlayList::with(['movie']);

        $playList = $playList->whereHas('movie', function ($query) use ($request) {
            $query->where('name', 'LIKE', "%" . $request->get('search') . "%");
        })->orderBy('ordering');

        if ($playList->count()) {
            // paginate to array
            $page = $playList->paginate(10)->toArray();
            // marge sort_type in paginate
            $page['sort_type'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            return $page;
        }

        // returned no items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // starting validator
        $validator = \Validator::make($request->all(), [
            'movie_ids' => 'required|array',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->getMessages()], 400);
        }

        // get first ordering or create new ordering
        if ($request->has("id")) {
            $playlistID = $request->get("id");
        } else {
            $playlistID = PlayList::orderBy("id", "DESC")->first();
            if ($playlistID) {
                $playlistID = $playlistID->id + 1;
            } else {
                $playlistID = 1;
            }
        }

        $movie_ids = $request->get("movie_ids");
        // get last play list id
        $playListFirst = PlayList::where("id", $playlistID)->orderBy("ordering", "DESC")->first();
        if (!$playListFirst) {
            $indexOrder = 0;
        } else {
            $indexOrder = $playListFirst->ordering;
        }

        // create PlayList's
        foreach ($movie_ids as $movie) {
            $indexOrder += 1;
            $playlist = new PlayList();
            $playlist->id = $playlistID;
            $playlist->ordering = $indexOrder;
            $playlist->movie_id = $movie;
            if (!$playlist->save()) {
                return response()->json(["message" => \Lang::get('custom.error_save')], 400);
            }
        }

        // returned success created messages
        return response()->json(["message" => \Lang::get('custom.success_creat')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PlayList  $playList
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PlayList::with(['movie', 'sessions'])->orderBy("ordering", "asc")->where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PlayList  $playList
     * @return \Illuminate\Http\Response
     */
    public function edit(PlayList $playList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PlayList  $playList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // starting validator
        $validator = \Validator::make($request->all(), [
            'movie_id' => 'required|numeric',
            'ordering' => 'required|numeric',
            'new_ordering' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->getMessages()], 400);
        }

        $movie_id = $request->get("movie_id");
        $ordering = $request->get("ordering");
        $new_ordering = $request->get("new_ordering");

        // update the new ordering
        PlayList::where("id", $id)->where('ordering', $new_ordering)->update(["ordering" => $ordering]);
        // to update the old ordering to the new ordering
        PlayList::where("id", $id)->where('movie_id', $movie_id)->update(["ordering" => $new_ordering]);

        // returning success update messages
        return response()->json(["message" => \Lang::get('custom.success_updated')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PlayList  $playList
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $playList = PlayList::where("id", $id);
        // deleted playlist by id or by movie id and playlist id
        if ($request->has('movie_id')) {
            $_movie_id = $request->get('movie_id');
            $playList->where('movie_id', $_movie_id)->delete();
            // returned success deleted messages
            return response()->json(['message1' => \Lang::get('custom.destroy')], 200);
        } else {
            $playList->delete();
            // returned success deleted messages
            return response()->json(['message2' => \Lang::get('custom.destroy')], 200);
        }

        // returned error deleted messages
        return response()->json(['message3' => \Lang::get('custom.error_destroy')], 400);
    }

    /**
     * @return array|string
     */
    public function getPlayList(Request $request)
    {

        // get all playlists
        $play_list = PlayList::with('movie');
        $unoccupied = $request->get('unoccupied');
        if ($unoccupied == "true") {
            $play_list->doesntHave('sessions');
        }

        $play_list->groupBy('id');
        if($request->get('page')==='all'){

            return $play_list->get();
        }
        if ($play_list->exists()){
            return $play_list->paginate(10);
    }
        // returned no items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

}
