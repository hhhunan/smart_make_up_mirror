<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\DeviceLabel;
use App\Models\DeviceMakeUpStyle;
use App\Models\Language;
use App\Models\DeviceTranslation;
use App\Models\DeviceConfig;
use App\Models\DeviceProduct;
use App\Models\MakeUpStyle;
use App\Models\Product;
use App\OAuthClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\AddonLib;
use DB;
use App\Models\DeviceUpdate;

class DeviceController extends Controller
{
    public $lang_code;

    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get Language id by LangCode (en/es/ru..)
        $lang_id = Language::where('code', $this->lang_code ? $this->lang_code : "en")->value('id');

        // Get all Devices
        $device = Device::with([
            'device_config',
            'secret',
            'by_live_image',
            'by_session_guide_image',
            'translation' => function ($query) use ($lang_id) {
                $query->with('language')->where('language_id', $lang_id);
            },
            'products' => function ($query) use ($request, $lang_id) {
                $this->addon->translationSearch($query, $lang_id, $request->get('product_search'), "name");
            },
            'styles' => function ($query) use ($request, $lang_id) {
                $this->addon->translationSearch($query, $lang_id, $request->get('product_search'), "name");
            },
        ]);

        // Devices translation options
        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($device, [
                "join" => [
                    "select" => "devices.*",
                    "table" => "device_translations",
                    "first" => "devices.id",
                    "second" => "device_translations.device_id",
                    "type" => "inner"
                ],
                "search" => $this->addon->checkUriParameter($request, 'search') ? [
                    "column" => "device_translations.name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? [
                    "column" => "device_translations.name",
                    "flag" => $request->get('sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }

        // Product translation options
        if ($this->addon->checkUriParameter($request, 'product_sort') || $this->addon->checkUriParameter($request, 'product_search')) {
            $device->join('device_products', 'devices.id', '=', 'device_products.device_id', 'left');
            $this->addon->customSortSearch($device, [
                "join" => [
                    "select" => "devices.*", "table" => "product_translations",
                    "first" => "product_translations.product_id", "second" => "device_products.product_id", "type" => "left"
                ],
                "search" => $this->addon->checkUriParameter($request, 'product_search') ? [
                    "column" => "product_translations.name",
                    "pattern" => $request->get('product_search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'product_sort') ? [
                    "column" => "product_translations.name",
                    "flag" => $request->get('product_sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }
        // Product translation options
        if ($this->addon->checkUriParameter($request, 'style_sort') || $this->addon->checkUriParameter($request, 'style_search')) {
            $device->join('device_make_up_styles', 'devices.id', '=', 'device_make_up_styles.device_id', 'left');
            $this->addon->customSortSearch($device, [
                "join" => [
                    "select" => "devices.*", "table" => "make_up_style_translations",
                    "first" => "make_up_style_translations.make_up_style_id", "second" => "device_make_up_styles.make_up_style_id", "type" => "left"
                ],
                "search" => $this->addon->checkUriParameter($request, 'style_search') ? [
                    "column" => "make_up_style_translations.name",
                    "pattern" => $request->get('style_search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'style_sort') ? [
                    "column" => "make_up_style_translations.name",
                    "flag" => $request->get('style_sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }

        if (!($this->addon->checkUriParameter($request, 'product_sort') || $this->addon->checkUriParameter($request, 'product_search') ||
            $this->addon->checkUriParameter($request, 'style_sort') || $this->addon->checkUriParameter($request, 'style_search') ||
            $this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')))
        {
            $device->orderBy('devices.id', 'desc');
        }

        // Grouping by device id
        $device->groupBy('devices.id');

        // check role
        if ($this->addon->checkRole("administrator")) {
            $device->select("devices.*", "oauth_clients.id AS client_id", "oauth_clients.secret")
                ->join('oauth_clients', 'devices.secret_id', '=', 'oauth_clients.id');
        }
        // Paginations
        $page = $device->paginate(10)->toArray();
        if (sizeof($page['data'])) {

            // Sorting type
            $page['sort_type_device'] =
                $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";

            $page['sort_type_product'] =
                $this->addon->checkUriParameter($request, 'product_sort') ? $request->get('product_sort') : "NORMAL";

            $page['sort_type_style'] =
                $this->addon->checkUriParameter($request, 'style_sort') ? $request->get('style_sort') : "NORMAL";

            // marge labels in devices
            $labels = DeviceLabel::with('language')->get()->groupBy('language.code');
            $page['labels'] = $labels->count() ? $labels : ['message' => \Lang::get('custom.no_item')];
            return $page;
        }

        // Returned, not found items messages.
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $StoreDevice
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function store(Request $request)
    {
        // Device new Object device.
        $device = new Device();

        // Starting validator.
        $validation = Validator::make($request->all(), array_add($device->ruleForCreate, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $device->imei_number = $request->get('imei_number');
        $device->status = $request->get('status');
        $device->type = $request->get('type');

        if($request->has('by_live_image')) {
            $image = $this->addon->fileUploader($request->get("by_live_image"), 'by_live');
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $device->by_live_image_id = $image;
        }

        if($request->has('by_session_image')) {
            $image = $this->addon->fileUploader($request->get("by_session_image"), 'by_session_guide');
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $device->by_session_image_id = $image;
        }

        // CREATE DEVICE SECRET KEY
        $client = new OAuthClient();
        $client->name = "Device Secret Key";
        $client->secret = str_random(40);
        $client->redirect = "/";
        $client->personal_access_client = 0;
        $client->password_client = 1;
        $client->revoked = 0;
        $client->save();
        // END

        $device->secret_id = $client->id;

        // Saved device object
        if (!$device->save()) {
            // Returned created error messages
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // Starting transaction
        DB::transaction(function () use ($device, $request) {
            /*  Device Translations */
            $languages = Language::pluck('code', 'id');
            $translations = [];
            foreach ($languages as $lngId => $lngCode) {
                if (array_key_exists($lngCode, $request->get('name'))) {
                    $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                    $translations[$lngId]['device_id'] = $device->id;
                    $translations[$lngId]['language_id'] = $lngId;
                }
            }
            /*  Devices Product*/
            $device->translationForSync()->sync($translations);

            if ($request->has('product_ids')) {
                $device->products()->sync($request->get('product_ids'));
            }
            if ($request->has('style_ids')) {
                $device->styles()->sync($request->get('style_ids'));
            }
        }, 3);

        // join config id's
        if ($request->has('cfg_ids')) {
            $cfg_ids = $request->get('cfg_ids');
            foreach ($cfg_ids as $value) {
                DeviceConfig::where("id", $value)->update(['device_id' => $device->id]);
            }
        }
        if(!$device->products()->exists() || !$device->styles()->exists())
        {
            $device->status=0;
            $device->save();
        }

        // Returned created success messages
        return response()->json(['message' => \Lang::get('custom.success_creat')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Device db joins
        $device = Device::with([
            'products' => function ($query) {
                (new AddonLib)->translation($query, $this->lang_code);
            },
            'styles' => function ($query) {
                (new AddonLib)->translation($query, $this->lang_code);
            },
            'device_config',
            'secret',
            'by_live_image',
            'by_session_guide_image',
            "translation" => function ($query) {
                if ($this->lang_code)
                    $query->with('language')->whereHas('language', function ($query) {
                        $query->where('code', $this->lang_code);
                    });
                else
                    $query->with('language');
            }
        ]);

        // check role
        if ($this->addon->checkRole("administrator")) {
            $device->select("devices.*", "oauth_clients.id AS client_id", "oauth_clients.secret")
                ->join('oauth_clients', 'devices.secret_id', '=', 'oauth_clients.id');
        }

        // Returned category by id
        return $device->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Starting validator
        $validation = Validator::make($request->all(), array_add((new Device)->ruleForEdit, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $device = Device::where("id", $id)->first();
        $device->status = $request->has('status') ? $request->get('status') : $device->status;
        $device->type = $request->has('type') ? $request->get('type') : $device->status;

        if($request->has('by_live_image')) {
            $image = $this->addon->fileUploader($request->get("by_live_image"), 'by_live', false, $device->by_live_image_id);
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $device->by_live_image_id = $image;
        }

        if($request->has('by_session_image')) {
            $image = $this->addon->fileUploader($request->get("by_session_image"), 'by_session_guide', false, $device->by_session_image_id);
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $device->by_session_image_id = $image;
        }
        // Saved Device object.
        if (!$device->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        if ($request->has('name')) {
            // Starting transaction
            DB::transaction(function () use ($device, $request) {
                /*  Device Translations */
                $languages = Language::pluck('code', 'id');
                $translations = [];
                foreach ($languages as $lngId => $lngCode) {
                    if (array_key_exists($lngCode, $request->get('name'))) {
                        $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                        $translations[$lngId]['device_id'] = $device->id;
                        $translations[$lngId]['language_id'] = $lngId;
                    }
                }

                $device->translationForSync()->sync($translations);
                /*  Devices Product */
                if ($request->has('product_ids')) {
                    $device->products()->sync($request->get('product_ids'));
                }
                if ($request->has('style_ids')) {
                    $device->styles()->sync($request->get('style_ids'));
                }
            }, 3);


        }

        // change device config id
        if ($request->has('cfg_ids')) {
            $cfg_ids = $request->get('cfg_ids');
            foreach ($cfg_ids as $value) {
                DeviceConfig::where("id", $value)->update(['device_id' => $device->id]);
            }
        }

        if(!$device->products()->exists() || !$device->styles()->exists())
        {
            $device->status=0;
            $device->save();
        }

        // Returned success updated messages.
        return response()->json(['messages' => \Lang::get('custom.success_updated')], 200);
    }

    public function GenerateSecretKey($client_id)
    {
        // CREATE DEVICE SECRET KEY
        $client = OAuthClient::findOrFail($client_id);
        $client->secret = str_random(40);
        if (!$client->save()) {
            return response()->json(["message" => \Lang::get('custom.error_save')], 400);
        }
        // END
        return response()->json([
            "message" => \Lang::get('custom.success_updated'),
            "secret" => $client->secret, "client_id" => $client->id
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id - device id
     * @param  int  $pid - product id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $pid = 0)
    {
        // Call device model and get this query.
        $device = Device::where('id', $id);

        // Device exists item.
        if (!$device->exists()) {
            return response()->json(['message' => \Lang::get('custom.record_not_found', ['id' => $id, 'model' => 'Category'])], 400);
        }

        // Starting destroy
        if ($pid == 0) {
            DB::beginTransaction();
            try {
                DB::transaction(function () use ($id) {
                    DB::table('device_configs')->where("device_id", $id)->update(['device_id' => null]);
                    DB::table('device_products')->where("device_id", $id)->delete();
                    DB::table('device_make_up_styles')->where("device_id", $id)->delete();
                    DB::table('device_translations')->where("device_id", $id)->delete();
                    DB::table('device_updates')->where("device_id", $id)->delete();
                    DB::table('devices')->where("id", $id)->delete();
                });
                DB::commit();
            } catch (\Exception $ex) {
                DB::rollBack();
                // Returned error updated messages.
                return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
            }
        } else {
            if (!DeviceProduct::where('product_id', $pid)->where('device_id', $id)->delete()) {
                // Returned error updated messages.
                return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
            }
        }

        // Returning success destroy messages.
        return response()->json(['message' => \Lang::get('custom.destroy')], 200);
    }

    /**
     * getStatus
     * @param mixed $request
     * @param mixed $id
     * @return mixed
     */
    public function getStatus($id)
    {
        // Call device model and get this query.
        $device = Device::where('id', $id);

        // Device check exists item.
        if (!$device->exists()) {
            return response()->json(['message' => \Lang::get('custom.record_not_found', ['id' => $id, 'model' => 'Device'])], 400);
        }

        // Get device by status
        $device = Device::where("id", $id)->get();

        // Returning status
        return response()->json(['status' => $device[0]["status"]], 200);
    }

    /**
     * setStatus
     * @param mixed $request
     * @param mixed $id
     * @return mixed
     */
    public function setStatus(Request $request, $id)
    {
        // Call device model and get this query.
        $device = Device::where('id', $id);

        // Device check exists item.
        if (!$device->exists()) {
            return response()->json(['message' => \Lang::get('custom.record_not_found', ['id' => $id, 'model' => 'Device'])], 400);
        }

        // Starting validator
        $validation = Validator::make($request->all(), array(
            "status" => "numeric",
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        // Starting status update.
        $current_date = date('Y-m-d h:i:s', strtotime(" +1 minutes "));
        $device->update(['status' => $request->get('status'), 'modify_status_date' => $current_date]);

        // Returning status
        return response()->json(['status' => $request->get('status')], 200);
    }

    /**
     * getProduct
     * @param mixed $request
     * @return mixed
     */
    public function getProduct()
    {
        // Returning all Product
        return Product::select('products.id')->with([
            "translation" => function ($query) {
                $query->with('language')->whereHas('language', function ($query) {
                    $query->where('code', "en");
                });
            }
        ])->get();
    }
    public function getStyle()
    {
        // Returning all Product
        return MakeUpStyle::select('make_up_styles.id')->with([
            "translation" => function ($query) {
                $query->with('language')->whereHas('language', function ($query) {
                    $query->where('code', "en");
                });
            }
        ])->get();
    }

    /**
     * addProducts
     * @param mixed $request
     * @param mixed $id
     * @return mixed
     */
    public function addProducts(Request $request, $id)
    {
        // Starting validator
        $validation = Validator::make($request->all(), array(
            "product_ids" => "array",
            "product_ids.*" => "numeric",
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        $device= Device::where("id", $id)->first();
        // device Devices
        if ($request->has('product_ids')) {
            $device->products()->attach($request->get('product_ids'));
            $active = null;

            if (!$device->styles()->exists()){
                $active = 0;
            }
            return response()->json(['message' => \Lang::get('custom.success_creat'), 'active' => $active], 200);

        }
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);

    }
    public function addStyle(Request $request, $id)
    {
        // Starting validator
        $validation = Validator::make($request->all(), array(
            "style_ids" => "array",
            "style_ids.*" => "numeric",
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        // device Devices
        $device= Device::where("id", $id)->first();
        if ($request->has('style_ids')) {
            $device->styles()->attach($request->get('style_ids'));
            $active = null;
            if (!$device->products()->exists()){
                $active = 0;
            }
            return response()->json(['message' => \Lang::get('custom.success_creat'), 'active' => $active], 200);

        }
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * getDevice
     * @param mixed $request
     * @return mixed
     */
    public function getDevice()
    {
        // Returning all devices name
        return Device::select('devices.id')->with([
            "translation" => function ($query) {
                $query->with('language')->whereHas('language', function ($query) {
                    $query->where('code', "en");
                });
            }
        ])->get();
    }

    // Android

    /**
     * getDevicesAndUpdates
     * @param mixed $request
     * @return mixed
     */


    /**
     * editDeviceUpdate
     * @param mixed $request
     * @return mixed
     */
    public function editDeviceUpdate(Request $request)
    {
        // Starting validator.
        $validation = Validator::make($request->all(), array(
            "hash" => "required",
            "status" => "numeric",
            "message" => "min:2|max:255",
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $hash = $request->get("hash");// Get first device update
        $deviceUpdate = DeviceUpdate::where("hash", $hash)->first();
        if (!$deviceUpdate) {
            return response()->json(['message' => \Lang::get('custom.no_item')], 200);
        }

        $deviceUpdate->update_date_status = date('Y-m-d H:i:s', time());

        if ($request->has("status"))
            $deviceUpdate->status = $request->get("status");
        if ($request->has("message"))
            $deviceUpdate->message = $request->get("message");

        // saved device object
        if (!$deviceUpdate->save()) {
            return response()->json(["message" => \Lang::get('custom.error_save')], 400);
        }

        // Returned success updated messages
        return response()->json(["message" => \Lang::get('custom.success_updated')], 200);
    }

    /**
     * destroyDeviceUpdate
     * @param mixed $request
     * @return mixed
     */
    public function destroyDeviceUpdate(Request $request)
    {
        // Starting validator.
        $validation = Validator::make($request->all(), array(
            "imei_number" => "min:10",
            "hash" => "min:64|max:64"
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        // Starting Device Update destroys
        if ($request->has("imei_number")) {
            $device_id = Device::where("imei_number", $request->get("imei_number"))->value('id');
            if (DeviceUpdate::where("device_id", $device_id)->delete()) {
                return response()->json(["message" => \Lang::get('custom.destroy')], 200);
            }
        } else if ($request->has("hash")) {
            if (DeviceUpdate::where("hash", $request->get("hash"))->delete()) {
                return response()->json(["message" => \Lang::get('custom.destroy')], 200);
            }
        }

        // Returned error destroy messages
        return response()->json(["message" => \Lang::get('custom.error_destroy')], 400);
    }

    /**
     * getDeviceUpdate
     * @param mixed $request
     * @return mixed
     */
    public function getDeviceUpdate()
    {
        // Get device by ID or All
        $devices = Device::with(["translation" => function ($query) {
            if ($this->lang_code) {
                $query->with('language')->whereHas('language', function ($query) {
                    $query->where('code', $this->lang_code);
                });
            } else
                $query->with('language');
        }, 'device_updates']);
        if ($devices->exists()) {
            return $devices->paginate(10);
        }

        // Returned not items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * searchByImaiNumberByHash
     * @param mixed $request
     * @return mixed
     */
    public function searchByImaiNumberByHash(Request $request)
    {
        if ($request->has("imei_number") || $request->has("hash")) {
            // Starting validator.
            $validation = Validator::make($request->all(), [
                "imei_number" => "alpha_dash|min:10",
                "hash" => "alpha_dash|min:64|max:64"
            ]);
            if ($validation->fails()) {
                return response()->json(['message' => $validation->errors()->getMessages()], 400);
            }

            // request device update hash
            $hash = ($request->has("hash") && strlen(trim($request->get("hash"))) == 64) ? $request->get("hash") : '';
            // request device imei number
            $imeiNumber = ($request->has("imei_number") && strlen(trim($request->get("imei_number")) >= 10)) ? $request->get("imei_number") : '';
            // Get all devices
            $devices = Device::with([
                "translation" => function ($query) {
                    if ($this->lang_code) {

                        $query->with('language')->whereHas('language', function ($query) {
                            $query->where('code', $this->lang_code);
                        });
                    } else
                        $query->with('language');
                }
            ]);
            if ($imeiNumber && $hash) {

                $devices->whereHas('device_updates', function ($query) use ($hash) {
                    $query->where('hash', $hash);

                })->with(['device_updates'=>function($query)use ($hash){
                    $query->where('hash', $hash);

                }])->where('imei_number', $imeiNumber);
                if ($devices->exists()) {
                    return $devices->first();
                }
            } elseif ($hash) {

                $devices->whereHas('device_updates', function ($query) use ($hash) {
                    $query->where('hash', $hash);

                })->with(['device_updates'=>function($query)use ($hash){
                    $query->where('hash', $hash);

                }]);
                if ($devices->exists()) {
                    return $devices->first();
                }
            } elseif ($imeiNumber) {
                $devices->where('imei_number', $imeiNumber);
                if ($devices->with('device_updates')->exists()) {
                    return $devices->first();
                }
            }

            // Returned not items messages
            return response()->json(['message' => \Lang::get('custom.no_item')], 200);
        }
    }

//
    public function destroy_device_style($id, $pid)
    {
        if (!DeviceMakeUpStyle::where('make_up_style_id', $pid)->where('device_id', $id)->exists()) {
            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
        }
        $DeviceStyle = Device::where('id', $id)->first();
        $DeviceStyle->styles()->detach($pid);
        $active = null;
        if (!$DeviceStyle->styles()->exists()) {
            $DeviceStyle->status = 0;
            $active = 0;
            $DeviceStyle->save();
        }
        elseif (!$DeviceStyle->products()->exists()){
            $active = 0;
        }
        return response()->json(['message' => \Lang::get('custom.destroy'), 'active' => $active], 200);

    }
//        if (!DeviceMakeUpStyle::where('make_up_style_id', $pid)->where('device_id', $id)->exists())
//            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists
//
//        if (DeviceMakeUpStyle::where('make_up_style_id', $pid)->where('device_id', $id)->delete()) {
//            return response()->json(['message' => \Lang::get('custom.destroy')], 200);
//        }
//        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);


    public function destroy_device_product($id,$pid)
    {
//        $device = Device::where("id", $id)->first();
//        $device->styles()->sync($request->get('style_ids'));
//        $active=null;
//        if(!$device->styles()->exists())
//        {
//            $device->status=0;
//            $device->save();
//            $active=0;
//            return response()->json(['message' => \Lang::get('custom.destroy'), 'active'=>$active], 200);
//        }
//
//        return response()->json(['message' => \Lang::get('custom.success_creat'), 'active'=>$active], 200);
        if (!DeviceProduct::where('product_id', $pid)->where('device_id', $id)->exists()) {
            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists
        }
        $brandProduct = Device::where('id', $id)->first();
        $brandProduct->products()->detach($pid);
        $active = null;
        if (!$brandProduct->products()->exists()) {
            $brandProduct->status = 0;
            $active = 0;
            $brandProduct->save();
        }
        elseif (!$brandProduct->styles()->exists()){
            $active = 0;
        }

        return response()->json(['message' => \Lang::get('custom.destroy'), 'active' => $active], 200);
//        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists
//      {}
    }
    public function analytic()
    {
        $data['created'] = DB::table('devices')
            ->select(DB::raw('count(*) as created_count, MONTH(created_at) month, YEAR(created_at) YEAR'))
            ->groupBy('month')
            ->get();
        $data['device_updated'] = DB::table('device_updates')
            ->select(DB::raw('count(*) as updated_count, MONTH(modify_update_date) month, YEAR(modify_update_date) YEAR'),'device_id' )
            ->LeftJoin('devices','device_updates.device_id','=','devices.id')
            ->groupBy('month','device_id')
            ->get()->groupBy('device_id')->values();

        return $data;

    }
}
