<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Language;

use App\Models\ProductTranslation;
use App\Models\BrandTranslation;
use App\Models\DeviceTranslation;
use App\Models\SessionTranslation;
use App\Models\CategoryTranslation;

use App\Models\Product;
use App\Models\Brand;
use App\Models\Device;
use App\Models\Session;
use App\Models\Category;
use App\AddonLib;


class LanguageController extends Controller
{
    public $lang_code;
    public $addon;
    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }
    public function check_translation($lang_id, $lang_name)
    {
        $brand_translation_count = BrandTranslation::where('language_id', $lang_id)->count();
        $brand_count = Brand::all()->count();
        if ($brand_translation_count !== $brand_count) {
            return ['return' => response()->json(['message' => \Lang::get('custom.not_translation', ['language' => $lang_name, 'model' => "brand"])], 400)];
        }

        $brand_translation_count = DeviceTranslation::where('language_id', $lang_id)->count();
        $device_count = Device::all()->count();
        if ($brand_translation_count !== $device_count) {
            return ['return' => response()->json(['message' => \Lang::get('custom.not_translation', ['language' => $lang_name, 'model' => "device"])], 400)];
        }

        $product_translation_count = ProductTranslation::where('language_id', $lang_id)->count();
        $product_count = Product::all()->count();
        if ($product_translation_count !== $product_count) {
            return ['return' => response()->json(['message' => \Lang::get('custom.not_translation', ['language' => $lang_name, 'model' => "product"])], 400)];
        }

        $category_translation_count = CategoryTranslation::where('language_id', $lang_id)->count();
        $category_count = Category::all()->count();
        if ($category_translation_count !== $category_count) {
            return ['return' => response()->json(['message' => \Lang::get('custom.not_translation', ['language' => $lang_name, 'model' => "category"])], 400)];
        }

        $session_translation_count = SessionTranslation::where('language_id', $lang_id)->count();
        $session_count = Session::all()->count();
        if ($session_translation_count !== $session_count) {
            return ['return' => response()->json(['message' => \Lang::get('custom.not_translation', ['language' => $lang_name, 'model' => "session"])], 400)];
        }
        return ['return' => false];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all languages
        $lang = Language::with('image');
        if ($lang) {
            return $lang->get();
        }

        // Returned not items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create new language object
        $lang = new Language;

        // Starting validator.
        $validator = Validator::make($request->all(), $lang->ruleForCreate);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->getMessages()], 400);
        }

        $lang->name = $request->get("name");
        $lang->code = $request->get("code");
        $lang->status = 0;        // Change default language
        $lang->default = 0;

        if($request->has('image')){
            $image = $this->addon->fileUploader($request->get('image'), 'flag');
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $lang->image_id = $image;
        }
        // saved language object.
        if ($lang->save()) {
            return response()->json(['message' => \Lang::get('custom.success_creat')], 200);
        }

        // returned error created messages.
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Call lang model and get this query.
        $lang = Language::where('id', $id);

        // Checking exists item.
        if (!$lang->exists()) {
            return response()->json(['message' => \Lang::get('custom.record_not_found', ['id' => $id, 'model' => 'Language'])], 400);
        }

        // returned all lang
        return $lang->with('image')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Starting validator
        $validator = Validator::make($request->all(), (new Language)->ruleForEdit);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->getMessages()], 400);
        }
        $lang = Language::where("id", $id)->first();
        $lang->code = $request->has('code') ? $request->get('code') : $lang->code;
        $lang->name = $request->has('name') ? $request->get('name') : $lang->name;

        // Change status language
        if ($request->has('status')) {
            if ($request->get('status') == 0 && $lang->default == 1) {
                $lang->default = 0;
                $lang->status = 0;
                $languages = Language::where('id', '!=', $id)->where('status', 1);
                if ($languages->exists()) {
                    $languages = $languages->first();
                    $languages->default = 1;
                    $languages->save();
                } else {
                    return response()->json(['message' => \Lang::get('custom.language_not_default')], 400);
                }
            } else {
                $lang->status = $request->get('status');
            }
        }

        if ($request->has("default")) {
            if ($request->get("default") == 1) {
                $status = $this->check_translation($lang->id, $lang->name);
                if ($status['return'] !== false) {
                    return $status['return'];
                }
                Language::where('default', 1)->update(['default' => 0]);
                $lang->default = 1;
                $lang->status = 1;
            } else {
                $languages = Language::where('id', '!=', $id)->where('status', 1);
                if (!$languages->where('default', 1)->exists()) {
                    if ($languages->where('default', 0)->exists())
                    {
                        $languages = $languages->first();
                        $languages->default = 1;
                        $languages->save();
                        $lang->default = 0;
                    } else {
                        return response()->json(['message' => \Lang::get('custom.language_not_default')], 400);
                    }
                }
            }
        }

        // Checks the absence of this language.
        if ($request->has('status') && $lang->status === 1) {
            $status = $this->check_translation($lang->id, $lang->name);
            if ($status['return'] !== false) {
                return $status['return'];
            }
        }

        if($request->has('image')) {
            $image = $this->addon->fileUploader($request->get('image'),'flag', true, $lang->image_id);
            if (isset($image['message']))
            {
                return response()->json($image['message'], 400);
            }
            $lang->image_id = $image;
        }
        // saved language object.
        if ($lang->save()) {
            return response()->json(['message' => \Lang::get('custom.success_updated')], 200);
        }

        // returned error created messages.
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Call lang model and get this query.
        $lang = Language::where('id', $id);

        // Checking exists item.
        if (!$lang->exists()) {
            return response()->json(['message' => \Lang::get('custom.record_not_found', ['id' => $id, 'model' => 'Language'])], 400);
        }

        // Checking is default language?
        if (Language::where('id', $id)->where('default', '1')->exists()) {
            return response()->json(['message' => \Lang::get('custom.error_default')], 400);
        }

        // starting destroy
        if (Language::destroy($id)) {
            return response()->json(['message' => \Lang::get('custom.destroy')], 200);
        }

        // returned error deleted messages
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }
}
