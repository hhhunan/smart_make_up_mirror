<?php

namespace App\Http\Controllers;

use App\Models\DeviceMakeUpStyle;
use App\Models\DeviceProduct;
use App\Models\Product;
use App\Models\Language;
use App\Models\ProductTranslation;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\AddonLib;
use App\Models\ProductSession;
use DB;
use App\Models\Device;

class ProductController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get Language id by lang code
        $lang_id = Language::where('code', $this->lang_code ? $this->lang_code : "en")->value('id');

        // call add product
        $product =
            Product::with([
                'images',
                "translation" => function ($query) use ($lang_id) {
                    $query->with('language')->where('language_id', $lang_id);
                },
                // brands in product
                'brands' => function ($query) use ($request, $lang_id) {
                    $query->with('images');
                    $this->addon->translationSearch($query, $lang_id, $request->get('brand_search'), "name");
                },
                // devices in product
                'devices' => function ($query) use ($request, $lang_id) {
                    $this->addon->translationSearch($query, $lang_id, $request->get('device_search'), "name");
                },
                // category in product
                'category' => function ($query) use ($request, $lang_id) {
                    $this->addon->translationSearch($query, $lang_id, $request->get('category_search'), "name");
                },
                'sessions' => function ($query) use ($request, $lang_id) {
                    $this->addon->translationSearch($query, $lang_id, $request->get('session_search'), "name");

                }
            ]);

        // Products options
        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($product, [
                "join" => ["select" => "products.*", "table" => "product_translations",
                    "first" => "products.id", "second" => "product_translations.product_id", "type" => "inner"
                ],
                "search" => $this->addon->checkUriParameter($request, 'search') ? ["column" => "product_translations.name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? ["column" => "product_translations.name",
                    "flag" => $request->get('sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }

        // Brands options
        if ($this->addon->checkUriParameter($request, 'brand_search') || $this->addon->checkUriParameter($request, 'brand_sort')) {
            $product->join('product_brands', 'products.id', '=', 'product_brands.product_id', 'left');
            $this->addon->customSortSearch($product, [
                "join" => ["select" => "products.*", "table" => "brand_translations",
                    "first" => "brand_translations.brand_id", "second" => "product_brands.brand_id", "type" => "left"
                ],
                "search" => $this->addon->checkUriParameter($request, 'brand_search') ? ["column" => "brand_translations.name",
                    "pattern" => $request->get('brand_search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'brand_sort') ? ["column" => "brand_translations.name",
                    "flag" => $request->get('brand_sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }

        // Devices options
        if ($this->addon->checkUriParameter($request, 'device_search') || $this->addon->checkUriParameter($request, 'device_sort')) {
            $product->join('device_products', 'products.id', '=', 'device_products.product_id', 'left');
            $this->addon->customSortSearch($product, [
                "join" => ["select" => "products.*", "table" => "device_translations",
                    "first" => "device_translations.device_id", "second" => "device_products.device_id", "type" => "left"
                ],
                "search" => $this->addon->checkUriParameter($request, 'device_search') ? ["column" => "device_translations.name",
                    "pattern" => $request->get('device_search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'device_sort') ? ["column" => "device_translations.name",
                    "flag" => $request->get('device_sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }
        // Session options
        if ($this->addon->checkUriParameter($request, 'session_search') || $this->addon->checkUriParameter($request, 'session_sort')) {
            $product->join('product_sessions', 'products.id', '=', 'product_sessions.product_id', 'left');
            $this->addon->customSortSearch($product, [
                "join" => ["select" => "products.*", "table" => "session_translations",
                    "first" => "session_translations.session_id", "second" => "product_sessions.session_id", "type" => "left"
                ],
                "search" => $this->addon->checkUriParameter($request, 'session_search') ? ["column" => "session_translations.name",
                    "pattern" => $request->get('session_search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'session_sort') ? ["column" => "session_translations.name",
                    "flag" => $request->get('session_sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }


        if (!($this->addon->checkUriParameter($request, 'category_search') || $this->addon->checkUriParameter($request, 'device_search') ||
            $this->addon->checkUriParameter($request, 'session_search') || $this->addon->checkUriParameter($request, 'brand_search') || $this->addon->checkUriParameter($request, 'search'))) {
            $product->orderBy('id', 'desc');
        }

        $product->groupBy('products.id');


        // paginate to Array
        $page = $product->paginate(10)->toArray();
        if (sizeof($page['data'])) {

            // marge sort type in paginate
            $page['sort_type_product'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            $page['sort_type_category'] = $this->addon->checkUriParameter($request, 'category_sort') ? $request->get('category_sort') : "NORMAL";
            $page['sort_type_brand'] = $this->addon->checkUriParameter($request, 'brand_sort') ? $request->get('brand_sort') : "NORMAL";
            $page['sort_type_device'] = $this->addon->checkUriParameter($request, 'device_sort') ? $request->get('device_sort') : "NORMAL";
            $page['sort_type_session'] = $this->addon->checkUriParameter($request, 'session_sort') ? $request->get('session_sort') : "NORMAL";

            return $page;
        }

        // returned no items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create new Product object
        $product = new Product();

        // starting validation
        $validation = Validator::make($request->all(), array_add($product->ruleForCreate, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $product->status = $request->has('status') ? $request->get('status') : 0;
        if ($request->has('image')) {
            $image = $this->addon->fileUploader($request->get('image'), 'product');
            if (isset($image['message'])) {
                return response()->json($image['message'], 400);
            }
            $product->image_id = $image;
        }
        // saved Product object
        if (!$product->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // starting translations
        DB::transaction(function () use ($product, $request) {
            $languages = Language::pluck('code', 'id');
            $translations = [];
            foreach ($languages as $lngId => $lngCode) {
                if (array_key_exists($lngCode, $request->get('name')) && array_key_exists($lngCode, $request->get('description'))) {
                    $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                    $translations[$lngId]['description'] = $request->get('description')[$lngCode];
                    $translations[$lngId]['product_id'] = $product->id;
                    $translations[$lngId]['language_id'] = $lngId;
                }
            }
            $product->translationForSync()->sync($translations);

            /*  Product Sessions  */
            if ($request->has('session_ids')) {
                $product->sessions()->sync($request->get('session_ids'));
            }
            /*  Product Brands  */
            if ($request->has('brand_ids')) {
                $product->brands()->sync($request->get('brand_ids'));
            }

            /*  Product Categories  */
            if ($request->has('category_ids')) {
                $product->category()->sync($request->get('category_ids'));
            }
        }, 3);
        if (!$product->category()->exists() || !$product->brands()->exists() || !$product->sessions()->exists()) {
            $product->status = 0;
            $product->save();
        }

        // returned successfully messages
        return response()->json(["messages" => \Lang::get('custom.success_creat')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->with([
            'devices' => function ($query) {
                (new AddonLib)->translation($query, $this->lang_code);
            },
            'sessions' => function ($query) {
                (new AddonLib)->translation($query, $this->lang_code);
                $query->with(['play_list' => function ($query) {
                    $query->with(['movie']);
                }]);
            },
            'brands' => function ($query) {
                $query->with('images');
                (new AddonLib)->translation($query, $this->lang_code);
            },
            'category' => function ($query) {
                (new AddonLib)->translation($query, $this->lang_code);
            },
            'images',
            "translation" => function ($query) {
                if ($this->lang_code)
                    $query->with('language')->whereHas('language', function ($query) {
                        $query->where('code', $this->lang_code);
                    });
                else
                    $query->with('language');
            }
        ]);

        if ($product->exists()) {
            return $product->first();
        }

        // returned no items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        // starting validation
        $validation = Validator::make($request->all(), array_add((new Product)->ruleForEdit, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $product = Product::where("id", $id)->first();
        $product->status = $request->has('status') ? $request->get('status') : $product->status;

        if ($request->has('image')) {
            $image = $this->addon->fileUploader($request->get('image'), 'product', true, $product->image_id);
            if (isset($image['message'])) {
                return response()->json($image['message'], 400);
            }
            $product->image_id = $image;
        }

        if (!$product->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // starting translations
        if ($request->get('name') && $request->get('description')) {
            DB::transaction(function () use ($product, $request) {

                $languages = Language::pluck('code', 'id');
                $translations = [];
                foreach ($languages as $lngId => $lngCode) {
                    if (array_key_exists($lngCode, $request->get('name')) && array_key_exists($lngCode, $request->get('description'))) {
                        $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                        $translations[$lngId]['description'] = $request->get('description')[$lngCode];
                        $translations[$lngId]['product_id'] = $product->id;
                        $translations[$lngId]['language_id'] = $lngId;
                    }
                }
                $product->translationForSync()->sync($translations);


//                if (!$request->has('session_ids') || !$request->has('brand_ids') ||!$request->get('category_ids') ) {
//                    if($request->get('session_ids').isEmpty() || $request->get('brand_ids').isEmpty() ||$request->get('category_ids').isEmpty() ){
//                        $product->status=0;
//                    }
//                }
                /*  Product Sessions  */
                if ($request->has('session_ids')) {
                    $product->sessions()->sync($request->get('session_ids'));
                }
                /*  Product Brands  */
                if ($request->has('brand_ids')) {
                    $product->brands()->sync($request->get('brand_ids'));
                }
                /*  Product Categories  */
                if ($request->has('category_ids')) {
                    $product->category()->sync($request->get('category_ids'));
                }
            }, 3);
        }
        if (!$product->category()->exists() || !$product->brands()->exists() || !$product->sessions()->exists()) {
            $product->status = 0;
            $product->save();
        }
        return response()->json(["messages" => \Lang::get('custom.success_updated')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrfail($id);
        if (isset($product->images->uri)) {
            $path = $product->images->uri;
            if (file_exists(public_path($path))) {
                unlink(public_path($path));
            }
        }
        $device_id = DeviceProduct::where('product_id', $id)->value('device_id');
        DeviceProduct::where('product_id', $id)->delete();

        if ($product->delete()) {
            if (!DeviceProduct::where('device_id', $device_id)->count())
                Device::where("id", $device_id)->update(['status' => '0']);
            // Returning success destroy messages.
            return response()->json(['message' => \Lang::get('custom.destroy')], 200);
        }
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }

    /**
     * addDevices
     * @param mixed $request
     * @param mixed $id
     * @return mixed
     */
    public function addDevices(Request $request, $id)
    {
        $validation = Validator::make($request->all(), array(
            "device_ids" => "array",
            "device_ids.*" => "numeric",
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        if ($request->has('device_ids')) {
            $device_ids = $request->get('device_ids');
            foreach ($device_ids as $index => $device_id) {
                if (!DeviceProduct::where("device_id", $device_id)->where("product_id", $id)->first()) {
                    $devProduct = new DeviceProduct;
                    $devProduct->device_id = $device_id;
                    $devProduct->product_id = $id;
                    $devProduct->save();
                }
            }
            return response()->json(['message' => \Lang::get('custom.success_creat')], 200);
        }
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * addBrands
     * @param mixed $request
     * @param mixed $id
     * @return mixed
     */
    public function addBrands(Request $request, $id)
    {
        $validation = Validator::make($request->all(), array(
            "brand_ids" => "array",
            "brand_ids.*" => "numeric",
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        $brandProduct = Product::where("id", $id)->first();
        // device Devices
        if ($request->has('brand_ids')) {
            $brandProduct->brands()->attach($request->get('brand_ids'));

            $active = null;

            if (!$brandProduct->sessions()->exists() || !$brandProduct->category()->exists()) {
                $active = 0;
            }
            return response()->json(['message' => \Lang::get('custom.success_creat'), 'active' => $active], 200);

        }
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * addCategory
     * @param mixed $request
     * @param mixed $id
     * @return mixed
     */
    public function addCategory(Request $request, $id)
    {
        $validation = Validator::make($request->all(), array(
            "category_ids" => "array",
            "category_ids.*" => "numeric",
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        $CategoryProduct = Product::where("id", $id)->first();
        // device Devices
        if ($request->has('category_ids')) {
            $CategoryProduct->category()->attach($request->get('category_ids'));
            $active = null;

            if (!$CategoryProduct->sessions()->exists() || !$CategoryProduct->brands()->exists()) {
                $active = 0;
            }
            return response()->json(['message' => \Lang::get('custom.success_creat'), 'active' => $active], 200);

        }
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * addSession
     * @param mixed $request
     * @param mixed $id
     * @return mixed
     */
    public function addSession(Request $request, $id)
    {
        $validation = Validator::make($request->all(), array(
            "session_ids" => "array",
            "session_ids.*" => "numeric",
        ));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        $SessionProduct = Product::where("id", $id)->first();
        // device Devices
        if ($request->has('session_ids')) {
            $SessionProduct->sessions()->attach($request->get('session_ids'));
            $active = null;

            if (!$SessionProduct->category()->exists() || !$SessionProduct->brands()->exists()) {
                $active = 0;
            }
            return response()->json(['message' => \Lang::get('custom.success_creat'), 'active' => $active], 200);

        }
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * destroy_device
     * @param mixed $request
     * @param mixed $id
     * @param mixed $pid
     * @return mixed
     */
    public function destroy_device($id, $pid)
    {
        $Product = Device::where('id', $id)->first();
        if (!DeviceProduct::where('product_id', $pid)->where('device_id', $id)->exists()) {
            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists
        }
        $Product->products()->detach($pid);
        $active = null;
        if (!$Product->products()->exists()) {
            $Product->status = 0;

            $Product->save();
            $active = 0;
            return response()->json(['message' => \Lang::get('custom.destroy'), 'active' => $active], 200);
        }
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);

    }


//    public function destroy_device_style($id, $pid)
//    {
//        if (!DeviceMakeUpStyle::where('make_up_style_id', $pid)->where('device_id', $id)->exists())
//            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists
//
//        if (DeviceMakeUpStyle::where('make_up_style_id', $pid)->where('device_id', $id)->delete()) {
//            return response()->json(['message' => \Lang::get('custom.destroy')], 200);
//        }
//        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
//    }

    /**
     * destroy_brand
     * @param mixed $request
     * @param mixed $id
     * @param mixed $pid
     * @return mixed
     */
    public function destroy_brand($id, $pid)
    {
        if (!ProductBrand::where('brand_id', $id)->where('product_id', $pid)->exists()) {
            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists
        }
        $brandProduct = Product::where('id', $pid)->first();
        $brandProduct->brands()->detach($id);
        $active = null;
        if (!$brandProduct->brands()->exists()) {
            $brandProduct->status = 0;
            $active = 0;
            $brandProduct->save();
        } elseif (!$brandProduct->sessions()->exists() || !$brandProduct->category()->exists()) {
            $active = 0;
        }

        return response()->json(['message' => \Lang::get('custom.destroy'), 'active' => $active], 200);
//        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists

    }

    /**
     * destroy_session
     * @param mixed $request
     * @param mixed $id
     * @param mixed $pid
     * @return mixed
     */
    public function destroy_session($id, $pid)
    {
        if (!ProductSession::where('session_id', $id)->where('product_id', $pid)->exists()) {
            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists
        }
        $SessionProduct = Product::where('id', $pid)->first();
        $SessionProduct->sessions()->detach($id);
        $active = null;
        if (!$SessionProduct->sessions()->exists()) {
            $SessionProduct->status = 0;
            $active = 0;
            $SessionProduct->save();
        } elseif (!$SessionProduct->category()->exists() || !$SessionProduct->brands()->exists()) {
            $active = 0;
        }

        return response()->json(['message' => \Lang::get('custom.destroy'), 'active' => $active], 200);
    }

    /**
     * destroy_category
     * @param mixed $request
     * @param mixed $id
     * @param mixed $pid
     * @return mixed
     */
    public function destroy_category($id, $pid)
    {
        if (!ProductCategory::where('category_id', $id)->where('product_id', $pid)->exists()) {
            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400); //brand id doesn`t exists
        }
        $CategoryProduct = Product::where('id', $pid)->first();
        $CategoryProduct->category()->detach($id);
        $active = null;
        if (!$CategoryProduct->category()->exists()) {
            $CategoryProduct->status = 0;
            $active = 0;
            $CategoryProduct->save();
        } elseif (!$CategoryProduct->sessions()->exists() || !$CategoryProduct->brands()->exists()) {
            $active = 0;
        }

        return response()->json(['message' => \Lang::get('custom.destroy'), 'active' => $active], 200);
    }

    public function analytic()
    {
        $data['created'] = DB::table('products')
            ->select(DB::raw('count(*) as created_count, MONTH(created_at) month, YEAR(created_at) YEAR'))
            ->groupBy('month')
            ->get();
        return $data;

    }


}
