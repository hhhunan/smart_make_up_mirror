<?php

namespace App\Http\Controllers;

use App\Models\DeviceConfig;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class DeviceConfigController extends Controller
{
    public $lang_code;
    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get configs
        $cfg = DeviceConfig::all();
        if ($cfg) {
            // Returned all configs group by. 
            return $cfg->groupBy('group');
        }
        
        // Returned, not found items messages.
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Create new Object config.
        $device_cfg = new DeviceConfig();
        
        // Starting validator.
        $validation = Validator::make($request->all(), array_add($device_cfg->ruleForCreate, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $device_cfg->group = $request->get('group');
        $device_cfg->type = $request->get('type');
        $device_cfg->value = $request->get('value');
        $device_cfg->device_id = $request->get('device_id');
        $device_cfg->status = $request->has('status') ? $request->get('status') : 1;

        // Saved config object
        if (!$device_cfg->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        // Returned created success messages
        return response()->json(['message' => \Lang::get('custom.success_creat')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Returned category by id
        return  DeviceConfig::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Starting validator
        $validation = Validator::make($request->all(), array_add((new DeviceConfig)->ruleForEdit, 'status', Rule::in([0, 1])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        $device_cfg=DeviceConfig::where("id", $id)->first();

        $device_cfg->group = $request->get('group');
        $device_cfg->type = $request->get('type');
        $device_cfg->value = $request->get('value');
        $device_cfg->device_id = $request->has('device_id') ? $request->get('device_id') : $device_cfg->device_id;
        $device_cfg->status = $request->has('status') ? $request->get('status') : $device_cfg->status;
        
        // Saved Config object.
        if ($device_cfg->save()) {
            return response()->json(['message' => \Lang::get('custom.success_updated')], 200);
        }
        
        // Returned success updated messages.
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!DeviceConfig::destroy($id)) {
            // Returned error updated messages.
            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
        }

        // Returning success destroy messages.
        return response()->json(['message' => \Lang::get('custom.destroy')], 200);
    }

    /**
     * Get all Groups
     *
     * @return \Illuminate\Http\Response
     */
    public function getGroup()
    {
        // Get all groups
        return DeviceConfig::select("device_configs.group")->groupBy('group')->get();
    }

    /**
     * Returning all Types
     *
     * @return \Illuminate\Http\Response
     */
    public function getType()
    {
        // Returning all types
        return DeviceConfig::select("device_configs.type")->get();
    }
}
