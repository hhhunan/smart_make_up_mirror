<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Role;

class RoleController extends Controller
{
    public $lang_code;
    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::with('User')->get();
        if ($role) {
            return $role;
        }
        return response()->json(['message' => \Lang::get('custom.no_item')], 400);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role();
        $validator = Validator::make($request->all(), $role->ruleForCreate);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->getMessages()], 400);
        }
        $role->name = $request->get('name');
        $role->save();
        if ($role->save()) {
            return response()->json(['message' => \Lang::get('custom.success_creat')], 200);
        }
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Role::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), (new Role)->ruleForEdit);
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }
        $role = Role::where("id", $id)->first();
        $role->name = $request->get('name');
        if ($role->save()) {
            return response()->json(['message' => \Lang::get('custom.success_updated')], 200);
        }
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Role::destroy($id)) {
            return response()->json(['message' => \Lang::get('custom.destroy')], 200);
        }
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }
}
