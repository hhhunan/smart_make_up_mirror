<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\PlayList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\AddonLib;

class MovieController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get movies and query.
        $movie = Movie::getQuery();

        // Movies options
        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($movie, [
                "join" => false,
                "search" => $this->addon->checkUriParameter($request, 'search') ? [
                    "column" => "name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? [
                    "column" => "name",
                    "flag" => $request->get('sort')
                ] : false
            ]);
        }
        else
        {
            $movie->orderBy('id', 'desc');
        }

        // pagination
        $page = $movie->paginate(12)->toArray();
        if (sizeof($page['data'])) {
            // marge sort type
            $page['sort_type'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            return $page;
        }

        // Returned not items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // checking send is file
        if (!$request->hasFile('video')) {
            return response()->json(['message' => \Lang::get('custom.error_file')], 400);
        }

        // Starting validator.
        $validator = Validator::make($request->all(), array(
            'video' => 'mimes:mp4,mov,avi,wmv|max:50000',
            'name' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
            'status' => 'required|numeric',
            'duration' => 'regex:/([0-9]?)([0-9]?):([0-9]?)([0-9]?)$/',
            'type' => 'required|numeric',
        ));
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->getMessages()], 400);
        }

        // request fields
        $video = $request->file('video');
        $extension = $video->getClientOriginalExtension();

        // generate and move files
        $_MD5 = md5_file($video);
        $generatedName = $_MD5 . time() . '.' . $extension;
        if (Movie::where("MD5", $_MD5)->exists()) {
            return response()->json(['message' => \Lang::get('custom.file_exists')], 400);
        }

        $path = env('UPLOAD_VIDEO_PATH');
        if (!File::exists(public_path($path))) {
            File::makeDirectory(public_path($path), $mode = 0777, true, true);
        }

        // create new movie object
        $movie = new Movie();
        $movie->name = $request->get('name');
        $movie->uri = $path . '/' . $generatedName;
        $movie->format = $extension;
        $movie->duration = $request->get('duration');
        $movie->file_size = filesize($request->file('video'));
        $movie->MD5 = $_MD5;

        $movie->status = $request->has('status') ? $request->get('status') : 1;
        $movie->type = $request->has('type') ? $request->get('type') : 1;

        // save movie object
        if ($movie->save() && request()->video->move(public_path($path), $generatedName)) {
            return response()->json(['message' => \Lang::get('custom.success_creat')], 200);
        }

        // returned error created messages
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Call Movie model and get this query.
        $movie = Movie::where('id', $id);
    
        // Checking exists item.
        if (!$movie->exists()) {
            return response()->json(['message' => \Lang::get('custom.record_not_found', ['id' => $id, 'model' => 'Movie'])], 400);
        }
        
        // returned all deices
        return $movie->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Movie::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Starting validator
        $validator = Validator::make($request->all(), array(
            'video' => 'mimes:mp4,mov,avi,wmv|max:50000',
            'name' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
            'status' => 'required|numeric',
            'duration' => 'regex:/([0-9]?)([0-9]):([0-9]?)([0-9])$/',
            'type' => 'numeric',
        ));
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->getMessages()], 400);
        }
        $movie = Movie::where("id", $id)->first();

        // check is file and generate/move file 
        if ($request->hasFile('video')) {
            $video = $request->file('video');
            $movie->file_size = filesize($request->file('video'));

            $extension = $video->getClientOriginalExtension();
            $_MD5 = md5_file($video);
            $generatedName = $_MD5 . time() . '.' . $extension;
            if (Movie::where("MD5", $_MD5)->exists()) {
                return response()->json(['message' => \Lang::get('custom.file_exists')], 400);
            }

            $path = env('UPLOAD_VIDEO_PATH');
            if (!File::exists(public_path($path))) {
                File::makeDirectory(public_path($path), $mode = 0777, true, true);
            }
            $old_file = $movie->uri;
            if (!$video->move(public_path($path), $generatedName)) {
                return response()->json(['message' => \Lang::get('custom.error_save')], 400);
            }

            if (file_exists(public_path($old_file))) {
                unlink(public_path($old_file));
            }

            $movie->format = $extension;
            $movie->duration = $request->has('duration') ? $request->get('duration') : $movie->duration;
            $movie->MD5 = $_MD5;
            $movie->uri = $path . '/' . $generatedName;
        }
        $movie->name = $request->has('name') ? $request->get('name') : $movie->name;
        $movie->status = $request->has('status') ? $request->get('status') : $movie->status;
        $movie->type = $request->has('type') ? $request->get('type') : $movie->type;

        // saved movie object
        if ($movie->save()) {
            return response()->json(['message' => \Lang::get('custom.success_updated')], 200);
        }

        // returned error updated message
        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = Movie::where("id", $id);
        if ($movie->exists()) {
            $movie = $movie->first();
            PlayList::where('movie_id', $id)->delete();
            if ($movie && file_exists(public_path() . '\\' . $movie->uri)) {
                unlink(public_path() . '\\' . $movie->uri);
            }
            if ($movie->delete()) {
                return response()->json(['message' => \Lang::get('custom.destroy')], 200);
            }
        }
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }


    /**
     * @return array|string
     */
    public function getMovieList()
    {
        // returned all movies list
        return Movie::select('movies.id', 'movies.name', 'movies.uri', 'movies.duration', 'movies.format')->get();
    }
}
