<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\AddonLib;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Models\Image;

class UserController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::with('Role')
            ->leftJoin("images", "users.image_id", "=", "images.id")
            ->select("users.*", "images.uri AS image");

        /*
         * Users options
         */
        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($user, [
                "join" => false,
                "search" => $this->addon->checkUriParameter($request, 'search') ? [
                    "column" => $request->has('search_column') ? $request->get('search_column') : "users.name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? [
                    "column" => $request->has('sort_column') ? $request->get('sort_column') : "name",
                    "flag" => $request->get('sort')
                ] : false
            ]);
        }
        else
        {
            $user->orderBy('id', 'desc');
        }
        
        // End Users options
        $page = $user->paginate(10)->toArray();
        if (sizeof($page['data'])) {
            $page['sort_type'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            return $page;
        }

        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $validation = Validator::make($request->all(), $user->ruleForCreate);

        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $user->name = $request->get('name');
        $user->password = bcrypt($request->get('password'));
        $user->email = $request->get('email');
        $user->role_id = $request->has('role_id') ? $request->get('role_id') : 3;
        if($request->has('image')){
            $imageSave = $this->addon->fileUploader($request->get('image'), 'profile');
            if(isset($imageSave['message'])){
                return response()->json($imageSave, 400);
            }$user->image_id = $imageSave;
        }

        if ($user->save()) {
            return response()->json(['message' => 'successfully created'], 200);
        }

        return response()->json(['message' => 'The user is not created.'], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::with('Role', 'image')
            ->leftjoin("images", "users.image_id", "=", "images.id")
            ->select("users.*", "images.uri AS image")->findOrFail($id);
    }

    /**
     * get_user
     * @return mixed
     */
    public function get_user()
    {
        return User::select(
            "users.name",
            "users.email",
            "images.uri AS image",
            "roles.name AS role_name",
            "roles.label AS role_label"
        )
        ->join("images", "users.image_id", "=", "images.id")
        ->join("roles", "users.role_id", "=", "roles.id")
        ->findOrFail(Auth::user()->id);
    }

    /**
     * get_user
     * @return mixed
     */
    public function editMyProfile(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'min:3|max:32',
            'password' => 'min:6|max:32',
            'confirm_password' => 'min:6|max:32|same:password',
            'image' => 'min:250',
        ]);
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $user = User::findOrFail(Auth::user()->id);

        if ($request->has("name")) {
            $user->name = $request->get('name');
        }

        if ($request->has("password")) {
            $user->password = bcrypt($request->get('password'));
        }
        if($request->has('image')){
            $imageSave = $this->addon->fileUploader($request->get('image'), 'profile', true, $user->image_id);
            if(isset($imageSave['message'])){
                return response()->json($imageSave, 400);
            }$user->image_id = $imageSave;
        }

        if ($request->has('image') || $request->has("name") || $request->has("password")) {
            if ($user->save())
                return response()->json(['message' => 'successfully updated.'], 200);
        }
        return response()->json(['message' => 'The user is not updated.'], 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user =User::findOrfail($id);
        if(isset($user->images->uri)){
            $path = $user->images->uri;
            if (file_exists(public_path($path))) {
                unlink(public_path($path));
            }
        }

        if ($user->delete()) {

            return response()->json(['message' => \Lang::get('custom.destroy')], 200);
        }
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }

    public function UserPersonalData(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'min:3|max:32',
            'image' => 'min:255',
        ]);
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $user = User::where("id", $id)->first();
        $user->name = $request->has('name') ? $request->get('name') : $user->name;

        if($request->has('image')){
            $imageSave = $this->addon->fileUploader($request->get('image'), 'profile', true, $user->image_id);
            if(isset($imageSave['message'])){
                return response()->json($imageSave, 400);
            }$user->image_id = $imageSave;
        }

        if ($request->has('image') || $request->has('name')) {
            if ($user->save()) {
                return response()->json(['message' => 'successfully updated.'], 200);
            }
        }

        return response()->json(['message' => 'The user is not updated.'], 400);
    }

    public function UserRoleSystem(Request $request, $id)
    {
        if ($id == Auth::user()->id)
        {
            return response()->json(['message' => "You cannot change your role."], 400);
        }

        $validation = Validator::make($request->all(), ['role_id' => 'required|numeric']);
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $user = User::findOrFail($id);
        $user->role_id = $request->get('role_id');
        if ($user->save()) {
            return response()->json(['message' => 'successfully updated.'], 200);
        }

        return response()->json(['message' => 'The user is not updated.'], 400);
    }

    public function UserPassword(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'password' => 'required|min:6|max:32',
            'confirm_password' => 'required|min:6|max:32|same:password'
        ]);
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $user = User::findOrFail($id);
        $user->password = bcrypt($request->get('password'));

        if ($user->save()) {
            return response()->json(['message' => 'successfully updated.'], 200);
        }

        return response()->json(['message' => 'The user is not updated.'], 400);
    }
}
