<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DeviceLabel;
use App\Models\Language;
use Illuminate\Support\Facades\Validator;

class DeviceLabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all devices and labels
        $labels = DeviceLabel::select(
            "device_labels.id",
            "device_labels.input",
            "device_labels.translation",
            "languages.code",
            "languages.name"
        )->join("languages", "device_labels.language_id", "=", "languages.id")->orderBy('languages.code')->get()->groupBy("id");
        
        if ($labels->count()) {
            return $labels->values();
        }

        // Returned, not found items messages.
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Starting validator.
        $validation = Validator::make($request->all(), [
            'input' => 'required|min:1|max:255',
            'translation' => 'required|array'
        ]);
        if (!$validation->fails()) {
            // Request fields
            $input = $request->get('input');
            $translation = $request->get('translation');

            if (DeviceLabel::where('input', $input)->exists()) {
                return response()->json(['message' => "Already exists."], 400);
            }
            $id = DeviceLabel::max('id');
            $id += 1;
            foreach ($translation as $key => $value) {
                $labels = new DeviceLabel;
                $labels->id = $id;
                $labels->input = $input;
                $labels->translation = $value;
                $labels->language_id = Language::where('code', $key)->value('id');

                if (!$labels->save()) {
                    // returned error create message
                    return response()->json(['message' => \Lang::get('custom.error_save')], 400);
                }
            }

            // returned success create message
            return response()->json(['message' => \Lang::get('custom.success_creat')], 200);
        }

        // returned validation error message
        return response()->json(['message' => $validation->errors()->getMessages()], 400);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get all device labels
        $labels = DeviceLabel::where('device_labels.id', $id)->orderBy('languages.code')->select(
            "device_labels.id",
            "device_labels.input",
            "device_labels.translation",
            "languages.code",
            "languages.name"
        )->join("languages", "device_labels.language_id", "=", "languages.id")->get()->groupBy("id");
        if ($labels->count()) {
            return $labels->values();
        }
        
        // Returned not items messages
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Starting validator.
        $validation = Validator::make($request->all(), [
            'input' => 'min:1|max:255',
            'translation' => 'array'
        ]);
        if (!$validation->fails()) {
            if ($request->has('input')) {
                if (DeviceLabel::where('id', '!=',$id)->where('input',$request->get('input'))->exists()) {
                    return response()->json(['message' => "Already exists."], 400);
                }
            }

            // Starting transaction
            foreach ($request->get('translation') as $k => $trans) {
                $lang_id = Language::where('code', $k);
                $labels = DeviceLabel::where(['id' => $id, 'language_id' => $lang_id->value('id')]);
                if (!$labels->exists()) {
                    return response()->json(['message' => \Lang::get('custom.no_item')], 400);
                }
                $labels->update([
                    'input' => $request->get('input'),
                    'translation' => $trans,
                    'language_id' => $lang_id->value('id')
                ]);
            }
            
            // Returned success updated messages.
            return response()->json(['message' => \Lang::get('custom.success_updated')], 200);
        }

        // returned validation error message
        return response()->json(['message' => $validation->errors()->getMessages()], 400);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Starting label destroy
        if (!DeviceLabel::destroy($id)) {
            return response()->json(['message' => \Lang::get('custom.error_delete')], 400);
        }
        
        // returned destroy error message
        return response()->json(['message' => \Lang::get('custom.destroy')], 200);
    }
}
