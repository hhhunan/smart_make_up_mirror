<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use App\AddonLib;
use Illuminate\Validation\Rule;


class PhotoController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // call all images list
        $img = Image::where("type", "!=", Image::$ImageTypeArray["profile"]);

        // Movies options
        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($img, [
                "join" => false,
                "search" => $this->addon->checkUriParameter($request, 'search') ? [
                    "column" => "name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? [
                    "column" => "name",
                    "flag" => $request->get('sort')
                ] : false
            ]);
        } else {
            $img->orderBy('id', 'desc');
        }


        // pagination to array 
        $page = $img->paginate(12)->toArray();
        if (sizeof($page['data'])) {
            // marge sort type
            $page['sort_type'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            return $page;
        }

        // returned not found items
        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        if (!$request->hasFile('image')) {
//            return response()->json(['message' => \Lang::get('custom.error_file')], 400);
//        }
//
//        // starting validator
//        $validator = Validator::make($request->all(), array_add([
//            'image' => 'required|mimes:jpeg,jpg,png|max:1000',
//            'name' => 'required|regex:/[a-zA-Z0-9 .-]*$/',
//            'status' => 'required|regex:/[0-9]$/',],
//            'type', Rule::in(array_keys($this->ImageTypeArray))));
//        if ($validator->fails()) {
//            return response()->json(['message' => $validator->errors()->getMessages()], 400);
//        }
//        // request fields
//        $image = $request->file('image');
//        $extension = $image->getClientOriginalExtension();
//
//        // generate and move files
//        $_MD5 = md5_file($image);
//        $generatedName = $_MD5 . time() . '.' . $extension;
//        if (Image::where("MD5", $_MD5)->exists()) {
//            return response()->json(['message' => \Lang::get('custom.file_exists')], 400);
//        }
//
//        $path = env('UPLOAD_IMAGE_PATH');
//        if (!File::exists(public_path($path))) {
//            File::makeDirectory(public_path($path), $mode = 0777, true, true);
//        }
//
//        // create new movie object
//        $image = new Image();
//        $image->name = $request->get('name');
//        $image->uri = $path . '/' . $generatedName;
//        $image->file_size = filesize($request->file('image'));
//        $image->MD5 = $_MD5;
//        $image->status = $request->has('status') ? $request->get('status') : 1;
//        $image->type =  $this->ImageTypeArray[$request->get('type')];
//
//        // save movie object
//        if ($image->save() && request()->image->move(public_path($path), $generatedName)) {
//            return response()->json($image, 200);
//        }
//
//        // returned error created messages
//        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
//    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Call images model and get this query.
        $images = Image::where('id', $id);

        // Checking exists item.
        if (!$images->exists()) {
            return response()->json(['message' => \Lang::get('custom.record_not_found', ['id' => $id, 'model' => 'Photo'])], 400);
        }

        // returned image by id
        return $images->first();
    }

//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  int $id
//     * @return \Illuminate\Http\Response
//     */
//    public function edit($id)
//    {
//        //
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request $request
//     * @param  int $id
//     * @return \Illuminate\Http\Response
//     */
//    public function update(Request $request, $id)
//    {
//
//        // starting validator
//        $validator = Validator::make($request->all(), array_add([
//            'image' => 'required|mimes:jpeg,jpg,png|max:1000',
//            'name' => 'required|regex:/[a-zA-Z0-9 .-]*$/',
//            'status' => 'required|regex:/[0-9]$/',],
//            'type', Rule::in(array_keys($this->ImageTypeArray))));
//
//        if ($validator->fails()) {
//            return response()->json(['message' => $validator->errors()->getMessages()], 400);
//        }
//
//        $img = Image::where("id", $id)->first();
//
//        // check is file and generate/move file
//        if ($request->hasFile('image')) {
//            $image = $request->file('image');
//            $img->file_size = filesize($request->file('image'));
//
//            $extension = $image->getClientOriginalExtension();
//            $_MD5 = md5_file($image);
//            $generatedName = $_MD5 . time() . '.' . $extension;
//            if (Image::where("MD5", $_MD5)->exists()) {
//                return response()->json(['message' => \Lang::get('custom.file_exists')], 400);
//            }
//
//            $path = env('UPLOAD_IMAGE_PATH');
//            if (!File::exists(public_path($path))) {
//                File::makeDirectory(public_path($path), $mode = 0777, true, true);
//            }
//
//            $old_file = $img->uri;
//
//            if (!$image->move(public_path($path), $generatedName)) {
//                return response()->json(['message' => \Lang::get('custom.error_save')], 400);
//            }
//            if (file_exists(public_path($old_file))) {
//                unlink(public_path($old_file));
//            }
//
//            $img->MD5 = $_MD5;
//            $img->uri = $path . '/' . $generatedName;
//        }
//        $img->name = $request->has('name') ? $request->get('name') : $img->name;
//        $img->status = $request->has('status') ? $request->get('status') : $img->status;
//        $img->type = $this->ImageTypeArray[$request->get('type')];
//
//        // saved movie object
//        if ($img->save()) {
//            return response()->json(['message' => \Lang::get('custom.success_updated')], 200);
//        }
//
//        // returned error updated message
//        return response()->json(['message' => \Lang::get('custom.error_save')], 400);
//    }
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  int $id
//     * @return \Illuminate\Http\Response
//     */
//    public function destroy($id)
//    {
//        $image = Image::where("id", $id)->first();
//        User::where('image_id', $id)->update(['image_id' => Image::where("type", $this->image_types["default_avatar"])->value("id")]);
//
//        // remove images in product
//        ProductImage::where('image_id', $id)->delete();
//
//        // remove files
//        if (file_exists($image->uri)) {
//            unlink($image->uri);
//        }
//
//        // start destroy
//        if ($image->delete())
//            // returning success message
//            return response()->json(['message' => \Lang::get('custom.destroy')], 200);
//        else
//            // returned error deleted messages
//            return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
//    }

    /**
     * get images by types
     *
     * @return \Illuminate\Http\Response
     */
//    public function getAllImagesByType(Request $request)
//    {
//        $imageType = $request->get('type');
//        if (isset(Image::$ImageTypeArray[$imageType])) {
//            if ($imageType == 'brand') {
//                $images = Image::doesntHave($imageType . 's')->where('type', $this->image_types[$imageType]);
//            }
//            if ($imageType == 'product') {
//                $images = Image::doesntHave("product_images")->where('type', $this->image_types[$imageType]);
//            }
//            if ($imageType == 'style') {
//                $images = Image::doesntHave("make_up_style_images")->where('type', $this->image_types[$imageType]);
//            }
//            if ($imageType == 'method') {
//                $images = Image::doesntHave("make_up_method_images")->where('type', $this->image_types[$imageType]);
//            }
//            if ($imageType == 'country_flag') {
//                $images = Image::with("languages")->where('type', $this->image_types[$imageType]);
//            }
//
//            if ($images->count()) {
//                return $images->paginate(20);
//            }
//        }
//
//        // returned not items messages
//        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
//    }

    /**
     * returned all image types
     *
     * @return \Illuminate\Http\Response
     */
//    public function getImageTypes()
//    {
//        return array_keys(Image::$ImageTypeArray);
//    }
}
