<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;
use App\Models\Role;

class RegisterController extends Controller
{
    use IssueTokenTrait;

	private $client;

	public function __construct(){
		$this->client = Client::find(1);
	}

    public function register(Request $request) {

    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email|unique:users,email',
    		'role_id' => 'required|numeric',
    		'password' => 'required|min:6'
    	]);

    	$user = User::create([
    		'name' => request('name'),
            'email' => request('email'),
            'user_id' => $request->has('role_id') ? $request->get('role_id') : 3,
    		'password' => bcrypt(request('password'))
		]);

    	return $this->issueToken($request, 'password');
    }
}
