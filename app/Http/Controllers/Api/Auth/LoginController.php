<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;
use Symfony\Component\Console\Descriptor\JsonDescriptor;
use function GuzzleHttp\json_decode;
use App\User;
use App\Models\Role;
class LoginController extends Controller
{

    use IssueTokenTrait;

	private $client;

	public function __construct() {
		$this->client = Client::findOrFail(1);
	}

    public function login(Request $request) {
    	$this->validate($request, [
    		'username' => 'required',
    		'password' => 'required'
    	]);

		$response = $this->issueToken($request, 'password');

		if($response->status() == 200)
		{
			$user = User::with('Role')->where('email', $request->get('username'))->leftJoin("images","users.image_id","=","images.id")
                ->select("users.*", "images.uri AS image")->get();
			$content = $response->content();
			$content = json_decode($content);
			if(isset($user[0]))
            {
                $content->user_id = $user[0]->id;
                $content->user_type = isset($user[0]->role->name) ? $user[0]->role->name : "user";
                $content->role_label = isset($user[0]->role->label) ? $user[0]->role->label : "user";
                $content->user_name = $user[0]->name;
                $content->user_image = $user[0]->image;
            }
			$contentJson = json_encode($content);
			$response->setContent($contentJson);
		}
		return $response;
	}
	
	public function device_login(Request $request) {
		$this->validate($request, [
    		'username' => 'required',
    		'password' => 'required'
    	]);

		$grantType = "password";

		$params = [
    		'grant_type' => $grantType,
    		'client_id' => $request->get("client_id"),
    		'client_secret' => $request->get("client_secret"),
    		'scope' => ""
    	];

        if($grantType !== 'social') {
            $params['username'] = $request->username ?: $request->email;
        }

    	$request->request->add($params);

    	$proxy = Request::create('oauth/token', 'POST');

    	return Route::dispatch($proxy);
	}

    public function refresh(Request $request) {
    	$this->validate($request, [
    		'refresh_token' => 'required'
    	]);

    	return $this->issueToken($request, 'refresh_token');
    }

    public function logout() {

    	$accessToken = Auth::user()->token();

    	DB::table('oauth_refresh_tokens')
    		->where('access_token_id', $accessToken->id)
    		->update(['revoked' => true]);

    	$accessToken->revoke();

    	return response()->json([], 204);
    }
}
