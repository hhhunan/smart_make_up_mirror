<?php

namespace App\Http\Controllers;

use App\Models\ProductSession;
use App\Models\Session;
use App\Models\Product;
use App\Models\Language;
use App\Models\SessionTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\AddonLib;
use Illuminate\Validation\Rule;
use DB;

class SessionController extends Controller
{
    public $lang_code;
    public $addon;

    public function __construct(Request $request)
    {
        $this->lang_code = $request->header("language");
        $this->addon = new AddonLib();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang_id = Language::where('code', $this->lang_code ? $this->lang_code : "en")->value('id');

        $validation = Validator::make($request->all(),['type'=>'required', 'alpha_dash',  Rule::in(['make_up_by_style', 'make_up_by_product'])]);
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }


        $session = Session::with([
            "play_list" => function ($query) {
                $query->orderBy('ordering')->with("movie");
            },
            "translation" => function ($query) use ($lang_id) {
                $query->with('language')->where('language_id', $lang_id);
            }
        ])->where('type', $request->get('type'))->orderBy('id','desc');

         /*
         * Sessions options
         */

        if ($this->addon->checkUriParameter($request, 'search') || $this->addon->checkUriParameter($request, 'sort')) {
            $this->addon->customSortSearch($session, [
                "join" => [
                    "select" => "sessions.*",
                    "table" => "session_translations",
                    "first" => "sessions.id",
                    "second" => "session_translations.session_id",
                    "type" => "inner"
                ],
                "search" => $this->addon->checkUriParameter($request, 'search') ? [
                    "column" => "session_translations.name",
                    "pattern" => $request->get('search')
                ] : false,
                "sorting" => $this->addon->checkUriParameter($request, 'sort') ? [
                    "column" => "session_translations.name",
                    "flag" => $request->get('sort')
                ] : false,
                "lang_id" => $lang_id
            ]);
        }

        $page = $session->paginate(9)->toArray();
        if (sizeof($page['data'])) {
            $page['sort_type'] = $this->addon->checkUriParameter($request, 'sort') ? $request->get('sort') : "NORMAL";
            return $page;
        }

        return response()->json(['message' => \Lang::get('custom.no_item')], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session = new Session();
        $validator = Validator::make(
            $request->all(),$session->ruleForCreate
        );
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->getMessages()], 400);
        }

        $session->play_list_id = $request->get('play_list_id');
        $session->status = $request->has('status') ? $request->get('status') : 0;
        $session->type = $request->get('type');

        if (!$session->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }

        DB::transaction(function () use ($session, $request) {
            $languages = Language::pluck('code', 'id');
            $translations = [];
            foreach ($languages as $lngId => $lngCode) {
                if (array_key_exists($lngCode, $request->get('name')) && array_key_exists($lngCode, $request->get('description'))) {
                    $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                    $translations[$lngId]['description'] = $request->get('description')[$lngCode];
                    $translations[$lngId]['session_id'] = $session->id;
                    $translations[$lngId]['language_id'] = $lngId;
                }
            }
            $session->translationForSync()->sync($translations);
        }, 3);
        return response()->json(['messages' => \Lang::get('custom.success_creat')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Session::with([
            "play_list" => function ($query) {
                $query->orderBy('ordering')->with("movie");
            },
            "translation" => function ($query) {
                if ($this->lang_code)
                    $query->with('language')->whereHas('language', function ($query) {
                    $query->where('code', $this->lang_code);
                });
                else
                    $query->with('language');
            }
        ])->FindOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
        $session = Session::findOrFail($session);
        if ($session) {
            return $session;
        }
        return response()->json(['message' => \Lang::get('custom.no_item')], 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), array_add((new Session)->ruleForEdit,'status', Rule::in(['make_up_by_style', 'make_up_by_product'])));
        if ($validation->fails()) {
            return response()->json(['message' => $validation->errors()->getMessages()], 400);
        }

        $session = Session::where("id", $id)->first();
        $session->status = $request->has('status') ? $request->get('status') : $session->status;
        $session->type = $request->get('type');
        $session->play_list_id = $request->get('play_list_id');

        if (!$session->save()) {
            return response()->json(['message' => \Lang::get('custom.error_save')], 400);
        }
        if ($request->get('name') && $request->get('description')) {
            DB::transaction(function () use ($session, $request) {

                $languages = Language::pluck('code', 'id');
                $translations = [];
                foreach ($languages as $lngId => $lngCode) {
                    if (array_key_exists($lngCode, $request->get('name')) && array_key_exists($lngCode, $request->get('description'))) {
                        $translations[$lngId]['name'] = $request->get('name')[$lngCode];
                        $translations[$lngId]['description'] = $request->get('description')[$lngCode];
                        $translations[$lngId]['session_id'] = $session->id;
                        $translations[$lngId]['language_id'] = $lngId;
                    }
                }
                $session->translationForSync()->sync($translations);

            }, 3);
        }
        return response()->json(['messages' => \Lang::get('custom.success_updated')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Session $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $session = Session::findOrFail($id);

        if ($session->exists()) {

            $product_id = ProductSession::where('session_id', $id)->value('product_id');
            // Destroyed brand in products.
            ProductSession::where('session_id', $id)->delete();

            if ($session->delete()) {
                if (!ProductSession::where('product_id', $product_id)->count())
                    Product::where("id", $product_id)->update(['status' => '0']);

                // Returning success destroy messages.
                return response()->json(['message' => \Lang::get('custom.destroy')], 200);
            }
        }
        // Returned error updated messages.
        return response()->json(['message' => \Lang::get('custom.error_destroy')], 400);
    }

    /**
     * getSession
     * @param mixed $request
     * @return mixed
     */
    public function getSession()
    {
        return Session::with(["translation" => function ($query) {
            $query->with('language')->whereHas('language', function ($query) {
                $query->where('code', "en");
            });
        }])->select('sessions.id')->get();
    }
}
