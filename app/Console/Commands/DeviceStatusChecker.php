<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Device;

class DeviceStatusChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:device';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        $output->writeln("[" . date('Y-m-d h:i:s', time()) . "] - The system checks for inactive devices..");

        $users = Device::where("status", 1)->get();
        foreach ($users as $key => $value) {
            if (isset($value->modify_status_date)) {
                $current_date = date('Y-m-d h:i:s', strtotime(" +0 minutes "));
                if (strcmp($value->modify_status_date, $current_date) != 1) {
                    Device::where("id", $value->id)->update(['status' => 0]);
                    $output->writeln('Detected an inactive device: ID/' . $value->id);
                }
            }
        }
        $output->writeln("[" . date('Y-m-d h:i:s', time()) . "] - End checking..");
    }
}
