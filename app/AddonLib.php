<?php

namespace App;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

use App\Models\Image;
use Illuminate\Support\Facades\File;

use App\ImageTypes;
use Illuminate\Support\Facades\Storage;

/**
 * 
 */
class AddonLib
{
    use ImageTypes;
    /**
     * Get Translation
     */
    public function translation($query, $lang_code)
    {
        $query->with(["translation" => function ($query) use ($lang_code) {
            if ($lang_code)
                $query->with('language')->whereHas('language', function ($query) use ($lang_code) {
                $query->where('code', $lang_code);
            });
            else
                $query->with('language')->orderBy('language_id');
        }]);
    }

    /**
     * Get Translation And Search
     */
    public function translationSearch(
        $query,
        $lang_id, // Language id [1,2 ...]
        $search_pattern, // Search pattern field
        $search_column // Sorting column name [name/id/created_at ...]
    ) {
        $query
            ->with(["translation" => function ($query) use ($lang_id) {
                $query->with('language')->whereLanguage_id($lang_id);
            }])
            ->whereHas('translation', function ($query) use ($search_pattern, $lang_id, $search_column) {
                $query->where($search_column, 'LIKE', "%" . $search_pattern . "%")->where('language_id', $lang_id);
            });
    }

    /**
     * Custom Sorting And Search
     * @param mixed $query 
     * @param mixed $option 
     * @return mixed 
     */
    public function customSortSearch($query, $option)
    {
        if ($option["join"]) {
            $query
                ->select($option["join"]["select"])
                ->Join($option["join"]["table"], $option["join"]["first"], "=", $option["join"]["second"], $option["join"]["type"])
                ->where(function ($medias) use ($option) {
                    $medias->where($option["join"]["table"] . ".language_id", '=', $option["lang_id"])
                        ->orWhereNull($option["join"]["table"] . ".language_id");
                });
        }

        if ($option["search"]) {
            $query->where($option["search"]["column"], "LIKE", "%" . $option["search"]["pattern"] . "%");
        }

        if ($option["sorting"]) {
            $query->orderBy($option["sorting"]["column"], $option["sorting"]["flag"]);
        }
    }

    /**
     * checkUriParameter
     * @param mixed $request 
     * @param mixed $field 
     * @param mixed $count 
     * @return mixed 
     */
    public function checkUriParameter($request, $field, $count = 3)
    {
        return ($request->has($field) && strlen($request->get($field)) >= $count);
    }

    /**
     * checkRole
     * @param mixed $role_name
     * @return mixed
     */
    public function checkRole($role_name)
    {
        $role = Role::where("id", Auth::user()->role_id);
        if($role->exists())
        {
            if(strcasecmp($role->value("name"), $role_name) == 0)
            {
                return true;
            }
        }
        return false;
    }

    public function file_upload($request, $name, $type_name, $file_delete = false, $id = null)
    {
        // request fields
        $file = $request->file($name);
        $extension = $file->getClientOriginalExtension();

        // generate and move files
        $generatedName = $file->getFilename() . time() . '.' . $extension;

        $path = env('UPLOAD_IMAGE_PATH');
        if (!File::exists(public_path($path))) {
            File::makeDirectory(public_path($path), $mode = 0777, true, true);
        }

        if ($file_delete)
        {
            // find by id movie object
            $image = Image::findOrFail($id);
            // remove files
            if (file_exists(public_path($image->uri))) {
                unlink(public_path($image->uri));
            }
        }
        else
        {
            // create new movie object
            $image = new Image();
        }

        $image->name = $generatedName;
        $image->uri = $path . '/' . $generatedName;
        $image->file_size = filesize($request->file($name));
        $image->status = 1;
        $image->type = $this->image_types[$type_name];
        if (!$file->move(public_path($path), $generatedName))
        {
            return ['message' => 'File upload error.'];
        }
        // save movie object
        if (!$image->save() ) {
            return ['message' => 'Error saved images.'];
        }
        return $image->id;
    }

    public function fileUploader($image_data, $by_type, $file_delete = false, $id = null)
    {
        $data = explode(',', $image_data);
        $file_detal = explode('/', explode(':', explode(';', $data[0])[0])[1]);
        $file_format = $file_detal[1];
        $name = str_random(32);
        $image = base64_decode($data[1]);
        $size = strlen($image);
        $path = env('UPLOAD_IMAGE_PATH');
        $file_name = $name.'.'.$file_format;

        if (!($file_format == 'jpeg' || $file_format == 'jpg' || $file_format == 'png'))
        {
            return ['message' => 'The image must be a file of type: jpeg, jpg, png.'];
        }
        $size_env = env('SIZE_IMAGE');
        if ($size >= $size_env)
        {
            return ['message' => 'The image may not be greater than 1 MB'];
        }

        if (!File::exists(public_path($path))) {
            File::makeDirectory(public_path($path, $mode = 0777, true, true));
        }
        Storage::disk('uploads')->put($path .'/'. $file_name, $image);

        if ($file_delete && Image::findOrFail($id)->name !='avatar.png')
        {
            // find by id movie object
            $image = Image::findOrFail($id);
            // remove files
            if (file_exists(public_path($image->uri))) {
                unlink(public_path($image->uri));
            }
        }
        else
        {
            // create new movie object
            $image = new Image();
        }

        $image->name = $file_name;
        $image->uri = $path . '/' . $file_name;
        $image->file_size = $size;
        $image->md5 = md5_file(public_path($path . '/' . $file_name));
        $image->status = 1;
        $image->type = Image::$ImageTypeArray[$by_type];

        // save movie object
        if (!$image->save() ) {
            return ['message' => 'Error saved images.'];
        }
        return $image->id;
    }

}
