<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserReset extends Mailable
{
    use Queueable, SerializesModels;

    protected $settings;

    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    public function build()
    {
        return $this->view('UserReset', $this->settings)->subject('Your Reminder!');
    }

}