<?php

namespace App;

trait ImageTypes
{
    public $image_types = [
        'flag' =>  1,
        'product' =>  2,
        'brand' =>  3,
        'method' =>  4,
        'style' =>  5,
        'profile' =>  6,
        'by_live' =>  7,
        'by_session_guide' =>  8,

    ];
}