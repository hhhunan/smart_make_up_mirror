<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSession extends Model
{
	public $incrementing = false;
	
	public $timestamps = false;
	
	protected $casts = [
		'product_id' => 'int',
		'session_id' => 'int'
	];

	protected $fillable = [
		'product_id',
		'session_id'
	];

	/**
	 * session
	 * @return mixed 
	 */
	public function session()
	{
		return $this->belongsTo(\App\Models\Session::class);
	}

	/**
	 * product
	 * @return mixed 
	 */
	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}
}
