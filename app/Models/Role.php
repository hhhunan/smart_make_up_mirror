<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    public $ruleForCreate = [
        'name' => 'required|regex:/^[A-Za-z\s-_]+$/',
    ];

    public $ruleForEdit = [
        'name' => 'required|unique:sessions|regex:/^[A-Za-z\s-_]+$/',
    ];
    
    /**
     * function User
     * @return mixed 
     */
    public function User()
    {
        return $this->belongsToMany(\App\User::class, 'users_roles');
    }
}
