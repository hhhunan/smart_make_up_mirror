<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Language
 * @var mixed
 */
class Language extends Model
{
    protected $table = "languages";
    
    public $timestamps = false;


    public $ruleForCreate = [
        'name' => 'required',
        'status' => 'numeric',
        'code' => 'required|min:1|max:50',
        'default' => 'numeric',
        'image' => 'required|min:150',
    ];

    public $ruleForEdit = [
        'name' => 'min:1|max:255',
        'status' => 'numeric',
        'code' => 'min:1|max:50',
        'default' => 'numeric',
        'image' => 'min:150',
    ];

    /**
	 * image
	 * @return mixed 
	 */
    public function image()
    {
        return $this->hasOne(Image::class, "id", "image_id");
    }
}