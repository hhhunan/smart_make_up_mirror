<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MakeUpMethodTranslation extends Model
{
    protected $table = "make_up_method_translations";

    public $timestamps = false;

    public $hidden = ["id", "language_id"];

    public $ruleForCreate = [
        'name' => 'required|max:100|regex:/^[A-Za-z\s-_]+$/',
    ];

    public $ruleForEdit = [
        'name' => 'required|unique:brands|max:100|regex:/^[A-Za-z\s_]+$/',
    ];

    protected $fillable = [
        'make_up_method_id',
        'language_id'
    ];

    /**
     * function language
     * @return mixed
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'language_id');
    }
}
