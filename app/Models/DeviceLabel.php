<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceLabel extends Model
{
    protected $table = "device_labels";

    public $timestamps = false;

    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'language_id');
    }
}
