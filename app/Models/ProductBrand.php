<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 26 Sep 2017 12:56:25 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProductBrand
 * 
 * @property int $product_id
 * @property int $brand_id
 * 
 * @property \App\Models\Brand $brand
 * @property \App\Models\Product $product
 *
 * @package App\Models
 */
class ProductBrand extends Eloquent
{
	public $incrementing = false;
	
	public $timestamps = false;

	protected $casts = [
		'product_id' => 'int',
		'brand_id' => 'int'
	];

	protected $fillable = [
		'product_id',
		'brand_id'
	];

	/**
	 * brand
	 * @return mixed 
	 */
	public function brand()
	{
		return $this->belongsTo(\App\Models\Brand::class);
	}

	/**
	 * product
	 * @return mixed 
	 */
	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}
}
