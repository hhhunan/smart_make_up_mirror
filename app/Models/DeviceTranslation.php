<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DeviceTranslation
 * @var mixed
 * @property \Illuminate\Database\Eloquent\Collection $language
 */
class DeviceTranslation extends Model
{
    protected $table = "device_translations";
    
    public $timestamps = false;

    public $hidden = ["device_id", "id", "language_id"];

    public $ruleForCreate = [
        'name' => 'required|max:100|regex:/^[A-Za-z\s-_]+$/',
    ];

    public $ruleForEdit = [
        'name' => 'required|unique:brands|max:100|regex:/^[A-Za-z\s-_]+$/',
    ];

    /**
     * function language
     * @return mixed 
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'language_id');
    }
}
