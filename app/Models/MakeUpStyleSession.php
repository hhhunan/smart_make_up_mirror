<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MakeUpStyleSession extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        'make_up_style_id' => 'int',
        'session_id' => 'int'
    ];

    protected $fillable = [
        'make_up_style_id',
        'session_id'
    ];

    /**
     * session
     * @return mixed
     */
    public function session()
    {
        return $this->belongsTo(\App\Models\Session::class);
    }

    /**
     * product
     * @return mixed
     */
    public function makeupstyle()
    {
        return $this->belongsTo(\App\Models\MakeUpStyle::class);
    }
}
