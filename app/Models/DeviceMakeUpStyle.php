<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceMakeUpStyle extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        'device_id' => 'int',
        'make_up_style_id' => 'int'
    ];

    protected $fillable = [
        'device_id',
        'make_up_style_id'
    ];

    /**
     * device
     * @return mixed
     */
    public function device()
    {
        return $this->belongsTo(\App\Models\Device::class);
    }

    /**
     * product
     * @return mixed
     */
    public function style()
    {
        return $this->belongsTo(\App\Models\MakeUpStyle::class);
    }
}
