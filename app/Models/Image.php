<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 15:09:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Image
 *
 * @property int $id
 * @property string $name
 * @property string $uri
 * @property string $md5
 * @property int $file_size
 * @property int $type
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Image extends Eloquent
{
    protected $casts = [
        'type' => 'int',
        'status' => 'int'
    ];
    public static $ImageTypeArray = [
        'flag' =>  1,
        'product' =>  2,
        'brand' =>  3,
        'method' =>  4,
        'style' =>  5,
        'profile' =>  6,
        'by_live' => 7,
        'by_session_guide' => 8,
    ];

    protected $fillable = [
        'name',
        'uri',
        'md5',
        'type',
        'status',
        'file_size',
    ];
	/**
	 * brands
	 * @return mixed 
	 */
    public function brands()
    {
        return $this->hasOne(\App\Models\Brand::class);
	}
	/**
	 * languages
	 * @return mixed 
	 */
    public function languages()
    {
        return $this->hasOne(\App\Models\Language::class);
    }
//    public function makeupstyle_images()
//    {
//        return $this->hasOne(\App\Models\MakeUpStyleImage::class);
//    }

}
