<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 15:09:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class User extends Eloquent
{
    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
        'remember_token'
    ];

    public $ruleForCreate = [
        'name' => 'required|min:3|max:32',
        'password' => 'required|min:6|max:32',
        'confirm_password' => 'required|min:6|max:32|same:password',
        'email' => 'required|email|max:25|regex:/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/|unique:users',
        "role_id" => 'required|numeric'
    ];
    
    public $ruleForEdit = [
        'name' => 'min:3|max:32',
        'password' => 'min:6|max:32',
        'confirm_password' => 'min:6|max:32|same:password',
    ];
}
