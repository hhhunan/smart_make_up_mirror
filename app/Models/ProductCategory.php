<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 23:27:49 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProductCategory
 * 
 * @property int $product_id
 * @property int $category_id
 * 
 * @property \App\Models\Category $category
 *
 * @package App\Models
 */
class ProductCategory extends Eloquent
{
	public $incrementing = false;
	
	public $timestamps = false;

	protected $casts = [
		'product_id' => 'int',
		'category_id' => 'int'
	];

	protected $fillable = [
		'product_id',
		'category_id'
	];

	/**
	 * category
	 * @return mixed 
	 */
	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}
    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }
}
