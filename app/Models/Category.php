<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 15:09:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Category
 * 
 * @property int $id
 * @property string $name
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\ProductCategory $product_category
 *
 * @package App\Models
 */
class Category extends Eloquent
{
    protected $casts = [
        'status' => 'int'
    ];

    public $hidden = [
        "pivot"
    ];

    protected $fillable = [
        'status'
    ];

    public $ruleForCreate = [
        'name' => 'required|array',
        'name.ru' => 'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/'
    ];

    public $ruleForEdit = [
        'name' => 'array',
        'name.ru' => 'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/'
    ];

    /**
     * function product_category
     * @return mixed
     */
    public function product_category()
    {
        return $this->hasOne(\App\Models\ProductCategory::class);
    }

    /**
     * function brand
     * @return mixed
     */
    public function translation()
    {
        return $this->hasMany(CategoryTranslation::class, 'categorie_id', 'id');
    }

    /**
     * function translationForSync
     * @return mixed
     */
    public function translationForSync()
    {
        return $this->belongsToMany(CategoryTranslation::class, 'categorie_translations', 'categorie_id', 'language_id');
    }
}
