<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 26 Sep 2017 12:55:48 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $devices
 * @property \Illuminate\Database\Eloquent\Collection $brands
 * @property \App\Models\ProductCategory $product_category
 * @property \Illuminate\Database\Eloquent\Collection $images
 *
 * @package App\Models
 */
class Product extends Eloquent
{
	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'status'

	];

	public $hidden = [
		"pivot"
	];
	public $ruleForCreate = [
		"name" => "required|array",
        'name.ru'=>'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
		"description" => "required|array",
        'description.ru'=>'min:1|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"\[\]{},+<>@&]*$/',
		'description.es' => 'min:1|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_?!()%`.\'"\[\]{},+<>@&]*$/',
		'description.en' => 'min:1|regex:/[a-zA-Z0-9 \-\/_?!()%`.\'"\[\]{},+<>@&]*$/',
        'image' => 'required|min:250',
		"session_ids" => "array",
		"device_ids" => "array",
		"brand_ids" => "array",
		"muvie_ids" => "array",

		"session_ids.*" => "numeric",
		"device_ids.*" => "numeric",
		"brand_ids.*" => "numeric",
		"muvie_ids.*" => "numeric"
	];

	public $ruleForEdit = [
		"name" => "array",
        'name.ru'=>'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
		"description" => "array",
        'description.ru'=>'min:1|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"\[\]{},+<>@&]*$/',
		'description.es' => 'min:1|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_?!()%`.\'"\[\]{},+<>@&]*$/',
		'description.en' => 'min:1|regex:/[a-zA-Z0-9 \-\/_?!()%`.\'"\[\]{},+<>@&]*$/',
        'image' => 'min:250',

		"session_ids" => "array",
		"device_ids" => "array",
		"brand_ids" => "array",
		"muvie_ids" => "array",

		"session_ids.*" => "numeric",
		"device_ids.*" => "numeric",
		"brand_ids.*" => "numeric",
		"muvie_ids.*" => "numeric"
	];

	/**
	 * devices
	 * @return mixed 
	 */
	public function devices()
	{
		return $this->belongsToMany(\App\Models\Device::class, 'device_products');
	}

	/**
	 * brands
	 * @return mixed 
	 */
	public function brands()
	{
		return $this->belongsToMany(\App\Models\Brand::class, 'product_brands');
	}

	/**
	 * category
	 * @return mixed 
	 */
	public function category()
	{
		return $this->belongsToMany(\App\Models\Category::class, 'product_categories');
	}

	/**
	 * images
	 * @return mixed 
	 */
	public function images()
	{
		return $this->hasOne(\App\Models\Image::class, 'id', 'image_id');
	}

	/**
	 * sessions
	 * @return mixed 
	 */
	public function sessions()
	{
		return $this->belongsToMany(\App\Models\Session::class, 'product_sessions');
	}

	/**
	 * function translation
	 * @return mixed 
	 */
	public function translation()
	{
		return $this->hasMany(ProductTranslation::class);
	}

	/**
     * function translationForSync
     * @return mixed
     */
    public function translationForSync()
    {
        return $this->belongsToMany(ProductTranslation::class, 'product_translations', 'product_id', 'language_id');
    }

    public function makeupmethod()
    {
        return $this->hasOne(MakeUpMethod::class, 'id', 'make_up_method_id');
    }

}
