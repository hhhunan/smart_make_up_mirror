<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 15:09:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PlayList
 * 
 * @property int $id
 * @property int $movie_id
 * @property int $ordering
 *
 * @property \App\Models\Movie $movie
 * @property \Illuminate\Database\Eloquent\Collection $sessions
 *
 * @package App\Models
 */
class PlayList extends Eloquent
{
	public $incrementing = false;
	
	public $timestamps = false;

	protected $casts = [
		'movie_id' => 'int'
	];

	protected $fillable = [
		'id',
		'movie_id',
		'ordering'
	];
	
	public $rule = [
		'movie_id' => 'required|numeric',
		'id' => 'required|numeric'
	];


	/**
	 * movie
	 * @return mixed 
	 */
	public function movie()
	{
		return $this->belongsTo(\App\Models\Movie::class);
	}

	/**
	 * sessions
	 * @return mixed 
	 */
	public function sessions()
	{
		return $this->hasMany(Session::class, 'play_list_id', 'id');
	}
}
