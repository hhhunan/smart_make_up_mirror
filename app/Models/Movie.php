<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 15:09:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Movie
 * 
 * @property int $id
 * @property string $name
 * @property string $uri
 * @property string $format
 * @property string $duration
 * @property string $MD5
 * @property int $type
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\PlayList $play_list
 *
 * @package App\Models
 */
class Movie extends Eloquent
{
	protected $casts = [
		'type' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'uri',
		'name',
		'format',
		'duration',
		'type',
		'status'
	];

	/**
	 * generateNames
	 * @return mixed 
	 */
	public function generateNames()
	{
		return hash('sha256', time());
	}

	/**
	 * play_list
	 * @return mixed 
	 */
	public function play_list()
	{
		return $this->hasOne(\App\Models\PlayList::class);
	}

	/**
	 * function translation
	 * @return mixed 
	 */
	public function translation()
	{
		return $this->hasMany(CategoryTranslation::class, 'image_id', 'id');
	}

	/**
	 * movie
	 * @return mixed 
	 */
	public function movie()
	{
		return $this->hasOne(\App\Models\Movie::class, 'id', 'movie_id');
	}
}
