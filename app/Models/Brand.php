<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 26 Sep 2017 13:35:28 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Brand
 * 
 * @property int $id
 * @property string $name
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Brand extends Eloquent
{
	protected $casts = [
		'status' => 'int',
		'image_id' => 'int'

	];

	protected $fillable = [
		'status',
		'image_id'
	];

	public $hidden = [
		"pivot"
	];

	public $ruleForCreate = [
        'name'=>'required|array',
        'name.ru'=>'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'image' => 'required|min:250',

	];
	
	public $ruleForEdit = [
        'name'=>'array',
        'name.ru'=>'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'image' => 'min:250',
    ];

	/**
	 * function products
	 * @return mixed
	 */
	public function products()
	{
		return $this->belongsToMany(\App\Models\Product::class, 'product_brands');
	}

	/**
     * function brand
     * @return mixed
     */
    public function translation()
	{
		return $this->hasMany(BrandTranslation::class, 'brand_id', 'id');
	}
	
	/**
     * function translationForSync
     * @return mixed 
     */
	public function translationForSync()
	{
		return $this->belongsToMany(BrandTranslation::class, 'brand_translations', 'brand_id', 'language_id');
	}

    public function images()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }
}
