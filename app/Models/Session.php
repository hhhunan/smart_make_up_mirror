<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 15:09:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Session
 * 
 * @property int $id
 * @property string $name
 * @property int $play_list_id
 * @property int $status
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\PlayList $play_list
 *
 * @package App\Models
 */
class Session extends Eloquent
{
//    private static $status = [0,1];
//    private static $type = ['make_up_by_style', 'make_up_by_product'];
//    protected $attributes = [
//        'status' => [0,1],
//        'type' => ['make_up_by_style', 'make_up_by_product'],
//    ];
	protected $casts = [
		'play_list_id' => 'int',
		'status' => 'int',
		'type' => 'string'
	];

	protected $fillable = [
		'play_list_id',
		'status',
        'type'
	];

    public $ruleForCreate = [
        'name' => 'required|array',
        'name.ru'=>'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        "description" => "required|array",
        'description.ru'=>'min:1|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"\[\]{},+<>@&]*$/',
        'description.es' => 'min:1|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_?!()%`.\'"\[\]{},+<>@&]*$/',
        'description.en' => 'min:1|regex:/[a-zA-Z0-9 \-\/_?!()%`.\'"\[\]{},+<>@&]*$/',
        'play_list_id' => 'required',
        'status' => 'required|in:1,0',
        'type' => 'required|in:make_up_by_style,make_up_by_product',
    ];

    public $ruleForEdit = [
        'name' => 'array',
        'name.ru'=>'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        "description" => "array",
        'description.ru'=>'min:1|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"\[\]{},+<>@&]*$/',
        'description.es' => 'min:1|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_?!()%`.\'"\[\]{},+<>@&]*$/',
        'description.en' => 'min:1|regex:/[a-zA-Z0-9 \-\/_?!()%`.\'"\[\]{},+<>@&]*$/',
        'play_list_id' => 'numeric',
        'status' => 'in:1,0',
        'type' => 'in:make_up_by_style,make_up_by_product',

    ];

	/**
     * function translation
     * @return mixed
     */
    public function translation()
	{
		return $this->hasMany(SessionTranslation::class, 'session_id', 'id');
    }
    
    /**
     * function translationForSync
     * @return mixed
     */
	public function translationForSync()
	{
		return $this->belongsToMany(SessionTranslation::class, 'session_translations', 'session_id','language_id');
	}

	/**
     * function play_list
     * @return mixed
     */
    public function play_list()
	{
		return $this->hasMany(PlayList::class, 'id', 'play_list_id');
	}

	/**
     * function muvie
     * @return mixed
     */
    public function muvie()
	{
		return $this->belongsToMany(Movie::class,'play_lists', 'play_list_id', 'movie_id');
	}
}
