<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceConfig extends Model
{
    protected $hidden = [
        'created_at',
        'device_id',
    ];
    protected $casts = [
        'status' => 'int'
    ];

    protected $fillable = [
        'value',
        'status',
        'device_id',
        'type_id'
    ];

    public $ruleForCreate = [
        'group' => 'required|max:250',
        'type' => 'required|max:250',
        'value' => 'required|min:2|max:512',
        'device_id' => 'required|numeric'
    ];

    public $ruleForEdit = [
        'group' => 'max:250',
        'type' => 'max:250',
        'value' => 'min:2|max:512',
        'device_id' => 'numeric'
    ];

    /**
     * device
     * @return mixed 
     */
    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }
}
