<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MakeUpStyle extends Model
{
    protected $casts = [
        'status' => 'int'
    ];

    protected $fillable = [
        'status',
    ];

    public $hidden = [
        "pivot"
    ];

    public $ruleForCreate = [
        "name" => "required|array",
        'name.ru' => 'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        "session_ids" => "array",
        "session_ids.*" => "numeric",
        'image' => 'required|min:250',
    ];

    public $ruleForEdit = [
        "name" => "array",
        'name.ru' => 'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        "session_ids" => "array",
        "session_ids.*" => "numeric",
        'image' => 'min:250',
    ];

    public function devices()
    {
        return $this->belongsToMany(\App\Models\Device::class, 'device_make_up_styles');
    }

    public function translation()
    {
        return $this->hasMany(MakeUpStyleTranslation::class);
    }

    /**
     * function translationForSync
     * @return mixed
     */
    public function translationForSync()
    {
        return $this->belongsToMany(MakeUpStyleTranslation::class, 'make_up_style_translations', 'make_up_style_id', 'language_id');
    }

    public function sessions()
    {
        return $this->belongsToMany(\App\Models\Session::class, 'make_up_style_sessions');
    }

//    public function images()
//    {
//        return $this->belongsToMany(\App\Models\Image::class, 'make_up_style_images');
//    }

    public function makeupmethod()
    {
        return $this->hasOne(MakeUpMethod::class, 'id', 'make_up_method_id');
    }
    
    public function images()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

}
