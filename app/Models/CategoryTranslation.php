<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryTranslation
 * @var mixed
 * @property \Illuminate\Database\Eloquent\Collection $language
 */
class CategoryTranslation extends Model
{
    protected $table = "categorie_translations";
    
    public $timestamps = false;

    public $hidden = ["categorie_id", "id", "language_id"];

    public $rule_post = [
        'name' => 'required|max:100|regex:/^[A-Za-z\s-_]+$/',
    ];

    public $rule_put = [
        'name' => 'required|unique:brands|max:100|regex:/^[A-Za-z\s-_]+$/',
    ];

    /**
     * function language
     * @return mixed 
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'language_id');
    }
}
