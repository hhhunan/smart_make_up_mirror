<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceUpdate extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'status',
        'device_id',
        'hash',
        'modify_update_date'
    ];

    /**
     * function device_update
     * @return mixed 
     */
    public function device_update()
    {
        return $this->hasOne(Device::class, 'id', 'image_id');
    }
}
