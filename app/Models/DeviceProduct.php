<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 15:09:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DeviceProduct
 * 
 * @property int $device_id
 * @property int $product_id
 * 
 * @property \App\Models\Device $device
 * @property \App\Models\Product $product
 *
 * @package App\Models
 */
class DeviceProduct extends Eloquent
{
	public $incrementing = false;

	public $timestamps = false;

	protected $casts = [
		'device_id' => 'int',
		'product_id' => 'int'
	];

	protected $fillable = [
		'device_id',
		'product_id'
	];

	/**
	 * device
	 * @return mixed 
	 */
	public function device()
	{
		return $this->belongsTo(\App\Models\Device::class);
	}

	/**
	 * product
	 * @return mixed 
	 */
	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}
}
