<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MakeUpMethod extends Model
{
    public $timestamps = false;
    protected $casts = [
        'status' => 'int'
    ];

    protected $fillable = [
        'status'
    ];

    public $hidden = [
        "pivot"
    ];


    public $ruleForEdit = [
        "name" => "array",
        'name.ru' => 'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'image' => 'min:250',
    ];


    public function style()
    {
        return $this->hasMany(MakeUpStyle::class, 'make_up_method_id', 'id');
    }

    public function product()
    {
        return $this->hasMany(Product::class, 'make_up_method_id', 'id');
    }

    public function translationForSync()
    {
        return $this->belongsToMany(MakeUpMethodTranslation::class, 'make_up_method_translations', 'make_up_method_id', 'language_id');
    }

//    public function images()
//    {
//        return $this->belongsToMany(\App\Models\Image::class, 'make_up_method_images');
//    }

    public function translation()
    {
        return $this->hasMany(MakeUpMethodTranslation::class, 'make_up_method_id', 'id');
    }
    public function images()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

}
