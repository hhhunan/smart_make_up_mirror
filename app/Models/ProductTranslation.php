<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryTranslation
 * @var mixed
 * @property \Illuminate\Database\Eloquent\Collection $language
 */
class ProductTranslation extends Model
{
    protected $table = "product_translations";
    
    public $timestamps = false;

    public $hidden = ["product_id", "id", "language_id"];

    public $ruleForCreate = [
        'name' => 'required|max:100|regex:/^[A-Za-z\s-_]+$/',
    ];

    public $ruleForEdit = [
        'name' => 'required|unique:brands|max:100|regex:/^[A-Za-z\s_]+$/',
    ];

    protected $fillable = [
        'name',
        'description',
        'product_id',
        'language_id'
    ];

    /**
     * function language
     * @return mixed 
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'language_id');
    }
}
