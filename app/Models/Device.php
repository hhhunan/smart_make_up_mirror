<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Sep 2017 15:09:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Device
 * 
 * @property int $id
 * @property string $name
 * @property string $imei_number
 * @property string $secret_id
 * @property int $type
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Device extends Eloquent
{
    protected $casts = [
        'type' => 'int',
        'status' => 'int',
        'by_live_image_id' => 'int',
        'by_session_image_id' => 'int'

    ];

    protected $fillable = [
        'imei_number',
        'type',
        'status',
        'by_live_image_id',
        'by_session_image_id'

    ];

    public $hidden = [
        "pivot",
        "secret_id"
    ];

    public $ruleForCreate = [
        'name' => 'required|array',
        'name.ru' => 'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'name.es' => 'min:1|max:255|regex:/[a-zA-Z0-9áéíñóúüÁÉÍÑÓÚÜ \-\/_()%.\'"@&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 \-\/_()%.\'"@&]*$/',
        'imei_number' => 'required|max:100',
        'type' => 'required|numeric|min:0|max:1',
        'cfg_ids' => 'array',
        'product_ids' => 'array',
        'style_ids' => 'array',
        'by_live_image' => 'required|min:250',
        'by_session_image' => 'required|min:250',


    ];

    public $ruleForEdit = [
        'name' => 'array',
        'name.ru' => 'min:1|max:255|regex:/[а-яА-яa-zA-Z0-9 .&]*$/',
        'name.en' => 'min:1|max:255|regex:/[a-zA-Z0-9 .&]*$/',
        'imei_number' => 'max:100',
        'type' => 'numeric|min:0|max:1',
        'cfg_ids' => 'array',
        'product_ids' => 'array',
        'style_ids' => 'array',
        'by_live_image' => 'min:250',
        'by_session_image' => 'min:250',

    ];


    /**
     * products
     * @return mixed 
     */
    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'device_products');
    }
   public function styles()
    {
        return $this->belongsToMany(\App\Models\MakeUpStyle::class, 'device_make_up_styles');
    }

    public function makeupmethods()
    {
        return $this->belongsToMany(\App\Models\MakeUpMethod::class, 'device_make_up_methods');
    }


    /**
     * function translation
     * @return mixed 
     */
    public function translation()
    {
        return $this->hasMany(DeviceTranslation::class, 'device_id', 'id');
    }

    /**
     * function translationForSync
     * @return mixed 
     */
    public function translationForSync()
    {
        return $this->belongsToMany(DeviceTranslation::class, 'device_translations', 'device_id', 'language_id');
    }

    /**
     * device_config
     * @return mixed
     */
    public function device_config()
    {
        return $this->hasMany(DeviceConfig::class);
    }

    public function device_updates()
    {
        return $this->hasMany(DeviceUpdate::class);
    }

    /**
     * secret
     * @return mixed
     */
    public function secret()
    {
        return $this->hasOne(\App\OAuthClient::class, 'id', 'secret_id');
    }

    public function by_live_image()
    {
        return $this->hasOne(Image::class, 'id', 'by_live_image_id');

    }
    public function by_session_guide_image()
    {
        return $this->hasOne(Image::class, 'id', 'by_session_image_id');

    }
}
